FROM maven:3.6.3-jdk-11-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package -DskipTests
FROM openjdk:11

COPY --from=build /home/app/target/med-checkup.jar /med-checkup.jar
RUN echo "Asia/Tashkent" > /etc/timezone
EXPOSE 8282
    ENTRYPOINT ["java","-jar","med-checkup.jar"]

