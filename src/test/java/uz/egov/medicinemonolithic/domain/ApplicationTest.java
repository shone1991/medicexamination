package uz.egov.medicinemonolithic.domain;


import uz.egov.medicinemonolithic.entity.Application;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class ApplicationTest {
    public static Application application(){
        Application application=new Application();
        application.setApplicant(ApplicantTest.applicant());
        return application;
    }

    public static Application applicationSaved(){
        Application application=new Application();
        application.setId(1L);
        application.setApplicant(ApplicantTest.applicant());
        application.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
        application.setUpdatedAt(Timestamp.valueOf(LocalDateTime.now()));
        return application;
    }
}
