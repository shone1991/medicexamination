package uz.egov.medicinemonolithic.domain;

import uz.egov.medicinemonolithic.entity.Applicant;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class ApplicantTest {
    public static Applicant applicant(){
        Applicant applicant=new Applicant();
        applicant.setId(1L);
        applicant.setBranch("Mirzo ulugbek");
        applicant.setFHDY_Bosh("Anvarov Anvar Ashurovich");
        applicant.setJshshir("12752311275231");
        applicant.setInVoiceD(LocalDate.of(20222,1,3));
        applicant.setInVoiceN("123745856");
        applicant.setNSh_FIO("Tursunov Abror Qodirovich");
        applicant.setNtky_id("41236748");
        applicant.setYNumber("7458");
        applicant.setNSh_Address("Yunusobod");
        applicant.setNSh_BDate(LocalDate.of(1999,1,3));
        applicant.setYDate(LocalDate.of(20222,1,3));
        applicant.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
        applicant.setUpdatedAt(Timestamp.valueOf(LocalDateTime.now()));
        return applicant;
    }
    public static Applicant applicantToSave(){
        Applicant applicant=new Applicant();
        applicant.setBranch("Mirzo ulugbek");
        applicant.setFHDY_Bosh("Anvarov Anvar Ashurovich");
        applicant.setJshshir("12752311275231");
        applicant.setInVoiceD(LocalDate.of(20222,1,3));
        applicant.setInVoiceN("123745856");
        applicant.setNSh_FIO("Tursunov Abror Qodirovich");
        applicant.setNtky_id("41236748");
        applicant.setYNumber("7458");
        applicant.setNSh_Address("Yunusobod");
        applicant.setNSh_BDate(LocalDate.of(1999,1,3));
        applicant.setYDate(LocalDate.of(20222,1,3));
        return applicant;
    }
}
