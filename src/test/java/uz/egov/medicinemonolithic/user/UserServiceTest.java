package uz.egov.medicinemonolithic.user;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import uz.egov.medicinemonolithic.repository.user_role.UserRepository;

/**
 * @Author: Saidov Og'abek
 * @Created: 22-February-2022  Tuesday 5:01 PM
 **/
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional(propagation = Propagation.NOT_SUPPORTED)
@SpringBootTest
public class UserServiceTest {
    @Autowired
    UserRepository userRepository;

    @Test
    public void testing() {
        System.out.println(userRepository.findByPinfl("52212005360020"));
    }
}
