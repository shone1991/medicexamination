package uz.egov.medicinemonolithic.repository.conclusion;

import uz.egov.medicinemonolithic.domain.ApplicantTest;
import uz.egov.medicinemonolithic.entity.Applicant;
import uz.egov.medicinemonolithic.entity.Application;
import uz.egov.medicinemonolithic.entity.ApplicationStatus;
import uz.egov.medicinemonolithic.entity.conclusion.EndocrinologistConclusion;
import uz.egov.medicinemonolithic.entity.conclusion.OverallConclusion;
import uz.egov.medicinemonolithic.repository.ApplicantRepository;
import uz.egov.medicinemonolithic.repository.ApplicationRepository;
import uz.egov.medicinemonolithic.repository.conclusion.EndocrinologistConclusionRepository;
import uz.egov.medicinemonolithic.repository.conclusion.OverallConclusionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional(propagation = Propagation.NOT_SUPPORTED)
@SpringBootTest
class OverallConclusionRepositoryTest {

    @Autowired
    OverallConclusionRepository overallConclusionRepository;

    @Autowired
    EndocrinologistConclusionRepository endocrinologistConclusionRepository;

    @Autowired
    ApplicationRepository applicationRepository;

    @Autowired
    ApplicantRepository applicantRepository;

    @Test
    void saveOverallConclusionTest(){
        Applicant savedApplicant=applicantRepository.save(ApplicantTest.applicantToSave());
        Application application=new Application();
        application.setApplicant(savedApplicant);
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        Application savedApplication=applicationRepository.save(application);
        EndocrinologistConclusion endocrinologistConclusion=new EndocrinologistConclusion();
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
        endocrinologistConclusion.setComment("endokrinolog xulosasi: soglom");
        endocrinologistConclusion.setApplication(savedApplication);
//        endocrinologistConclusion.setSpecialistId(2L);
//        endocrinologistConclusion.setIdWorkPlace(3L);
        EndocrinologistConclusion savedEndocrinologistConclusion=endocrinologistConclusionRepository.save(endocrinologistConclusion);
        OverallConclusion overallConclusion=new OverallConclusion();
        overallConclusion.setEndocrinologistConclusion(savedEndocrinologistConclusion);
        overallConclusion.setComment("soglom");
        overallConclusion.setConclusionDate(LocalDate.now());
        overallConclusion.setApplication(savedApplication);
//        overallConclusion.setIdWorkPlace(2L);
//        overallConclusion.setSpecialistId(3L);
        OverallConclusion overallConclusionSaved=overallConclusionRepository.save(overallConclusion);
        System.out.println(overallConclusionSaved.getEndocrinologistConclusion().getComment());

    }
}