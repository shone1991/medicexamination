package uz.egov.medicinemonolithic.repository.conclusion;

import org.checkerframework.checker.units.qual.A;
import org.springframework.data.domain.Pageable;
import uz.egov.medicinemonolithic.domain.ApplicantTest;
import uz.egov.medicinemonolithic.entity.*;
import uz.egov.medicinemonolithic.entity.conclusion.EndocrinologistConclusion;
import uz.egov.medicinemonolithic.entity.users.User;
import uz.egov.medicinemonolithic.entity.users.UserDetail;
import uz.egov.medicinemonolithic.repository.ApplicantRepository;
import uz.egov.medicinemonolithic.repository.ApplicationRepository;
import uz.egov.medicinemonolithic.repository.ClinicRepository;
import uz.egov.medicinemonolithic.repository.SpecialistRepository;
import uz.egov.medicinemonolithic.repository.conclusion.EndocrinologistConclusionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import uz.egov.medicinemonolithic.repository.user_role.UserRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
class EndocrinologistConclusionRepositoryTest {
    @Autowired
    EndocrinologistConclusionRepository endocrinologistConclusionRepository;
    @Autowired
    ApplicantRepository applicantRepository;
    @Autowired
    ApplicationRepository applicationRepository;
    @Autowired
    ClinicRepository clinicRepository;
    @Autowired
    SpecialistRepository specialistRepository;

    @Autowired
    UserRepository userRepository;

    @Test
    @Transactional
    @Rollback
    void saveEndoConcTest(){
        Applicant applicant= ApplicantTest.applicantToSave();
        Applicant savedApplicant=applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(savedApplicant);
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        Application savedApplication=applicationRepository.save(application);
        EndocrinologistConclusion endocrinologistConclusion=new EndocrinologistConclusion();
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
        endocrinologistConclusion.setComment("soglom");
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
//        endocrinologistConclusion.setIdWorkPlace(1L);
//        endocrinologistConclusion.setSpecialistId(1L);
        endocrinologistConclusion.setApplication(savedApplication);
        EndocrinologistConclusion savedEndoConc=endocrinologistConclusionRepository.save(endocrinologistConclusion);
        System.out.println(savedEndoConc.getApplication().getApplicant().getNSh_FIO()+":"+savedEndoConc.getComment());
    }

    @Test
    @Transactional
    @Rollback
    void findByApplicationIdTest(){
        Applicant applicant= ApplicantTest.applicantToSave();
        Applicant savedApplicant=applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(savedApplicant);
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        Application savedApplication=applicationRepository.save(application);
        EndocrinologistConclusion endocrinologistConclusion=new EndocrinologistConclusion();
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
        endocrinologistConclusion.setComment("soglom");
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
//        endocrinologistConclusion.setIdWorkPlace(1L);
//        endocrinologistConclusion.setSpecialistId(1L);
        endocrinologistConclusion.setApplication(savedApplication);
        endocrinologistConclusionRepository.save(endocrinologistConclusion);
//        EndocrinologistConclusion found=endocrinologistConclusionRepository.findEndocrinologistConclusionByApplicationId(savedApplication.getId());
//        System.out.println(found.getApplication().getApplicant().getNSh_FIO());
    }

    @Test
    @Transactional
    @Rollback
    void findByApplicantIdTest(){
        Applicant applicant= ApplicantTest.applicantToSave();
        Applicant savedApplicant=applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(savedApplicant);
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        Application savedApplication=applicationRepository.save(application);
        EndocrinologistConclusion endocrinologistConclusion=new EndocrinologistConclusion();
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
        endocrinologistConclusion.setComment("soglom");
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
//        endocrinologistConclusion.setIdWorkPlace(1L);
//        endocrinologistConclusion.setSpecialistId(1L);
        endocrinologistConclusion.setApplication(savedApplication);
        endocrinologistConclusionRepository.save(endocrinologistConclusion);
//        EndocrinologistConclusion found=endocrinologistConclusionRepository.findEndocrinologistConclusionByApplicantId(savedApplicant.getId());
//        System.out.println(found.getApplication().getApplicant().getNSh_FIO());
    }

    @Test
    @Transactional
    @Rollback
    void findEndocrinologistConclusionByApplicantJShshirTest(){
        Applicant applicant= ApplicantTest.applicantToSave();
        Applicant savedApplicant=applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(savedApplicant);
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        Application savedApplication=applicationRepository.save(application);
        EndocrinologistConclusion endocrinologistConclusion=new EndocrinologistConclusion();
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
        endocrinologistConclusion.setComment("soglom");
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
//        endocrinologistConclusion.setIdWorkPlace(1L);
//        endocrinologistConclusion.setSpecialistId(1L);
        endocrinologistConclusion.setApplication(savedApplication);
        endocrinologistConclusionRepository.save(endocrinologistConclusion);
//        List<EndocrinologistConclusion> found=endocrinologistConclusionRepository.findEndocrinologistConclusionByApplicantJShshir(savedApplicant.getJshshir());
//        assertThat(found.size()>0);
    }

    @Test
    @Transactional
    @Rollback
    void findEndocrinologistConclusionByApplicantNtky_idTest(){
        Applicant applicant= ApplicantTest.applicantToSave();
        applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(applicant);
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        applicationRepository.save(application);
        EndocrinologistConclusion endocrinologistConclusion=new EndocrinologistConclusion();
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
        endocrinologistConclusion.setComment("soglom");
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
//        endocrinologistConclusion.setIdWorkPlace(1L);
//        endocrinologistConclusion.setSpecialistId(1L);
        endocrinologistConclusion.setApplication(application);
        endocrinologistConclusionRepository.save(endocrinologistConclusion);
//        EndocrinologistConclusion found=endocrinologistConclusionRepository.findEndocrinologistConclusionByApplicantNtky_id(applicant.getNtky_id());
//        assertThat(found.getApplication().getApplicant().getNtky_id().equals(applicant.getNtky_id()));
    }

    @Test
    @Transactional
    @Rollback
    void findEndocrinologistConclusionByApplicantByyNumberTest(){
        Applicant applicant= ApplicantTest.applicantToSave();
        applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(applicant);
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        applicationRepository.save(application);
        EndocrinologistConclusion endocrinologistConclusion=new EndocrinologistConclusion();
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
        endocrinologistConclusion.setComment("soglom");
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
//        endocrinologistConclusion.setIdWorkPlace(1L);
//        endocrinologistConclusion.setSpecialistId(1L);
        endocrinologistConclusion.setApplication(application);
        endocrinologistConclusionRepository.save(endocrinologistConclusion);
//        EndocrinologistConclusion found=endocrinologistConclusionRepository.findEndocrinologistConclusionByApplicantByyNumber(applicant.getYNumber());
//        assertThat(found.getApplication().getApplicant().getYNumber().equals(applicant.getYNumber()));
    }

    @Test
    @Transactional
    @Rollback
    void findEndocrinologistConclusionsBySpecialistId(){
        UserDetail userDetail=new UserDetail();
        userDetail.setName("Jamshid");
        userDetail.setSurname("Xojiakbarov");
        userDetail.setBirthDate(LocalDate.of(1988,6,15));
        userDetail.setPinfl("31506885232541");
        userDetail.setPassportSerialNumber("AC2585225");
        User user = new User();
        user.setUsername("jam");
        user.setPassword("123");
        user.setUserDetail(userDetail);
        user.setIsActive(true);
        user.setPhone("998977771014");
        userRepository.save(user);
        Clinic clinic=new Clinic();
        clinic.setName("1-poliklinika");
        clinicRepository.save(clinic);
        Specialist specialist=new Specialist();
        specialist.setWorkPlace(clinic);
        specialist.setUserMedicine(user);
        specialistRepository.save(specialist);
        Applicant applicant= ApplicantTest.applicantToSave();
        applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(applicant);
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        applicationRepository.save(application);
        EndocrinologistConclusion endocrinologistConclusion=new EndocrinologistConclusion();
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
        endocrinologistConclusion.setComment("soglom");
        endocrinologistConclusion.setConclusionDate(LocalDate.now());
        endocrinologistConclusion.setSpecialist(specialist);
        endocrinologistConclusion.setWorkPlace(specialist.getWorkPlace());
        endocrinologistConclusion.setApplication(application);
        endocrinologistConclusionRepository.save(endocrinologistConclusion);
        List<EndocrinologistConclusion> found=endocrinologistConclusionRepository.findEndocrinologistConclusionsBySpecialistId(specialist.getId(), Pageable.ofSize(1));
        found.stream().forEach(e->{
            System.out.println(e.getApplication().getApplicant().getNSh_FIO());
            System.out.println(e.getApplication().getApplicant().getYNumber());
            System.out.println(e.getApplication().getApplicant().getYDate());
            System.out.println(e.getApplication().getApplicant().getNtky_id());
            System.out.println(e.getApplication().getApplicationStatus());
            System.out.println(e.getComment());
            System.out.println(e.getConclusionDate());
            System.out.println(e.getSpecialist().getUserMedicine().getId());
            System.out.println(e.getWorkPlace().getName());
        });
    }

}