package uz.egov.medicinemonolithic.repository.application;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.Rollback;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.domain.ApplicantTest;
import uz.egov.medicinemonolithic.entity.Applicant;
import uz.egov.medicinemonolithic.entity.Application;
import uz.egov.medicinemonolithic.entity.ApplicationStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import uz.egov.medicinemonolithic.repository.ApplicantRepository;
import uz.egov.medicinemonolithic.repository.ApplicationRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional(propagation = Propagation.NOT_SUPPORTED)
@SpringBootTest
class ApplicationRepositoryTest {

    @Autowired
    ApplicationRepository applicationRepository;

    @Autowired
    ApplicantRepository applicantRepository;

    @Test
    @Rollback
    void saveApplicationTest(){
        Applicant applicant=ApplicantTest.applicantToSave();
        Applicant savedApplicant=applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(savedApplicant);
        application.setInformation_date(LocalDateTime.now());
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        applicationRepository.save(application);
        applicationRepository.delete(application);
        applicantRepository.delete(applicant);
    }

    @Test
    @Rollback
    void findAllApplicationByStatusTest(){
        Applicant applicant=ApplicantTest.applicantToSave();
        Applicant savedApplicant=applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(savedApplicant);
        application.setInformation_date(LocalDateTime.now());
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        applicationRepository.save(application);

        Pageable pageable= PageRequest.of(0,2);

        List<Application> allApplicationByStatus = applicationRepository.findAllApplicationByStatus(application.getApplicationStatus(), pageable);

        assertThat(allApplicationByStatus.size()>0);

        applicationRepository.delete(application);
        applicantRepository.delete(applicant);
    }

    @Test
    @Rollback
    void findAllApplicationByStatusAndDateBetweenTest(){
        Applicant applicant=ApplicantTest.applicantToSave();
        Applicant savedApplicant=applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(savedApplicant);
        application.setInformation_date(LocalDateTime.now());
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        applicationRepository.save(application);
        Pageable pageable= PageRequest.of(0,2);
        final List<Application> allApplicationByStatusAndDateBetween = applicationRepository.findAllApplicationByStatusAndDateBetween(ApplicationStatus.ACCEPTED,
                LocalDate.now(), LocalDate.now().plusDays(1), pageable);
        assertThat(allApplicationByStatusAndDateBetween.size()>0);
        applicationRepository.delete(application);
        applicantRepository.delete(applicant);
    }

    @Test
    @Rollback
    void findAllApplicationByApplicantBranchAndDateTest(){
        Applicant applicant=ApplicantTest.applicantToSave();
        Applicant savedApplicant=applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(savedApplicant);
        application.setInformation_date(LocalDateTime.now());
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        applicationRepository.save(application);
        Pageable pageable= PageRequest.of(0,2);
        final List<Application> mirzo_ulugbek = applicationRepository.findAllApplicationByApplicantBranchAndDate("Mirzo ulugbek",
                LocalDate.now(), LocalDate.now().plusDays(1), pageable);
        assertThat(mirzo_ulugbek.size()>0);
        applicationRepository.delete(application);
        applicantRepository.delete(applicant);
    }

    @Test
    @Rollback
    void findApplicationByApplicantJshshirTest(){
        Applicant applicant=ApplicantTest.applicantToSave();
        Applicant applicant2=ApplicantTest.applicantToSave();
        applicant2.setYNumber("qwer2321");
        applicant2.setNtky_id("123587");
        Applicant savedApplicant=applicantRepository.save(applicant);
        applicantRepository.save(applicant2);
        Application application=new Application();
        application.setApplicant(savedApplicant);
        application.setInformation_date(LocalDateTime.now());
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        applicationRepository.save(application);
        Application application2=new Application(applicant2,LocalDateTime.now(),ApplicationStatus.ACCEPTED);
        applicationRepository.save(application2);
        Pageable pageable= PageRequest.of(0,2);
        final List<Application> foundByJshshir = applicationRepository.findApplicationByApplicantJshshir("12752311275231");
        assertThat(foundByJshshir.size()>0);
        applicationRepository.delete(application);
        applicationRepository.delete(application2);
        applicantRepository.delete(applicant);
        applicantRepository.delete(applicant2);
    }

    @Test
    @Rollback
    void findApplicationByApplicationId(){
        Applicant applicant=ApplicantTest.applicantToSave();
        Applicant savedApplicant=applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(savedApplicant);
        application.setInformation_date(LocalDateTime.now());
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        applicationRepository.save(application);
        Application applicationFound = applicationRepository.findApplicationByApplicationId(application.getId())
                .orElseThrow(()->new ResponseStatusException(HttpStatus.NO_CONTENT,
                        String.format("Application not found with this id [%d]",application.getId())));
        assertThat(applicationFound);
        applicationRepository.delete(application);
        applicantRepository.delete(applicant);
    }

    @Test
    @Rollback
    void findApplicationByApplicantId(){
        Applicant applicant=ApplicantTest.applicantToSave();
        Applicant savedApplicant=applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(savedApplicant);
        application.setInformation_date(LocalDateTime.now());
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        applicationRepository.save(application);
        Application applicationFound = applicationRepository.findApplicationByApplicantId(application.getApplicant().getId())
                .orElseThrow(()->new ResponseStatusException(HttpStatus.NO_CONTENT,
                        String.format("Applicant not found with this id [%d]",applicant.getId())));
        assertThat(applicationFound);
        applicationRepository.delete(application);
        applicantRepository.delete(applicant);
    }

    @Test
    @Rollback
    void findApplicationByApplicantNtkyId(){
        Applicant applicant=ApplicantTest.applicantToSave();
        Applicant savedApplicant=applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(savedApplicant);
        application.setInformation_date(LocalDateTime.now());
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        applicationRepository.save(application);
        Application applicationFound = applicationRepository.findApplicationByApplicantNtkyId(application.getApplicant().getNtky_id())
                .orElseThrow(()->new ResponseStatusException(HttpStatus.NO_CONTENT,
                        String.format("Applicant not found with this ntky_id [%s]",applicant.getNtky_id())));
        assertThat(applicationFound);
        applicationRepository.delete(application);
        applicantRepository.delete(applicant);
    }

    @Test
    @Transactional
    @Rollback
    void findApplicationByApplicantYnumber(){
        Applicant applicant=ApplicantTest.applicantToSave();
        Applicant savedApplicant=applicantRepository.save(applicant);
        Application application=new Application();
        application.setApplicant(savedApplicant);
        application.setInformation_date(LocalDateTime.now());
        application.setApplicationStatus(ApplicationStatus.ACCEPTED);
        applicationRepository.save(application);
        Application applicationFound = applicationRepository.findApplicationByApplicantYnumber(application.getApplicant().getYNumber(),
                        LocalDate.of(20222,1,3))
                .orElseThrow(()->new ResponseStatusException(HttpStatus.NO_CONTENT,
                        String.format("Applicant not found with this yNumber [%s]",applicant.getYNumber())));
        assertThat(applicationFound);
        applicationRepository.delete(application);
        applicantRepository.delete(applicant);
    }












}