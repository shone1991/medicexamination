package uz.egov.medicinemonolithic.repository.applicant;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.domain.ApplicantTest;
import uz.egov.medicinemonolithic.entity.Applicant;
import uz.egov.medicinemonolithic.repository.ApplicantRepository;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@EnableTransactionManagement
class ApplicantRepositoryTest {
    @Autowired
    ApplicantRepository applicantRepository;

    @Test
    @Rollback()
    void saveApplicantTest() {
        Applicant applicant = new Applicant();
        applicant.setBranch("Mirzo ulugbek");
        applicant.setFHDY_Bosh("Anvarov Anvar Ashurovich");
        applicant.setJshshir("12752311275231");
        applicant.setInVoiceD(LocalDate.of(20222, 1, 3));
        applicant.setInVoiceN("123745856");
        applicant.setNSh_FIO("Tursunov Abror Qodirovich");
        applicant.setNtky_id("41236748");
        applicant.setYNumber("7458");
        applicant.setNSh_Address("Yunusobod");
        applicant.setNSh_BDate(LocalDate.of(1999, 1, 3));
        applicant.setYDate(LocalDate.of(20222, 1, 3));
        Applicant saved = applicantRepository.save(applicant);
        assertNotNull(saved);
        applicantRepository.delete(saved);
    }

    @Test
    @Rollback
    void findApplicantByNtky_idTest(){
        Applicant applicant = ApplicantTest.applicantToSave();
        applicantRepository.save(applicant);
        final Applicant found = applicantRepository.findApplicantByNtky_id(applicant.getNtky_id()).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NO_CONTENT, String.format("applicant not found with this ntyk_id [%s]", applicant.getNtky_id())));
        assertNotNull(found);
        applicantRepository.delete(found);
    }

    @Test
    @Rollback
    void findApplicantByBranchAndYDateTest(){
        Applicant applicant1 = ApplicantTest.applicantToSave();
        Applicant applicant2 = ApplicantTest.applicantToSave();
        applicant2.setYNumber("4589");
        applicant2.setNtky_id("das544421");
        applicantRepository.save(applicant1);
        applicantRepository.save(applicant2);
        Pageable pageRequest = PageRequest.of(0, 2);
        List<Applicant> mirzo_ulugbek = applicantRepository.findApplicantByBranchAndYDate("Mirzo ulugbek",
                LocalDate.of(20222, 1, 3), pageRequest).orElseThrow(()->new ResponseStatusException(HttpStatus.NO_CONTENT,
                "Applicants with given params not found"));
        assertThat(mirzo_ulugbek.size()>0);
        applicantRepository.deleteAll(List.of(applicant1,applicant2));
    }




}