package uz.egov.medicinemonolithic.validator;

import org.springframework.beans.factory.annotation.Autowired;
import uz.egov.medicinemonolithic.repository.ApplicantRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author f.lapasov on 2/8/2022
 * @project medicine
 */
public class NtykUniqueValidator implements ConstraintValidator<Unique,String> {

    @Autowired
    private ApplicantRepository applicantRepository;

    @Override
    public void initialize(Unique unique) {
        unique.message();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (applicantRepository != null && applicantRepository.findApplicantByNtky_id(s)==null) {
            return false;
        }
        return true;
    }
}
