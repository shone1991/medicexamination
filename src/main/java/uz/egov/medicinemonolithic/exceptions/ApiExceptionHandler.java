package uz.egov.medicinemonolithic.exceptions;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * @author f.lapasov on 2/8/2022
 * @project medicine
 */

@ControllerAdvice
public class ApiExceptionHandler {
//    @ExceptionHandler(RuntimeException.class)
//    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
//    public ResponseEntity<ApiError> onServerErrorException(RuntimeException ex) {
//        ApiError apiError = new ApiError(HttpStatus.I_AM_A_TEAPOT, ex.getMessage(), ex);
//        return buildResponseEntity(apiError);
//    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiError> onRuntimeException(MethodArgumentNotValidException ex) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiError> onIllegalException(IllegalArgumentException ex) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ResponseEntity<ApiError> onAuthException(AuthenticationException ex) {
        ApiError apiError = new ApiError(HttpStatus.UNAUTHORIZED, "Bad Credential", ex);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(ResponseStatusException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiError> onBadRequestException(ResponseStatusException ex) {
        ApiError apiError = new ApiError(ex.getStatus(), ex.getReason(), ex);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public ResponseEntity<ApiError> onMethodNotSupportedException(HttpRequestMethodNotSupportedException ex) {
        ApiError apiError = new ApiError(HttpStatus.METHOD_NOT_ALLOWED, "Method Not Allowed", ex);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(DataNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ApiError> onDataNotFoundException(DataNotFoundException ex) {
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, "Data Not Found", ex);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ResponseEntity<ApiError> handleConflict(DataIntegrityViolationException ex) {
        ConstraintViolationException exDetail = (ConstraintViolationException) ex.getCause();
        ApiError apiError = new ApiError(HttpStatus.NOT_ACCEPTABLE, exDetail.getConstraintName() + " unique value violation", ex);
        return buildResponseEntity(apiError);
    }

    private ResponseEntity<ApiError> buildResponseEntity(ApiError apiError) {
        return ResponseEntity
                .status(apiError.getStatus())
                .body(apiError);
    }

//    private String fieldCnvrt(String field) {
//        switch (field) {
//            case "ntyk_id":
//                field = "Kullanıcı Adı";
//                break;
//            case "email":
//                field = "Email";
//                break;
//            case "short_name":
//                field = "Kısaltımış Ad";
//                break;
//            case "name":
//                field = "Ad";
//                break;
//        }
//
//        return field;
//    }
}
