package uz.egov.medicinemonolithic.exceptions;

/**
 * @author f.lapasov on 2/8/2022
 * @project medicine
 */
public class DataNotFoundException extends RuntimeException{
    public DataNotFoundException(String message) {
        super(message);
    }
}
