package uz.egov.medicinemonolithic.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Page or size error entered")
public class PageableException extends RuntimeException {

    public PageableException(String message) {
        super(message);
    }

}
