package uz.egov.medicinemonolithic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedicineMonolithicApplication {

    public static void main(String[] args) {
        SpringApplication.run(MedicineMonolithicApplication.class, args);
    }
}
