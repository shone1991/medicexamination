package uz.egov.medicinemonolithic.advice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 28-March-2022  Monday 5:34 PM
 **/
@Aspect
@Component
@RequiredArgsConstructor
public class LoggingAdvice {
    private static final Logger LOGGER = LogManager.getLogger();

    @Pointcut(value = "execution(* uz.egov.medicinemonolithic.service.conclusion.*.*.*(..))")
    public void myPointcut() {
    }

    @Around("myPointcut()")
    public Object applicationLogger(ProceedingJoinPoint pjp) throws Throwable {
        ObjectMapper objectMapper = new ObjectMapper();
        String methodName = pjp.getSignature().getName();
        String className = pjp.getTarget().getClass().toString();
        Object[] array = pjp.getArgs();
        String username;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }

        Object object = pjp.proceed();
        LOGGER.info("\n=======================================\n" +
                        "Username: [{}]\n" +
                        "Method invoked {} : {}()\n" +
                        "Arguments: {}\n" +
                        "Response: {}\n" +
                        "=======================================\n",
                username, className, methodName, objectMapper.writeValueAsString(array),
                objectMapper.writeValueAsString(object.toString()));
        return object;
    }

    @AfterThrowing(pointcut = "execution(* uz.egov.medicinemonolithic.service.conclusion.*.*.*(..))", throwing = "ex")
    public void errorInterceptor(Exception ex) {
        String username;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Error Message Interceptor started");
            LOGGER.info("\n=======================================\n" +
                            "Username: [{}]\n" +
                            "Message: {}\n" +
                            "=======================================\n",
                    username,
                    ex.getMessage());
        }

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Error Message Interceptor finished.");
        }
    }
}