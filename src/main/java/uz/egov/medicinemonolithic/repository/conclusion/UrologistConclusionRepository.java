package uz.egov.medicinemonolithic.repository.conclusion;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.egov.medicinemonolithic.entity.conclusion.UrologistConclusion;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface UrologistConclusionRepository extends JpaRepository<UrologistConclusion,Long> {
    /*Below methods for retrieving existing records of medical checkups*/

    /**
     * @param specialistId
     * @param pageable
     * @return
     */
    @Query(value = "SELECT u FROM UrologistConclusion u join fetch u.application as application " +
            "join fetch application.applicant applicant WHERE u.specialist.id=:specialistId")
    List<UrologistConclusion> findUrologistConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    /**
     * @param idWorkPlace
     * @param pageable
     * @return
     */
    @Query(value = "SELECT u FROM UrologistConclusion u join fetch u.application as application " +
            "join fetch application.applicant applicant WHERE u.workPlace.id=:idWorkPlace")
    List<UrologistConclusion> findUrologistConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    /**
     * @param applicationId
     * @return
     */
    @Query(value = "SELECT u FROM UrologistConclusion u join fetch u.application as application " +
            "WHERE application.id=:applicationId")
    Optional<UrologistConclusion> findUrologistConclusionByApplicationId(Long applicationId);

    /**
     * @param ntky_id
     * @return
     */
    @Query(value = "SELECT u FROM UrologistConclusion u join fetch u.application as application " +
            "join fetch application.applicant applicant WHERE applicant.ntky_id=:ntky_id")
    Optional<UrologistConclusion> findUrologistConclusionByApplicantNtky_id(String ntky_id);

    /**
     * @param yNumber
     * @return
     */
    @Query(value = "SELECT u FROM UrologistConclusion u join fetch u.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yNumber=:yNumber")
    Optional<UrologistConclusion> findUrologistConclusionByApplicantByyNumber(String yNumber);

    /**
     * @param applicantId
     * @return
     */
    @Query(value = "SELECT u FROM UrologistConclusion u join fetch u.application as application " +
            "join fetch application.applicant applicant WHERE applicant.id=:applicantId")
    Optional<UrologistConclusion> findUrologistConclusionByApplicantId(Long applicantId);

    /**
     * @param jshshir
     * @param pageable
     * @return
     */
    @Query(value = "SELECT u FROM UrologistConclusion u join fetch u.application as application " +
            "join fetch application.applicant applicant WHERE applicant.jshshir=:jshshir")
    List<UrologistConclusion> findUrologistConclusionByApplicantJShshir(String jshshir, Pageable pageable);

    /**
     * @param yDate
     * @param pageable
     * @return
     */
    @Query(value = "SELECT u FROM UrologistConclusion u join fetch u.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate=:yDate")
    List<UrologistConclusion> findUrologistConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    /**
     * @param yDate1
     * @param yDate2
     * @return
     */
    @Query(value = "SELECT u FROM UrologistConclusion u join fetch u.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate>=:yDate1 AND applicant.yDate<=:yDate2")
    List<UrologistConclusion> findUrologistConclusionByApplicantByYDate(LocalDate yDate1, LocalDate yDate2, Pageable pageable);

}
