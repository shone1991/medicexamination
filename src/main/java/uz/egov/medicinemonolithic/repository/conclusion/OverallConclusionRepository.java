package uz.egov.medicinemonolithic.repository.conclusion;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.egov.medicinemonolithic.entity.conclusion.OverallConclusion;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface OverallConclusionRepository extends JpaRepository<OverallConclusion, Long> {
    /*Below methods for retrieving existing records of medical checkups*/

    /**
     * BySpecialistId
     */
    @Query(value = "SELECT r FROM OverallConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE r.specialist.id=:specialistId")
    List<OverallConclusion> findOverallConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    /**
     * ByIdWorkPlace
     */
    @Query(value = "SELECT r FROM OverallConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE r.workPlace.id=:idWorkPlace")
    List<OverallConclusion> findOverallConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    /**
     * ByApplicationId
     */
    @Query(value = "SELECT r FROM OverallConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE application.id=:applicationId")
    Optional<OverallConclusion> findOverallConclusionByApplicationId(Long applicationId);

    /**
     * ByApplicantNtky_id
     */
    @Query(value = "SELECT r FROM OverallConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.ntky_id=:ntky_id")
    Optional<OverallConclusion> findOverallConclusionByApplicantNtky_id(String ntky_id);

    /**
     * ByApplicantByyNumber
     */
    @Query(value = "SELECT r FROM OverallConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yNumber=:yNumber")
    Optional<OverallConclusion> findOverallConclusionByApplicantByyNumber(String yNumber);

    /**
     * ByApplicantId
     */
    @Query(value = "SELECT r FROM OverallConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.id=:applicantId")
    Optional<OverallConclusion> findOverallConclusionByApplicantId(Long applicantId);

    /**
     * ByApplicantJShshir
     */
    @Query(value = "SELECT r FROM OverallConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.jshshir=:jshshir")
    List<OverallConclusion> findOverallConclusionByApplicantJShshir(String jshshir, Pageable pageable);

    /**
     * ByApplicantByYDate
     */
    @Query(value = "SELECT r FROM OverallConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate=:yDate")
    List<OverallConclusion> findOverallConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    /**
     * ByApplicantByYDate
     */
    @Query(value = "SELECT r FROM OverallConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate>=:yDate1 AND applicant.yDate<=:yDate2")
    List<OverallConclusion> findOverallConclusionByApplicantByYDate(LocalDate yDate1,
                                                                              LocalDate yDate2,
                                                                              Pageable pageable);

}
