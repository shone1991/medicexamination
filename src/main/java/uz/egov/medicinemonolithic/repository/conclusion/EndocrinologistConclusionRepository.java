package uz.egov.medicinemonolithic.repository.conclusion;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.egov.medicinemonolithic.entity.conclusion.EndocrinologistConclusion;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface EndocrinologistConclusionRepository extends JpaRepository<EndocrinologistConclusion,Long> {


    /*Below methods for retrieving existing records of medical checkups*/

    /***
     *
     * @param specialistId
     * @return
     */
    @Query(value = "SELECT e FROM EndocrinologistConclusion e join fetch e.application as application " +
            "join fetch e.specialist as specialist join fetch application.applicant applicant WHERE e.specialist.id=:specialistId")
    List<EndocrinologistConclusion> findEndocrinologistConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    /***
     *
     * @param idWorkPlace
     * @return
     */
    @Query(value = "SELECT e FROM EndocrinologistConclusion e join fetch e.application as application " +
            "join fetch application.applicant applicant WHERE e.workPlace.id=:idWorkPlace")
    List<EndocrinologistConclusion> findEndocrinologistConclusionsByIdWorkPlace(Long idWorkPlace,
                                                                                Pageable pageable);

    //    @Query(value = "SELECT e FROM EndocrinologistConclusion e join fetch e.application as application " +
//            "join fetch application.applicant applicant WHERE e.conclusionDate =:conclusionDate")
//    List<EndocrinologistConclusion> findEndocrinologistConclusionsByConclusionDate(Date conclusionDate);

//    @Query(value = "SELECT e FROM EndocrinologistConclusion e join fetch e.application as application " +
//            "join fetch application.applicant applicant WHERE e.conclusionDate >=:conclusionDate1 and e.conclusionDate <=:conclusionDate2")
//    List<EndocrinologistConclusion> findEndocrinologistConclusionsByBetweenDate1AndDate2(Date conclusionDate1, Date conclusionDate2);

//    @Query(value = "SELECT e FROM EndocrinologistConclusion e join fetch e.application as application " +
//            "join fetch application.applicant applicant WHERE date(e.createdAt) =:localDate")
//    List<EndocrinologistConclusion> findEndocrinologistConclusionByCreatedAt(Date localDate);

//    @Query(value = "SELECT e FROM EndocrinologistConclusion e join fetch e.application as application " +
//            "join fetch application.applicant applicant WHERE date(e.createdAt) >=:localDate1 and date(e.createdAt) <=:localDate2")
//    List<EndocrinologistConclusion> findEndocrinologistConclusionsByBetweenDate1AndDate2(LocalDate localDate1, LocalDate LocaDate2);

    /***
     *
     * @param applicationId
     * @return
     */
    @Query(value = "SELECT e FROM EndocrinologistConclusion e join fetch e.application as application " +
            "join fetch application.applicant applicant WHERE application.id=:applicationId")
    Optional<EndocrinologistConclusion> findEndocrinologistConclusionByApplicationId(Long applicationId);

    /***
     *
     * @param ntky_id
     * @return
     */
    @Query(value = "SELECT e FROM EndocrinologistConclusion e join fetch e.application as application " +
            "join fetch application.applicant applicant WHERE applicant.ntky_id=:ntky_id")
    Optional<EndocrinologistConclusion> findEndocrinologistConclusionByApplicantNtky_id(String ntky_id);

    /***
     *
     * @param yNumber
     * @return
     */
    @Query(value = "SELECT e FROM EndocrinologistConclusion e join fetch e.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yNumber=:yNumber")
    Optional<EndocrinologistConclusion> findEndocrinologistConclusionByApplicantByyNumber(String yNumber);

    /***
     *
     * @param applicantId
     * @return
     */
    @Query(value = "SELECT e FROM EndocrinologistConclusion e join fetch e.application as application " +
            "join fetch application.applicant applicant WHERE applicant.id=:applicantId")
    Optional<EndocrinologistConclusion> findEndocrinologistConclusionByApplicantId(Long applicantId);

    /***
     *
     * @param jshshir
     * @return
     */
    @Query(value = "SELECT e FROM EndocrinologistConclusion e join fetch e.application as application " +
            "join fetch application.applicant applicant WHERE applicant.jshshir=:jshshir")
    List<EndocrinologistConclusion> findEndocrinologistConclusionByApplicantJShshir(String jshshir,
                                                                                    Pageable pageable);

    /***
     *
     * @param yDate,
     * Pageable pageable
     * @return
     */
    @Query(value = "SELECT e FROM EndocrinologistConclusion e join fetch e.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate=:yDate")
    List<EndocrinologistConclusion> findEndocrinologistConclusionByApplicantByYDate(LocalDate yDate,
                                                                                    Pageable pageable);

    /***
     *
     * @param yDate1
     * @param yDate2
     * @return
     */
    @Query(value = "SELECT e FROM EndocrinologistConclusion e join fetch e.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate>=:yDate1 AND applicant.yDate<=:yDate2")
    List<EndocrinologistConclusion> findEndocrinologistConclusionByApplicantByYDate(LocalDate yDate1,LocalDate yDate2,
                                                                                    Pageable pageable);
}
