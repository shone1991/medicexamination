package uz.egov.medicinemonolithic.repository.conclusion;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.egov.medicinemonolithic.entity.conclusion.VenerologistConclusion;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface VenerologistConclusionRepository extends JpaRepository<VenerologistConclusion,Long> {
    /*Below methods for retrieving existing records of medical checkups*/

    /**
     * @param specialistId
     * @param pageable
     * @return
     */
    @Query(value = "SELECT v FROM VenerologistConclusion v join fetch v.application as application " +
            "join fetch application.applicant applicant WHERE v.specialist.id=:specialistId")
    List<VenerologistConclusion> findVenerologistConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    /**
     * @param idWorkPlace
     * @param pageable
     * @return
     */
    @Query(value = "SELECT v FROM VenerologistConclusion v join fetch v.application as application " +
            "join fetch application.applicant applicant WHERE v.workPlace.id=:idWorkPlace")
    List<VenerologistConclusion> findVenerologistConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    /**
     * @param applicationId
     * @return
     */
    @Query(value = "SELECT v FROM VenerologistConclusion v join fetch v.application as application " +
            "join fetch application.applicant applicant WHERE application.id=:applicationId")
    Optional<VenerologistConclusion> findVenerologistConclusionByApplicationId(Long applicationId);

    /**
     * @param ntky_id
     * @return
     */
    @Query(value = "SELECT v FROM VenerologistConclusion v join fetch v.application as application " +
            "join fetch application.applicant applicant WHERE applicant.ntky_id=:ntky_id")
    Optional<VenerologistConclusion> findVenerologistConclusionByApplicantNtky_id(String ntky_id);

    /**
     * @param yNumber
     * @return
     */
    @Query(value = "SELECT v FROM VenerologistConclusion v join fetch v.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yNumber=:yNumber")
    Optional<VenerologistConclusion> findVenerologistConclusionByApplicantByyNumber(String yNumber);

    /**
     * @param applicantId
     * @return
     */
    @Query(value = "SELECT v FROM VenerologistConclusion v join fetch v.application as application " +
            "join fetch application.applicant applicant WHERE applicant.id=:applicantId")
    Optional<VenerologistConclusion> findVenerologistConclusionByApplicantId(Long applicantId);

    /**
     * @param jshshir
     * @param pageable
     * @return
     */
    @Query(value = "SELECT v FROM VenerologistConclusion v join fetch v.application as application " +
            "join fetch application.applicant applicant WHERE applicant.jshshir=:jshshir")
    List<VenerologistConclusion> findVenerologistConclusionByApplicantJShshir(String jshshir, Pageable pageable);

    /**
     * @param yDate
     * @return
     */
    @Query(value = "SELECT v FROM VenerologistConclusion v join fetch v.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate=:yDate")
    List<VenerologistConclusion> findVenerologistConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    /**
     * @param yDate1
     * @param yDate2
     * @return
     */
    @Query(value = "SELECT v FROM VenerologistConclusion v join fetch v.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate>=:yDate1 AND applicant.yDate<=:yDate2")
    List<VenerologistConclusion> findVenerologistConclusionByApplicantByYDate(LocalDate yDate1, LocalDate yDate2,
                                                                              Pageable pageable);

}
