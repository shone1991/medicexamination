package uz.egov.medicinemonolithic.repository.conclusion;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.egov.medicinemonolithic.entity.conclusion.CardiologistConclusion;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface CardiologistConclusionRepository extends JpaRepository<CardiologistConclusion, Long> {

    /*Below methods for retrieving existing records of medical checkups*/

    /***
     *
     * @param specialistId
     * @return
     */
    @Query(value = "SELECT c FROM CardiologistConclusion c join fetch c.application as application " +
            "join fetch application.applicant applicant join fetch c.specialist as specialist WHERE  " +
            "specialist.id=:specialistId")
    List<CardiologistConclusion> findCardiologistConclusionsBySpecialistId(Long specialistId);

    /***
     *
     * @param idWorkPlace
     * @param pageable
     * @return
     */
//    @Query(value = "SELECT c FROM CardiologistConclusion c WHERE c.workPlace.id=:idWorkPlace")
    @Query(value = "SELECT c FROM CardiologistConclusion c join fetch c.application as application " +
            "join fetch application.applicant applicant WHERE c.workPlace.id=:idWorkPlace")
    List<CardiologistConclusion> findCardiologistConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    //    @Query(value = "SELECT c FROM CardiologistConclusion c join fetch c.application as application " +
//            "join fetch application.applicant applicant WHERE c.conclusionDate =:conclusionDate")
//    List<CardiologistConclusion> findCardiologistConclusionsByConclusionDate(Date conclusionDate);

//    @Query(value = "SELECT c FROM CardiologistConclusion c join fetch c.application as application " +
//            "join fetch application.applicant applicant WHERE c.conclusionDate >=:conclusionDate1 and c.conclusionDate <=:conclusionDate2")
//    List<CardiologistConclusion> findCardiologistConclusionsByBetweenDate1AndDate2(Date conclusionDate1, Date conclusionDate2);

//    @Query(value = "SELECT c FROM CardiologistConclusion c join fetch c.application as application " +
//            "join fetch application.applicant applicant WHERE date(c.createdAt) =:localDate")
//    List<CardiologistConclusion> findCardiologistConclusionByCreatedAt(Date localDate);

//    @Query(value = "SELECT c FROM CardiologistConclusion c join fetch c.application as application " +
//            "join fetch application.applicant applicant WHERE date(c.createdAt) >=:localDate1 and date(c.createdAt) <=:localDate2")
//    List<CardiologistConclusion> findCardiologistConclusionsByBetweenDate1AndDate2(LocalDate localDate1, LocalDate LocaDate2);

    /***
     *
     * @param applicationId
     * @return
     */
    @Query(value = "SELECT c FROM CardiologistConclusion c join fetch c.application as application " +
            "join fetch application.applicant applicant WHERE application.id=:applicationId")
    Optional<CardiologistConclusion> findCardiologistConclusionByApplicationId(Long applicationId);

    /***
     *
     * @param ntky_id
     * @return
     */
    @Query(value = "SELECT c FROM CardiologistConclusion c join fetch c.application as application " +
            "join fetch application.applicant applicant WHERE applicant.ntky_id=:ntky_id")
    Optional<CardiologistConclusion> findCardiologistConclusionByApplicantNtky_id(String ntky_id);

    /***
     *
     * @param yNumber
     * @return
     */
    @Query(value = "SELECT c FROM CardiologistConclusion c join fetch c.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yNumber=:yNumber")
    Optional<CardiologistConclusion> findCardiologistConclusionByApplicantByyNumber(String yNumber);

    /***
     *
     * @param applicantId
     * @return
     */
    @Query(value = "SELECT c FROM CardiologistConclusion c join fetch c.application as application " +
            "join fetch application.applicant applicant WHERE applicant.id=:applicantId")
    Optional<CardiologistConclusion> findCardiologistConclusionByApplicantId(Long applicantId);

    /***
     *
     * @param jshshir
     * @param pageable
     * @return
     */
    @Query(value = "SELECT c FROM CardiologistConclusion c join fetch c.application as application " +
            "join fetch application.applicant applicant WHERE applicant.jshshir=:jshshir")
    List<CardiologistConclusion> findCardiologistConclusionsByApplicantJShshir(String jshshir, Pageable pageable);

    /***
     *
     * @param yDate
     * @param pageable
     * @return
     */
    @Query(value = "SELECT c FROM CardiologistConclusion c join fetch c.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate=:yDate")
    List<CardiologistConclusion> findCardiologistConclusionsByApplicantByYDate(LocalDate yDate, Pageable pageable);

    /***
     *
     * @param yDate1
     * @param yDate2
     * @return
     */
    @Query(value = "SELECT c FROM CardiologistConclusion c join fetch c.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate>=:yDate1 AND applicant.yDate<=:yDate2")
    List<CardiologistConclusion> findCardiologistConclusionsByApplicantByYDate(LocalDate yDate1,
                                                                               LocalDate yDate2,
                                                                               Pageable pageable);

}
