package uz.egov.medicinemonolithic.repository.conclusion;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.egov.medicinemonolithic.entity.conclusion.PsychiatristConclusion;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 07-March-2022  Monday 3:03 PM
 **/
public interface PsychiatristConclusionRepository extends JpaRepository<PsychiatristConclusion, Long> {
    /*Below methods for retrieving existing records of medical checkups*/

    /**
     * BySpecialistId
     */
    @Query(value = "SELECT r FROM PsychiatristConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE r.specialist.id=:specialistId")
    List<PsychiatristConclusion> findPsychiatristConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    /**
     * ByIdWorkPlace
     */
    @Query(value = "SELECT r FROM PsychiatristConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE r.workPlace.id=:idWorkPlace")
    List<PsychiatristConclusion> findPsychiatristConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    /**
     * ByApplicationId
     */
    @Query(value = "SELECT r FROM PsychiatristConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE application.id=:applicationId")
    Optional<PsychiatristConclusion> findPsychiatristConclusionByApplicationId(Long applicationId);

    /**
     * ByApplicantNtky_id
     */
    @Query(value = "SELECT r FROM PsychiatristConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.ntky_id=:ntky_id")
    Optional<PsychiatristConclusion> findPsychiatristConclusionByApplicantNtky_id(String ntky_id);

    /**
     * ByApplicantByyNumber
     */
    @Query(value = "SELECT r FROM PsychiatristConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yNumber=:yNumber")
    Optional<PsychiatristConclusion> findPsychiatristConclusionByApplicantByyNumber(String yNumber);

    /**
     * ByApplicantId
     */
    @Query(value = "SELECT r FROM PsychiatristConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.id=:applicantId")
    Optional<PsychiatristConclusion> findPsychiatristConclusionByApplicantId(Long applicantId);

    /**
     * ByApplicantJShshir
     */
    @Query(value = "SELECT r FROM PsychiatristConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.jshshir=:jshshir")
    List<PsychiatristConclusion> findPsychiatristConclusionByApplicantJShshir(String jshshir, Pageable pageable);

    /**
     * ByApplicantByYDate
     */
    @Query(value = "SELECT r FROM PsychiatristConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate=:yDate")
    List<PsychiatristConclusion> findPsychiatristConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    /**
     * ByApplicantByYDate
     */
    @Query(value = "SELECT r FROM PsychiatristConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate>=:yDate1 AND applicant.yDate<=:yDate2")
    List<PsychiatristConclusion> findPsychiatristConclusionByApplicantByYDate(LocalDate yDate1,
                                                                                  LocalDate yDate2,
                                                                                  Pageable pageable);

}
