package uz.egov.medicinemonolithic.repository.conclusion;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.egov.medicinemonolithic.entity.conclusion.PhthisiatricianConclusion;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface PhthisiatricianConclusionRepository extends JpaRepository<PhthisiatricianConclusion, Long> {
    /*Below methods for retrieving existing records of medical checkups*/

    /**
     * @param specialistId
     * @param pageable
     * @return
     */
    @Query(value = "SELECT p FROM PhthisiatricianConclusion p join fetch p.application as application " +
            "join fetch application.applicant applicant WHERE p.specialist.id=:specialistId")
    List<PhthisiatricianConclusion> findPhthisiatricianConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    /**
     * @param idWorkPlace
     * @param pageable
     * @return
     */
    @Query(value = "SELECT p FROM PhthisiatricianConclusion p join fetch p.application as application " +
            "join fetch application.applicant applicant WHERE p.workPlace.id=:idWorkPlace")
    List<PhthisiatricianConclusion> findPhthisiatricianConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    /**
     * @param applicationId
     * @return
     */
    @Query(value = "SELECT p FROM PhthisiatricianConclusion p join fetch p.application as application " +
            "join fetch application.applicant applicant WHERE application.id=:applicationId")
    Optional<PhthisiatricianConclusion> findPhthisiatricianConclusionByApplicationId(Long applicationId);

    /**
     * @param ntky_id
     * @return
     */
    @Query(value = "SELECT p FROM PhthisiatricianConclusion p join fetch p.application as application " +
            "join fetch application.applicant applicant WHERE applicant.ntky_id=:ntky_id")
    Optional<PhthisiatricianConclusion> findPhthisiatricianConclusionByApplicantNtky_id(String ntky_id);

    /**
     * @param yNumber
     * @return
     */
    @Query(value = "SELECT p FROM PhthisiatricianConclusion p join fetch p.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yNumber=:yNumber")
    Optional<PhthisiatricianConclusion> findPhthisiatricianConclusionByApplicantByyNumber(String yNumber);

    /**
     * @param applicantId
     * @return
     */
    @Query(value = "SELECT p FROM PhthisiatricianConclusion p join fetch p.application as application " +
            "join fetch application.applicant applicant WHERE applicant.id=:applicantId")
    Optional<PhthisiatricianConclusion> findPhthisiatricianConclusionByApplicantId(Long applicantId);

    /**
     * @param jshshir
     * @param pageable
     * @return
     */
    @Query(value = "SELECT p FROM PhthisiatricianConclusion p join fetch p.application as application " +
            "join fetch application.applicant applicant WHERE applicant.jshshir=:jshshir")
    List<PhthisiatricianConclusion> findPhthisiatricianConclusionByApplicantJShshir(String jshshir, Pageable pageable);

    /**
     * @param yDate
     * @return
     */
    @Query(value = "SELECT p FROM PhthisiatricianConclusion p join fetch p.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate=:yDate")
    List<PhthisiatricianConclusion> findPhthisiatricianConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    /**
     * @param yDate1
     * @param yDate2
     * @return
     */
    @Query(value = "SELECT p FROM PhthisiatricianConclusion p join fetch p.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate>=:yDate1 AND applicant.yDate<=:yDate2")
    List<PhthisiatricianConclusion> findPhthisiatricianConclusionByApplicantByYDate(LocalDate yDate1,
                                                                                    LocalDate yDate2,
                                                                                    Pageable pageable);
}
