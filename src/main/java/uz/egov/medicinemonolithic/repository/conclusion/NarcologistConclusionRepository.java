package uz.egov.medicinemonolithic.repository.conclusion;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.egov.medicinemonolithic.entity.conclusion.NarcologistConclusion;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface NarcologistConclusionRepository extends JpaRepository<NarcologistConclusion, Long> {
    /**
     * @param specialistId
     * @return
     */
    @Query(value = "SELECT n FROM NarcologistConclusion n join fetch n.application as application " +
            "join fetch application.applicant applicant WHERE n.specialist.id=:specialistId")
    List<NarcologistConclusion> findNarcologistConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    /**
     * @param idWorkPlace
     * @return
     */
    @Query(value = "SELECT n FROM NarcologistConclusion n join fetch n.application as application " +
            "join fetch application.applicant applicant WHERE n.workPlace.id=:idWorkPlace")
    List<NarcologistConclusion> findNarcologistConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    /**
     * @param applicationId
     * @return
     */
    @Query(value = "SELECT n FROM NarcologistConclusion n join fetch n.application as application " +
            "join fetch application.applicant applicant WHERE application.id=:applicationId")
    Optional<NarcologistConclusion> findNarcologistConclusionByApplicationId(Long applicationId);

    /**
     * @param ntky_id
     * @return
     */
    @Query(value = "SELECT n FROM NarcologistConclusion n join fetch n.application as application " +
            "join fetch application.applicant applicant WHERE applicant.ntky_id=:ntky_id")
    Optional<NarcologistConclusion> findNarcologistConclusionByApplicantNtky_id(String ntky_id);

    /**
     * @param yNumber
     * @return
     */
    @Query(value = "SELECT n FROM NarcologistConclusion n join fetch n.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yNumber=:yNumber")
    Optional<NarcologistConclusion> findNarcologistConclusionByApplicantByyNumber(String yNumber);

    /**
     * @param applicantId
     * @return
     */
    @Query(value = "SELECT n FROM NarcologistConclusion n join fetch n.application as application " +
            "join fetch application.applicant applicant WHERE applicant.id=:applicantId")
    Optional<NarcologistConclusion> findNarcologistConclusionByApplicantId(Long applicantId);

    /***
     *
     * @param jshshir
     * @return
     */
    @Query(value = "SELECT n FROM NarcologistConclusion n join fetch n.application as application " +
            "join fetch application.applicant applicant WHERE applicant.jshshir=:jshshir")
    List<NarcologistConclusion> findNarcologistConclusionByApplicantJshshir(String jshshir, Pageable pageable);

    /***
     *
     * @param yDate
     * @return
     */
    @Query(value = "SELECT n FROM NarcologistConclusion n join fetch n.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate=:yDate")
    List<NarcologistConclusion> findNarcologistConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    /***
     *
     * @param yDate1
     * @param yDate2
     * @return
     */
    @Query(value = "SELECT n FROM NarcologistConclusion n join fetch n.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate>=:yDate1 AND applicant.yDate<=:yDate2")
    List<NarcologistConclusion> findNarcologistConclusionByApplicantByYDate(LocalDate yDate1, LocalDate yDate2, Pageable pageable);
}
