package uz.egov.medicinemonolithic.repository.conclusion;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.egov.medicinemonolithic.entity.conclusion.HIVCentreConclusion;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface HivCentreConclusionRepository extends JpaRepository<HIVCentreConclusion, Long> {
    /**
     * @param specialistId
     * @return
     */
    @Query(value = "SELECT h FROM HIVCentreConclusion h join fetch h.application as application " +
            "join fetch application.applicant applicant WHERE h.specialist.id=:specialistId")
    List<HIVCentreConclusion> findHIVCentreConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    /**
     * @param idWorkPlace
     * @return
     */
    @Query(value = "SELECT h FROM HIVCentreConclusion h join fetch h.application as application " +
            "join fetch application.applicant applicant WHERE h.workPlace.id=:idWorkPlace")
    List<HIVCentreConclusion> findHIVCentreConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    /**
     * @param applicationId
     * @return
     */
    @Query(value = "SELECT h FROM HIVCentreConclusion h join fetch h.application as application " +
            "join fetch application.applicant applicant WHERE application.id=:applicationId")
    Optional<HIVCentreConclusion> findHIVCentreConclusionByApplicationId(Long applicationId);

    /**
     * @param ntky_id
     * @return
     */
    @Query(value = "SELECT h FROM HIVCentreConclusion h join fetch h.application as application " +
            "join fetch application.applicant applicant WHERE applicant.ntky_id=:ntky_id")
    Optional<HIVCentreConclusion> findHIVCentreConclusionByApplicantNtky_id(String ntky_id);

    /**
     * @param yNumber
     * @return
     */
    @Query(value = "SELECT h FROM HIVCentreConclusion h join fetch h.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yNumber=:yNumber")
    Optional<HIVCentreConclusion> findHIVCentreConclusionByApplicantByyNumber(String yNumber);

    /**
     * @param applicantId
     * @return
     */
    @Query(value = "SELECT h FROM HIVCentreConclusion h join fetch h.application as application " +
            "join fetch application.applicant applicant WHERE applicant.id=:applicantId")
    Optional<HIVCentreConclusion> findHIVCentreConclusionByApplicantId(Long applicantId);

    /***
     *
     * @param jshshir
     * @return
     */
    @Query(value = "SELECT h FROM HIVCentreConclusion h join fetch h.application as application " +
            "join fetch application.applicant applicant WHERE applicant.jshshir=:jshshir")
    List<HIVCentreConclusion> findHIVCentreConclusionByApplicantJshshir(String jshshir, Pageable pageable);

    /***
     *
     * @param yDate
     * @return
     */
    @Query(value = "SELECT h FROM HIVCentreConclusion h join fetch h.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate=:yDate")
    List<HIVCentreConclusion> findHIVCentreConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    /***
     *
     * @param yDate1
     * @param yDate2
     * @return
     */
    @Query(value = "SELECT h FROM HIVCentreConclusion h join fetch h.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate>=:yDate1 AND applicant.yDate<=:yDate2")
    List<HIVCentreConclusion> findHIVCentreConclusionByApplicantByYDate(LocalDate yDate1, LocalDate yDate2, Pageable pageable);
}
