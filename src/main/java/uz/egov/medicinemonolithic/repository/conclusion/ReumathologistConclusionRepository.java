package uz.egov.medicinemonolithic.repository.conclusion;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.egov.medicinemonolithic.entity.conclusion.ReumathologistConclusion;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ReumathologistConclusionRepository extends JpaRepository<ReumathologistConclusion, Long> {
    /*Below methods for retrieving existing records of medical checkups*/

    /**
     * @param specialistId
     * @param pageable
     * @return
     */
    @Query(value = "SELECT r FROM ReumathologistConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE r.specialist.id=:specialistId")
    List<ReumathologistConclusion> findReumathologistConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    /**
     * @param idWorkPlace
     * @param pageable
     * @return
     */
    @Query(value = "SELECT r FROM ReumathologistConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE r.workPlace.id=:idWorkPlace")
    List<ReumathologistConclusion> findReumathologistConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    /**
     * @param applicationId
     * @return
     */
    @Query(value = "SELECT r FROM ReumathologistConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE application.id=:applicationId")
    Optional<ReumathologistConclusion> findReumathologistConclusionByApplicationId(Long applicationId);

    /**
     * @param ntky_id
     * @return
     */
    @Query(value = "SELECT r FROM ReumathologistConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.ntky_id=:ntky_id")
    Optional<ReumathologistConclusion> findReumathologistConclusionByApplicantNtky_id(String ntky_id);

    /**
     * @param yNumber
     * @return
     */
    @Query(value = "SELECT r FROM ReumathologistConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yNumber=:yNumber")
    Optional<ReumathologistConclusion> findReumathologistConclusionByApplicantByyNumber(String yNumber);

    /**
     * @param applicantId
     * @return
     */
    @Query(value = "SELECT r FROM ReumathologistConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.id=:applicantId")
    Optional<ReumathologistConclusion> findReumathologistConclusionByApplicantId(Long applicantId);

    /**
     * @param jshshir
     * @param pageable
     * @return
     */
    @Query(value = "SELECT r FROM ReumathologistConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.jshshir=:jshshir")
    List<ReumathologistConclusion> findReumathologistConclusionByApplicantJShshir(String jshshir, Pageable pageable);

    /**
     * @param yDate
     * @return
     */
    @Query(value = "SELECT r FROM ReumathologistConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate=:yDate")
    List<ReumathologistConclusion> findReumathologistConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    /**
     * @param yDate1
     * @param yDate2
     * @return
     */
    @Query(value = "SELECT r FROM ReumathologistConclusion r join fetch r.application as application " +
            "join fetch application.applicant applicant WHERE applicant.yDate>=:yDate1 AND applicant.yDate<=:yDate2")
    List<ReumathologistConclusion> findReumathologistConclusionByApplicantByYDate(LocalDate yDate1,
                                                                                  LocalDate yDate2,
                                                                                  Pageable pageable);

}
