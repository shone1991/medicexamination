package uz.egov.medicinemonolithic.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.egov.medicinemonolithic.entity.Applicant;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ApplicantRepository extends JpaRepository<Applicant, Long> {
    @Query(value = "SELECT a FROM Applicant a WHERE a.ntky_id=:ntky_id")
    Optional<Applicant> findApplicantByNtky_id(String ntky_id);

    @Query(value = "SELECT a FROM Applicant a WHERE a.yNumber=:yNumber")
    Optional<Applicant> findApplicantByYnumber(String yNumber);

    @Query(value = "SELECT a FROM Applicant a WHERE a.branch=:branch and date(a.yDate)=:yDate")
    Optional<List<Applicant>> findApplicantByBranchAndYDate(String branch, LocalDate yDate, Pageable pageable);


}
