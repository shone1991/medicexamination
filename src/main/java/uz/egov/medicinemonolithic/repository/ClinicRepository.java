package uz.egov.medicinemonolithic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.egov.medicinemonolithic.entity.Clinic;

import java.util.List;

/**
 * @author f.lapasov on 2/15/2022
 * @project medicine-monolithic
 */
@Repository
public interface ClinicRepository extends JpaRepository<Clinic,Long> {
    @Query("SELECT c FROM Clinic  c WHERE c.name LIKE %:name%")
    List<Clinic> findAllByNameLike(@Param("name") String name);
}
