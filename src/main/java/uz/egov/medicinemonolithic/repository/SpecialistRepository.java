package uz.egov.medicinemonolithic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.egov.medicinemonolithic.entity.Specialist;

import java.util.Optional;

/**
 * @author f.lapasov on 2/15/2022
 * @project medicine-monolithic
 */
@Repository
public interface SpecialistRepository extends JpaRepository<Specialist,Long> {
    @Query("SELECT s FROM Specialist s WHERE s.userMedicine.userDetail.pinfl=:pinfl")
    Optional<Specialist> findSpecialistByPinfl(@Param("pinfl") String pinfl);

    @Query("SELECT s FROM Specialist s where s.userMedicine.username=:username")
    Optional<Specialist> findSpecialistByUsername(@Param("username") String username);

    @Query("SELECT s FROM Specialist s where s.id=:id")
    Specialist findSpecialistById(@Param("id") Long id);

    @Query("SELECT s FROM Specialist s join fetch s.userMedicine as user where user.id=:id")
    Optional<Specialist> findSpecialistByUserId(@Param("id") Long id);

}
