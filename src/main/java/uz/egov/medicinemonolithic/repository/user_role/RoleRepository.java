package uz.egov.medicinemonolithic.repository.user_role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.egov.medicinemonolithic.entity.users.ERole;
import uz.egov.medicinemonolithic.entity.users.Role;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
  Optional<Role> findByName(ERole name);

  Boolean existsByName(ERole name);
}
