package uz.egov.medicinemonolithic.repository.user_role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.egov.medicinemonolithic.entity.users.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Optional<User> findByPhone(String phone);

    @Query("SELECT u FROM User u WHERE u.userDetail.pinfl=?1")
    Optional<User> findByPinfl(String pinfl);

    Boolean existsByPhone(String phone);

    Boolean existsByUsername(String username);

    Boolean existsByUsernameAndIsActiveTrue(String username);
}
