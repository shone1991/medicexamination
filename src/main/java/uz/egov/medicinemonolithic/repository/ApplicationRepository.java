package uz.egov.medicinemonolithic.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.egov.medicinemonolithic.entity.Application;
import uz.egov.medicinemonolithic.entity.ApplicationStatus;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 *
 * @project medicine-monolithic
 * @author f.lapasov on 2/24/2022
 */

@Repository
public interface ApplicationRepository extends JpaRepository<Application,Long> {

    @Query(value = "select a from Application a join fetch a.applicant as applicant")
    List<Application> findAllApplication(Pageable pageable);

    @Query(value = "select a from Application a join fetch a.applicant as applicant " +
            "where a.applicationStatus=:applicationStatus")
    List<Application> findAllApplicationByStatus(ApplicationStatus applicationStatus, Pageable pageable);

    @Query(value = "select a from Application a join fetch a.applicant as applicant " +
            "where a.applicationStatus=:applicationStatus and a.information_date=:dateInfo")
    List<Application> findAllApplicationByStatusAndDate(ApplicationStatus applicationStatus,
                                                        LocalDate dateInfo, Pageable pageable);

    @Query(value = "select a from Application a join fetch a.applicant as applicant " +
            "where a.applicationStatus=:status and date(a.information_date)>=:dateInfo1 and " +
            "date(a.information_date)<=:dateInfo2")
    List<Application> findAllApplicationByStatusAndDateBetween(ApplicationStatus status, LocalDate dateInfo1,
                                                               LocalDate dateInfo2, Pageable pageable);


    @Query(value = "select a from Application a join fetch a.applicant as applicant " +
            "where applicant.branch=:branch and date(a.information_date)>=:dateInfo")
    List<Application> findAllApplicationByApplicantBranchAndDate(String branch, LocalDate dateInfo, Pageable pageable);

    @Query(value = "select a from Application a join fetch a.applicant as applicant " +
            "where applicant.branch=:branch and date(a.information_date)>=:dateInfo1 and " +
            "date(a.information_date)<=:dateInfo2")
    List<Application> findAllApplicationByApplicantBranchAndDate(String branch, LocalDate dateInfo1,
                                                                 LocalDate dateInfo2, Pageable pageable);

    @Query(value = "select a from Application a join fetch a.applicant as applicant " +
            "where applicant.jshshir=:jshshir")
    List<Application> findApplicationByApplicantJshshir(String jshshir);


    @Query(value = "select a from Application a join fetch a.applicant as applicant " +
            "where a.id=:id")
    Optional<Application> findApplicationByApplicationId(Long id);

    @Query(value = "select a from Application a join fetch a.applicant as applicant " +
            "where applicant.id=:id")
    Optional<Application> findApplicationByApplicantId(Long id);

    @Query(value = "select a from Application a join fetch a.applicant as applicant " +
            "where applicant.ntky_id=:ntykId")
    Optional<Application> findApplicationByApplicantNtkyId(String ntykId);

    @Query(value = "select a from Application a join fetch a.applicant as applicant " +
            "where applicant.yNumber=:yNumber and date(applicant.yDate)=:yDate")
    Optional<Application> findApplicationByApplicantYnumber(String yNumber, LocalDate yDate);
}
