package uz.egov.medicinemonolithic.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import uz.egov.medicinemonolithic.exceptions.PageableException;

public class CommonUtils {
    public static void validatePageNumberOrSize(int page, int size) {
        if (page<0 || size<0) {
            throw new PageableException("The number of page and size can't be less than 0 !!");
        }
        if (size > ApplicationConstants.DEFAULT_MAX_SIZE) {
            throw new PageableException("The size of page can't be more than "+ApplicationConstants.DEFAULT_MAX_SIZE+" !!");
        }
    }

    public static Pageable sortedByCreatedAt(int page, int size) {
        validatePageNumberOrSize(page,size);
        return PageRequest.of(page,size, Sort.by(Sort.Direction.DESC,"createdAt"));
    }
}
