package uz.egov.medicinemonolithic.util;

public interface ApplicationConstants {
    String DEFAULT_PAGE_NUMBER = "0";
    String DEFAULT_PAGE_SIZE = "10";
    Integer DEFAULT_MAX_SIZE = 50;
}
