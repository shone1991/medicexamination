package uz.egov.medicinemonolithic.util.specialist;

import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.specialist.request.SpecialistRequestDto;
import uz.egov.medicinemonolithic.dto.specialist.response.SpecialistResponseDto;
import uz.egov.medicinemonolithic.entity.Clinic;
import uz.egov.medicinemonolithic.entity.Specialist;
import uz.egov.medicinemonolithic.entity.users.User;
import uz.egov.medicinemonolithic.repository.ClinicRepository;
import uz.egov.medicinemonolithic.repository.user_role.UserRepository;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 14-March-2022  Monday 4:33 PM
 **/
@Component
public class SpecialistUtils {
    private static UserRepository userRepository;
    private static ClinicRepository clinicRepository;

    @Autowired
    public SpecialistUtils(UserRepository userRepository, ClinicRepository clinicRepository) {
        SpecialistUtils.userRepository = userRepository;
        SpecialistUtils.clinicRepository = clinicRepository;
    }

    public static Specialist convertToEntity(SpecialistRequestDto requestDto) {
        Assert.notNull(requestDto.getUserId(), "userId cannot be null");
        Assert.notNull(requestDto.getClinicId(), "clinicId cannot be null");
        User user = userRepository.findById(requestDto.getUserId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("User not founds this id: [%d]", requestDto.getUserId())));

        Clinic clinic = clinicRepository.findById(requestDto.getClinicId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Clinic not founds this id: [%d]", requestDto.getClinicId())));
        return Specialist.builder()
                .userMedicine(user)
                .workPlace(clinic)
                .build();
    }

    public static SpecialistRequestDto convertToRequestDto(Specialist specialist) {
        Assert.notNull(specialist, "Specialist cannot be null");
        Assert.notNull(specialist.getUserMedicine(), "Specialist userId cannot be null");
        Assert.notNull(specialist.getWorkPlace(), "Specialist workPlace cannot be null");
        return SpecialistRequestDto.builder()
                .userId(specialist.getUserMedicine().getId())
                .clinicId(specialist.getWorkPlace().getId())
                .build();
    }

    public static SpecialistResponseDto convertToResponseDto(Specialist specialist) {
        Assert.notNull(specialist.getUserMedicine(), "Specialist userId not found!!");
        Assert.notNull(specialist.getUserMedicine(), "Specialist workPlaceId not found!!");
        return SpecialistResponseDto.builder()
                .id(specialist.getId())
                .userId(specialist.getUserMedicine().getId())
                .clinicId(specialist.getWorkPlace().getId())
                .build();
    }
}
