package uz.egov.medicinemonolithic.util.conclusion;

import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.conclusion.request.CardiologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.entity.Applicant;
import uz.egov.medicinemonolithic.entity.Application;
import uz.egov.medicinemonolithic.entity.Specialist;
import uz.egov.medicinemonolithic.entity.conclusion.CardiologistConclusion;
import uz.egov.medicinemonolithic.repository.ApplicationRepository;
import uz.egov.medicinemonolithic.repository.SpecialistRepository;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 03-March-2022  Thursday 5:49 PM
 **/
@Component
public class CardiologistUtils {
    private static ApplicationRepository applicationRepository;
    private static SpecialistRepository specialistRepository;

    @Autowired
    public CardiologistUtils(ApplicationRepository applicationRepository, SpecialistRepository specialistRepository) {
        CardiologistUtils.applicationRepository = applicationRepository;
        CardiologistUtils.specialistRepository = specialistRepository;
    }

    public static CardiologistConclusion convertToEntity(CardiologistConclusionRequestDto requestDto) {
        Assert.notNull(requestDto.getApplicationId(), "applicationId cannot be null");
        Assert.notNull(requestDto.getSpecialistId(), "specialistId cannot be null");
        Application application = applicationRepository.findById(requestDto.getApplicationId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Application not founds this id: [%d]", requestDto.getApplicationId())));

        Specialist specialist = specialistRepository.findById(requestDto.getSpecialistId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Specialist not founds this id: [%d]", requestDto.getApplicationId())));
        return CardiologistConclusion.builder()
                .application(application)
                .ekgResultComment(requestDto.getEkgResultComment())
                .isHealthy(requestDto.getIsHealthy())
                .examinationDetails(requestDto.getExaminationDetails())
                .specialist(specialist)
                .workPlace(specialist.getWorkPlace())
                .conclusionDate(requestDto.getConclusionDate())
                .build();
    }

    public static CardiologistConclusionRequestDto convertToRequestDto(CardiologistConclusion conclusion) {
        return CardiologistConclusionRequestDto.builder()
                .applicationId(conclusion.getApplication().getId())
                .ekgResultComment(conclusion.getEkgResultComment())
                .isHealthy(conclusion.getIsHealthy())
                .examinationDetails(conclusion.getExaminationDetails())
                .specialistId(conclusion.getSpecialist().getId())
                .conclusionDate(conclusion.getConclusionDate())
                .build();
    }

    public static ConclusionResponseDto convertToResponseDto(CardiologistConclusion conclusion) {
        Application application = conclusion.getApplication();
        Assert.notNull(application, String.format("Application not found this conclusion id [%d]!!", conclusion.getId()));
        Applicant applicant = application.getApplicant();
        return ConclusionResponseDto.builder()
                .id(conclusion.getId())
                .applicationId(application.getId())
                .nsh_FIO(applicant.getNSh_FIO())
                .nSh_BDate(applicant.getNSh_BDate())
                .branch(applicant.getBranch())
                .information_date(application.getInformation_date())
                .applicationStatus(application.getApplicationStatus())
                .comment(conclusion.getEkgResultComment())
                .build();
    }


}
