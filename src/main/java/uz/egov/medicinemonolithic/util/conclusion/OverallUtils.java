package uz.egov.medicinemonolithic.util.conclusion;

import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.conclusion.request.OverallConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.entity.Applicant;
import uz.egov.medicinemonolithic.entity.Application;
import uz.egov.medicinemonolithic.entity.Specialist;
import uz.egov.medicinemonolithic.entity.conclusion.*;
import uz.egov.medicinemonolithic.repository.ApplicationRepository;
import uz.egov.medicinemonolithic.repository.SpecialistRepository;
import uz.egov.medicinemonolithic.repository.conclusion.*;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 07-March-2022  Monday 2:51 PM
 **/
@Component
public class OverallUtils {
    private static ApplicationRepository applicationRepository;
    private static SpecialistRepository specialistRepository;
    private static EndocrinologistConclusionRepository endocrinologistConclusionRepository;
    private static CardiologistConclusionRepository cardiologistConclusionRepository;
    private static NarcologistConclusionRepository narcologistConclusionRepository;
    private static PhthisiatricianConclusionRepository phthisiatricianConclusionRepository;
    private static PsychiatristConclusionRepository psychiatristConclusionRepository;
    private static ReumathologistConclusionRepository reumathologistConclusionRepository;
    private static UrologistConclusionRepository urologistConclusionRepository;
    private static VenerologistConclusionRepository venerologistConclusionRepository;

    @Autowired
    public OverallUtils(ApplicationRepository applicationRepository, SpecialistRepository specialistRepository, EndocrinologistConclusionRepository endocrinologistConclusionRepository, CardiologistConclusionRepository cardiologistConclusionRepository, NarcologistConclusionRepository narcologistConclusionRepository, PhthisiatricianConclusionRepository phthisiatricianConclusionRepository, PsychiatristConclusionRepository psychiatristConclusionRepository, ReumathologistConclusionRepository reumathologistConclusionRepository, UrologistConclusionRepository urologistConclusionRepository, VenerologistConclusionRepository venerologistConclusionRepository) {
        OverallUtils.applicationRepository = applicationRepository;
        OverallUtils.specialistRepository = specialistRepository;
        OverallUtils.endocrinologistConclusionRepository = endocrinologistConclusionRepository;
        OverallUtils.cardiologistConclusionRepository = cardiologistConclusionRepository;
        OverallUtils.narcologistConclusionRepository = narcologistConclusionRepository;
        OverallUtils.phthisiatricianConclusionRepository = phthisiatricianConclusionRepository;
        OverallUtils.psychiatristConclusionRepository = psychiatristConclusionRepository;
        OverallUtils.reumathologistConclusionRepository = reumathologistConclusionRepository;
        OverallUtils.urologistConclusionRepository = urologistConclusionRepository;
        OverallUtils.venerologistConclusionRepository = venerologistConclusionRepository;
    }

    public static OverallConclusion convertToEntity(OverallConclusionRequestDto requestDto) {
        Assert.notNull(requestDto.getApplicationId(), "applicationId cannot be null");
        Assert.notNull(requestDto.getSpecialistId(), "specialistId cannot be null");

        Application application = applicationRepository.findById(requestDto.getApplicationId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Application not founds this id: [%d]", requestDto.getApplicationId())));

        Specialist specialist = specialistRepository.findById(requestDto.getSpecialistId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Specialist not founds this id: [%d]", requestDto.getApplicationId())));

        EndocrinologistConclusion endocrinologistConclusion = endocrinologistConclusionRepository.findById(
                requestDto.getSpecialistId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("EndocrinologistConclusion not founds this id: [%d]", requestDto.getApplicationId())));

        CardiologistConclusion cardiologistConclusion = cardiologistConclusionRepository.findById(
                requestDto.getSpecialistId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("CardiologistConclusion not founds this id: [%d]", requestDto.getApplicationId())));

        NarcologistConclusion narcologistConclusion = narcologistConclusionRepository.findById(
                requestDto.getSpecialistId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("NarcologistConclusion not founds this id: [%d]", requestDto.getApplicationId())));

        PhthisiatricianConclusion phthisiatricianConclusion = phthisiatricianConclusionRepository.findById(
                requestDto.getSpecialistId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("PhthisiatricianConclusion not founds this id: [%d]", requestDto.getApplicationId())));

        PsychiatristConclusion psychiatristConclusion = psychiatristConclusionRepository.findById(
                requestDto.getSpecialistId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("PsychiatristConclusion not founds this id: [%d]", requestDto.getApplicationId())));

        ReumathologistConclusion reumathologistConclusion = reumathologistConclusionRepository.findById(
                requestDto.getSpecialistId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("ReumathologistConclusion not founds this id: [%d]", requestDto.getApplicationId())));

        UrologistConclusion urologistConclusion = urologistConclusionRepository.findById(
                requestDto.getSpecialistId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("UrologistConclusion not founds this id: [%d]", requestDto.getApplicationId())));

        VenerologistConclusion venerologistConclusion = venerologistConclusionRepository.findById(
                requestDto.getSpecialistId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("VenerologistConclusion not founds this id: [%d]", requestDto.getApplicationId())));

        return OverallConclusion.builder()
                .application(application)
                .endocrinologistConclusion(endocrinologistConclusion)
                .cardiologistConclusion(cardiologistConclusion)
                .narcologistConclusion(narcologistConclusion)
                .phthisiatricianConclusion(phthisiatricianConclusion)
                .psychiatristConclusion(psychiatristConclusion)
                .reumathologistConclusion(reumathologistConclusion)
                .urologistConclusion(urologistConclusion)
                .venerologistConclusion(venerologistConclusion)
                .specialist(specialist)
                .workPlace(specialist.getWorkPlace())
                .comment(requestDto.getComment())
                .numberConclusion(requestDto.getNumberConclusion())
                .conclusionDate(requestDto.getConclusionDate())
                .conclusionValidDate(requestDto.getConclusionValidDate())
                .explanationDate(requestDto.getExplanationDate())
                .build();
    }

    public static OverallConclusionRequestDto convertToRequestDto(OverallConclusion conclusion) {
        return OverallConclusionRequestDto.builder()
                .applicationId(conclusion.getApplication().getId())
                .endocrinologistConclusionId(conclusion.getEndocrinologistConclusion().getId())
                .cardiologistConclusionId(conclusion.getCardiologistConclusion().getId())
                .narcologistConclusionId(conclusion.getNarcologistConclusion().getId())
                .phthisiatricianConclusionId(conclusion.getPhthisiatricianConclusion().getId())
                .psychiatristConclusionId(conclusion.getPsychiatristConclusion().getId())
                .reumathologistConclusionId(conclusion.getReumathologistConclusion().getId())
                .urologistConclusionId(conclusion.getUrologistConclusion().getId())
                .venerologistConclusionId(conclusion.getVenerologistConclusion().getId())
                .specialistId(conclusion.getSpecialist().getId())
                .comment(conclusion.getComment())
                .numberConclusion(conclusion.getNumberConclusion())
                .conclusionDate(conclusion.getConclusionDate())
                .conclusionValidDate(conclusion.getConclusionValidDate())
                .explanationDate(conclusion.getExplanationDate())
                .build();
    }

    public static ConclusionResponseDto convertToResponseDto(OverallConclusion conclusion) {
        Application application = conclusion.getApplication();
        Assert.notNull(application, String.format("Application not found this conclusion id [%d]!!", conclusion.getId()));
        Applicant applicant = application.getApplicant();
        return ConclusionResponseDto.builder()
                .id(conclusion.getId())
                .applicationId(application.getId())
                .nsh_FIO(applicant.getNSh_FIO())
                .nSh_BDate(applicant.getNSh_BDate())
                .branch(applicant.getBranch())
                .information_date(application.getInformation_date())
                .applicationStatus(application.getApplicationStatus())
                .comment(conclusion.getComment())
                .build();
    }


}
