package uz.egov.medicinemonolithic.util.conclusion;

import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.conclusion.request.PsychiatristConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.entity.Applicant;
import uz.egov.medicinemonolithic.entity.Application;
import uz.egov.medicinemonolithic.entity.Specialist;
import uz.egov.medicinemonolithic.entity.conclusion.PsychiatristConclusion;
import uz.egov.medicinemonolithic.repository.ApplicationRepository;
import uz.egov.medicinemonolithic.repository.SpecialistRepository;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 07-March-2022  Monday 3:20 PM
 **/
@Component
public class PsychiatristUtils {
    private static ApplicationRepository applicationRepository;
    private static SpecialistRepository specialistRepository;

    @Autowired
    public PsychiatristUtils(ApplicationRepository applicationRepository, SpecialistRepository specialistRepository) {
        PsychiatristUtils.applicationRepository = applicationRepository;
        PsychiatristUtils.specialistRepository = specialistRepository;
    }

    public static PsychiatristConclusion convertToEntity(PsychiatristConclusionRequestDto requestDto) {
        Assert.notNull(requestDto.getApplicationId(), "applicationId cannot be null");
        Assert.notNull(requestDto.getSpecialistId(), "specialistId cannot be null");

        Application application = applicationRepository.findById(requestDto.getApplicationId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Application not founds this id: [%d]", requestDto.getApplicationId())));

        Specialist specialist = specialistRepository.findById(requestDto.getSpecialistId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Specialist not founds this id: [%d]", requestDto.getApplicationId())));
        return PsychiatristConclusion.builder()
                .application(application)
                .isRegistered(requestDto.getIsRegistered())
                .certificate(requestDto.getCertificate())
                .hasPsychoDisorders(requestDto.getHasPsychoDisorders())
                .comment(requestDto.getComment())
                .specialist(specialist)
                .workPlace(specialist.getWorkPlace())
                .conclusionDate(requestDto.getConclusionDate())
                .build();
    }

    public static PsychiatristConclusionRequestDto convertToRequestDto(PsychiatristConclusion conclusion) {
        return PsychiatristConclusionRequestDto.builder()
                .applicationId(conclusion.getApplication().getId())
                .isRegistered(conclusion.isRegistered())
                .certificate(conclusion.getCertificate())
                .hasPsychoDisorders(conclusion.isHasPsychoDisorders())
                .comment(conclusion.getComment())
                .specialistId(conclusion.getSpecialist().getId())
                .conclusionDate(conclusion.getConclusionDate())
                .build();
    }

    public static ConclusionResponseDto convertToResponseDto(PsychiatristConclusion conclusion) {
        Application application = conclusion.getApplication();
        Assert.notNull(application, String.format("Application not found this conclusion id [%d]!!", conclusion.getId()));
        Applicant applicant = application.getApplicant();
        return ConclusionResponseDto.builder()
                .id(conclusion.getId())
                .applicationId(application.getId())
                .nsh_FIO(applicant.getNSh_FIO())
                .nSh_BDate(applicant.getNSh_BDate())
                .branch(applicant.getBranch())
                .information_date(application.getInformation_date())
                .applicationStatus(application.getApplicationStatus())
                .comment(conclusion.getComment())
                .build();
    }

}
