package uz.egov.medicinemonolithic.util.conclusion;

import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.conclusion.request.VenerologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.entity.Applicant;
import uz.egov.medicinemonolithic.entity.Application;
import uz.egov.medicinemonolithic.entity.Specialist;
import uz.egov.medicinemonolithic.entity.conclusion.VenerologistConclusion;
import uz.egov.medicinemonolithic.repository.ApplicationRepository;
import uz.egov.medicinemonolithic.repository.SpecialistRepository;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 28-February-2022  Monday 6:30 PM
 **/
@Component
public class VenerologistUtils {
    private static ApplicationRepository applicationRepository;
    private static SpecialistRepository specialistRepository;

    @Autowired
    public VenerologistUtils(ApplicationRepository applicationRepository, SpecialistRepository specialistRepository) {
        VenerologistUtils.applicationRepository = applicationRepository;
        VenerologistUtils.specialistRepository = specialistRepository;
    }

    public static VenerologistConclusion convertToEntity(VenerologistConclusionRequestDto requestDto) {
        Assert.notNull(requestDto.getApplicationId(), "applicationId cannot be null");
        Assert.notNull(requestDto.getSpecialistId(), "specialistId cannot be null");

        Application application = applicationRepository.findById(requestDto.getApplicationId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Application not founds this id: [%d]", requestDto.getApplicationId())));

        Specialist specialist = specialistRepository.findById(requestDto.getSpecialistId()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Specialist not founds this id: [%d]", requestDto.getApplicationId())));
        return VenerologistConclusion.builder()
                .application(application)
                .specialist(specialist)
                .workPlace(specialist.getWorkPlace())
//                .certificate(requestDto.getCertificate())
                .comment(requestDto.getComment())
                .hasSiphilis(requestDto.getHasSiphilis())
                .foundOutDate(requestDto.getFoundOutDate())
                .conclusionDate(requestDto.getConclusionDate())
                .build();
    }

    public static VenerologistConclusionRequestDto convertToRequestDto(VenerologistConclusion conclusion) {
        return VenerologistConclusionRequestDto.builder()
                .applicationId(conclusion.getApplication().getId())
                .hasSiphilis(conclusion.getHasSiphilis())
                .foundOutDate(conclusion.getFoundOutDate())
//                .certificate(conclusion.getCertificate())
                .comment(conclusion.getComment())
                .specialistId(conclusion.getSpecialist().getId())
                .conclusionDate(conclusion.getConclusionDate())
                .build();
    }

    public static ConclusionResponseDto convertToResponseDto(VenerologistConclusion conclusion) {
        Application application = conclusion.getApplication();
        Assert.notNull(application, String.format("Application not found this conclusion id [%d]!!", conclusion.getId()));
        Applicant applicant = application.getApplicant();
        return ConclusionResponseDto.builder()
                .id(conclusion.getId())
                .applicationId(application.getId())
                .nsh_FIO(applicant.getNSh_FIO())
                .nSh_BDate(applicant.getNSh_BDate())
                .branch(applicant.getBranch())
                .information_date(application.getInformation_date())
                .applicationStatus(application.getApplicationStatus())
                .comment(conclusion.getComment())
                .build();
    }
}
