package uz.egov.medicinemonolithic.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import uz.egov.medicinemonolithic.dto.specialist.request.SpecialistRequestDto;
import uz.egov.medicinemonolithic.dto.specialist.response.SpecialistResponseDto;
import uz.egov.medicinemonolithic.service.specialist.SpecialistService;
import uz.egov.medicinemonolithic.util.ApplicationConstants;

import java.util.List;

/**
 * @Author: Saidov Og'abek
 * @Created: 17-February-2022  Thursday 6:33 PM
 **/
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/specialist")
public class SpecialistController {
    private final SpecialistService service;

    @PostMapping("/save")
    public SpecialistResponseDto saveSpecialist(@RequestBody SpecialistRequestDto requestDto) {
        return service.saveSpecialist(requestDto);
    }

    @PutMapping("/update/{id}")
    public SpecialistResponseDto updateSpecialist(@PathVariable Long id, @RequestBody SpecialistRequestDto requestDto) {
        return service.updateSpecialist(id, requestDto);
    }

    @GetMapping("/getById/{id}")
    public SpecialistResponseDto getById(@PathVariable Long id) {
        return service.findById(id);
    }

    @GetMapping("/getByPinfl/{pinfl}")
    public SpecialistResponseDto getByPinfl(@PathVariable String pinfl) {
        return service.getByPinfl(pinfl);
    }

    @GetMapping("/getByUsername/{username}")
    public SpecialistResponseDto getByUsername(@PathVariable String username) {
        return service.getByUsername(username);
    }

    @GetMapping("/getAll")
    public List<SpecialistResponseDto> getAll(
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        return service.getAll(PageRequest.of(page, size));
    }
}
