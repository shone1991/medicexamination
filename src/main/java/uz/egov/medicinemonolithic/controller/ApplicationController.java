package uz.egov.medicinemonolithic.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import uz.egov.medicinemonolithic.dto.application.request.ApplicationExchangeDto;
import uz.egov.medicinemonolithic.dto.application.request.ApplicationRequestDto;
import uz.egov.medicinemonolithic.entity.Application;
import uz.egov.medicinemonolithic.entity.ApplicationStatus;
import uz.egov.medicinemonolithic.service.application.ApplicationService;
import uz.egov.medicinemonolithic.util.ApplicationConstants;

import java.time.LocalDate;
import java.util.List;

/**
 * @author f.lapasov on 2/8/2022
 * @project medicine
 */
@RestController
@RequestMapping("api/application")
@RequiredArgsConstructor
public class ApplicationController {

    private final ApplicationService applicationService;

    @PostMapping("/save")
    public Application saveApplication(@RequestBody Application application) {
        return applicationService.saveApplication(application);
    }

    @PostMapping("/saveFromExchange")
    public Application saveApplicationFromExchange(@RequestBody ApplicationExchangeDto applicationExchangeDto) {
        return applicationService.saveApplicationFromExchange(applicationExchangeDto);
    }

    @PostMapping("/saveFromDto")
    public Application saveApplicationFromDto(@RequestBody ApplicationRequestDto applicationRequestDto) {
        return applicationService.saveApplicationFromDto(applicationRequestDto);
    }


    @PostMapping("/getAll")
    List<Application> findAllApplication(
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        return applicationService.findAllApplication(PageRequest.of(page, size));
    }

    @GetMapping("/getByStatus/{status}")
    List<Application> findAllApplicationByStatus(
            @PathVariable ApplicationStatus status,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        return applicationService.findAllApplicationByStatus(status, PageRequest.of(page, size));
    }

    @GetMapping("/getByStatusAndDate/{status}")
    List<Application> findAllApplicationByStatusAndDate(
            @PathVariable ApplicationStatus status,
            @RequestParam LocalDate dateInfo,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        return applicationService.findAllApplicationByStatusAndDate(status, dateInfo, PageRequest.of(page, size));
    }

    @GetMapping("/getByStatusAndDateBetween/{status}")
    List<Application> findAllApplicationByStatusAndDateBetween(
            @PathVariable ApplicationStatus status,
            @RequestParam LocalDate dateInfo1,
            @RequestParam LocalDate dateInfo2,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        return applicationService.findAllApplicationByStatusAndDateBetween(
                status, dateInfo1, dateInfo2, PageRequest.of(page, size)
        );
    }

    @GetMapping("/getByApplicantBranchAndDate/{branch}")
    List<Application> findAllApplicationByApplicantBranchAndDate(@PathVariable String branch,
                                                                 @RequestParam LocalDate dateInfo,
                                                                 @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                                                 @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        return applicationService.findAllApplicationByApplicantBranchAndDate(branch, dateInfo, PageRequest.of(page, size));
    }

    @GetMapping("/getByApplicantBranchAndDateBetween/{branch}")
    List<Application> findAllApplicationByApplicantBranchAndDateBetween(@PathVariable String branch,
                                                                        @RequestParam LocalDate dateInfo1,
                                                                        @RequestParam LocalDate dateInfo2,
                                                                        @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
                                                                        @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        return applicationService.findAllApplicationByApplicantBranchAndDateBetween(branch, dateInfo1,
                dateInfo2, PageRequest.of(page, size));
    }

    @GetMapping("/getByApplicantJshshir/{jshshir}")
    List<Application> findAllApplicationByApplicantJshshir(@PathVariable String jshshir) {
        return applicationService.findApplicationByApplicantJshshir(jshshir);
    }

    @GetMapping("/getByApplicationId/{applicationId}")
    Application findApplicationByApplicationId(@PathVariable Long applicationId) {
        return applicationService.findApplicationByApplicationId(applicationId);
    }

    @GetMapping("/getByApplicantId/{applicantId}")
    Application findApplicationByApplicantId(@PathVariable Long applicantId) {
        return applicationService.findApplicationByApplicantId(applicantId);
    }

    @GetMapping("/getByApplicantNtkyId/{applicantNtkyId}")
    Application findApplicationByApplicantNtkyId(@PathVariable String applicantNtkyId) {
        return applicationService.findApplicationByApplicantNtkyId(applicantNtkyId);
    }

    @GetMapping("/getByApplicantYNumber/{applicantYNumber}")
    Application findApplicationByApplicantYNumber(@PathVariable String applicantYNumber,
                                                  @RequestParam LocalDate yDate) {
        return applicationService.findApplicationByApplicantYNumber(applicantYNumber, yDate);
    }

    @PutMapping("/updateApplication/{idApplication}")
    Application updateApplication(@PathVariable Long idApplication,
                                  @RequestBody Application application) {
        return applicationService.updateApplication(idApplication, application);
    }

    @PutMapping("/updateApplicationStatus/{idApplication}")
    Application updateApplicationStatus(@PathVariable Long idApplication,
                                  @RequestParam ApplicationStatus status) {
        return applicationService.updateApplication(idApplication, status);
    }

}
