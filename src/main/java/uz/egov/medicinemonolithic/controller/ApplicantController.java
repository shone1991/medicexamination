package uz.egov.medicinemonolithic.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import uz.egov.medicinemonolithic.dto.exchange.request.MinJusticeToMinHealthRequest;
import uz.egov.medicinemonolithic.entity.Applicant;
import uz.egov.medicinemonolithic.service.applicant.ApplicantService;
import uz.egov.medicinemonolithic.util.ApplicationConstants;

import java.time.LocalDate;
import java.util.List;

/**
 * @author f.lapasov on 2/8/2022
 * @project medicine
 */
@RestController
@RequestMapping("api/applicant")
@RequiredArgsConstructor
public class ApplicantController {
    private final ApplicantService applicantService;

    @PostMapping("/save")
    public Applicant saveApplicant(@RequestBody Applicant applicant) {
        return applicantService.addApplicant(applicant);
    }

    @PostMapping("/save/dto")
    public Applicant saveApplicantFromDto(@RequestBody MinJusticeToMinHealthRequest minJusticeToMinHealthRequest) {
        return applicantService.saveFromDto(minJusticeToMinHealthRequest);
    }

    @PutMapping("/update/{id}")
    public Applicant updateApplicant(@PathVariable Long id, @RequestBody Applicant applicant) {
        return applicantService.updateApplicant(id, applicant);
    }

    /**
     * Get pageable list of applicants
     **/
    @GetMapping("getAll")
    public Page<Applicant> getApplicantList(@RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER)
                                                    Integer page,
                                            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE)
                                                    Integer size) {
        return applicantService.getAllApplicants(page, size);
    }

    @GetMapping("getById/{id}")
    public Applicant getApplicantById(@PathVariable Long id) {
        return applicantService.getApplicantById(id);
    }

    @GetMapping("getByNtykId/{ntyk_id}")
    public Applicant getApplicantByNtykId(@PathVariable String ntyk_id) {
        return applicantService.getApplicantByNtykId(ntyk_id);
    }

    @GetMapping("getByYNumber/{yNumber}")
    public Applicant getApplicantByYNumber(@PathVariable String yNumber) {
        return applicantService.getApplicantByYNumber(yNumber);
    }

    @GetMapping("getByBranchAndDate/{branch}")
    public List<Applicant> getAllApplicantsByBranchAndDate(
            @PathVariable String branch,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size,
            @RequestParam LocalDate yDate) {

        return applicantService.getAllApplicantsByBranchAndDate(branch, yDate, PageRequest.of(page,size));
    }

}
