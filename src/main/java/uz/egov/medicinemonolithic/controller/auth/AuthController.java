package uz.egov.medicinemonolithic.controller.auth;

import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.egov.medicinemonolithic.dto.auth.request.LoginRequest;
import uz.egov.medicinemonolithic.dto.auth.request.SignupRequest;
import uz.egov.medicinemonolithic.entity.Clinic;
import uz.egov.medicinemonolithic.entity.users.ERole;
import uz.egov.medicinemonolithic.entity.users.Role;
import uz.egov.medicinemonolithic.service.auth.AuthService;
import uz.egov.medicinemonolithic.service.clinic.ClinicService;
import uz.egov.medicinemonolithic.service.user.role.RoleService;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {
    private static final Logger LOGGER = LogManager.getLogger();

    private final AuthService authService;
    private final RoleService roleService;
    private final ClinicService clinicService;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        return ResponseEntity.ok(authService.login(loginRequest));
    }

    @PostMapping("/signUp")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        return authService.signUp(signUpRequest);
    }

    @PostConstruct
    private void initRoles(){
        if(roleService.getRoles().isEmpty()){
            Role roleAdmin =new Role(ERole.ROLE_ADMIN);
            Role roleUser =new Role(ERole.ROLE_USER);
            Role roleModerator =new Role(ERole.ROLE_MODERATOR);
            Role roleCardio =new Role(ERole.ROLE_CARDIOLOGIST);
            Role roleEndo =new Role(ERole.ROLE_ENDOCRINOLOGIST);
            Role roleHiv =new Role(ERole.ROLE_HIVCENTRE);
            Role rolePhthisiat =new Role(ERole.ROLE_PHTHISIATRICIAN);
            Role roleNarco =new Role(ERole.ROLE_NARCOLOGIST);
            Role rolePsychiat =new Role(ERole.ROLE_PSYCHIATRIST);
            Role roleReuma =new Role(ERole.ROLE_REUMATHOLOGIST);
            Role roleUrolog =new Role(ERole.ROLE_UROLOGIST);
            Role roleVener =new Role(ERole.ROLE_VENEROLOGIST);
            Role roleHeadPhisician =new Role(ERole.ROLE_HEAD_PHYSICIAN);

            roleService.saveRole(roleAdmin);
            roleService.saveRole(roleEndo);
            roleService.saveRole(roleCardio);
            roleService.saveRole(roleNarco);
            roleService.saveRole(roleReuma);
            roleService.saveRole(roleHeadPhisician);
            roleService.saveRole(roleModerator);
            roleService.saveRole(roleHiv);
            roleService.saveRole(rolePhthisiat);
            roleService.saveRole(rolePsychiat);
            roleService.saveRole(roleUrolog);
            roleService.saveRole(roleUser);
            roleService.saveRole(roleVener);

            LOGGER.info("roleService.saveRole(roleAdmin);\n" +
                    "            roleService.saveRole(roleEndo);\n" +
                    "            roleService.saveRole(roleCardio);\n" +
                    "            roleService.saveRole(roleNarco);\n" +
                    "            roleService.saveRole(roleReuma);\n" +
                    "            roleService.saveRole(roleHeadPhisician);\n" +
                    "            roleService.saveRole(roleModerator);\n" +
                    "            roleService.saveRole(roleHiv);\n" +
                    "            roleService.saveRole(rolePhthisiat);\n" +
                    "            roleService.saveRole(rolePsychiat);\n" +
                    "            roleService.saveRole(roleUrolog);\n" +
                    "            roleService.saveRole(roleUser);\n" +
                    "            roleService.saveRole(roleVener);");



        }
        if(clinicService.getAll().isEmpty()){
            Clinic clinic=new Clinic("ECMA");
            clinicService.save(clinic);
        }
        LOGGER.info("PostConstruct: Add all roles to db");
    }
}
