package uz.egov.medicinemonolithic.controller.conclusion;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.egov.medicinemonolithic.dto.conclusion.request.NarcologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.service.conclusion.narco.NarcologistConclusionService;
import uz.egov.medicinemonolithic.util.ApplicationConstants;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author: Saidov Og'abek
 * @Created: 21-February-2022  Monday 11:53 AM
 **/
@RestController
@RequestMapping("/api/narcologist")
@RequiredArgsConstructor
@PreAuthorize("hasAnyRole('ROLE_NARCOLOGIST', 'ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_HEAD_PHYSICIAN')")
public class NarcologistConclusionController {
    private final NarcologistConclusionService narcologistConclusionService;

    @PostMapping("/save")
    @PreAuthorize("hasRole('ROLE_NARCOLOGIST')")
    public ConclusionResponseDto saveNarcologistConclusion(@RequestBody NarcologistConclusionRequestDto conclusionDto) {
        return narcologistConclusionService.save(conclusionDto);
    }

    @PutMapping("/update/{id}")
    @PreAuthorize("hasRole('ROLE_NARCOLOGIST')")
    public ConclusionResponseDto updateNarcologistConclusion(@PathVariable Long id,
                                                             @RequestBody NarcologistConclusionRequestDto conclusion) {
        return narcologistConclusionService.update(id, conclusion);
    }

    @GetMapping("/getById/{id}")
    public ConclusionResponseDto getById(@PathVariable("id") Long id) {
        return narcologistConclusionService.getUrologistConclusionById(id);
    }

    @GetMapping("/getAll")
    public List<ConclusionResponseDto> getAll(
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        return narcologistConclusionService.getAllUrologistConclusions(PageRequest.of(page, size));
    }

    @GetMapping("/getBySpecialistId/{specialistId}")
    public List<ConclusionResponseDto> getByConclusionId(
            @PathVariable("specialistId") Long id,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return narcologistConclusionService.getNarcologistConclusionsBySpecialistId(id, PageRequest.of(page, size));
    }

    @GetMapping("/getByWorkPlaceId/{workPlaceId}")
    public List<ConclusionResponseDto> getByWorkPlaceId(
            @PathVariable("workPlaceId") Long id,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return narcologistConclusionService.getNarcologistConclusionsByIdWorkPlace(id, PageRequest.of(page, size));
    }

    @GetMapping("/getByApplicationId/{applicationId}")
    public ConclusionResponseDto getByApplicationId(@PathVariable("applicationId") Long id) {
        return narcologistConclusionService.getNarcologistConclusionByApplicationId(id);
    }

    @GetMapping("/getByApplicantNtkyId/{ntkyId}")
    public ConclusionResponseDto getByApplicantNtkyId(@PathVariable("ntkyId") String ntkyId) {
        return narcologistConclusionService.getNarcologistConclusionByApplicantNtky_id(ntkyId);
    }

    @GetMapping("/getByYNumber/{yNumber}")
    public ConclusionResponseDto getByYNumber(@PathVariable("yNumber") String yNumber) {
        return narcologistConclusionService.getNarcologistConclusionByApplicantByyNumber(yNumber);
    }

    @GetMapping("/getByApplicantId/{applicantId}")
    public ConclusionResponseDto getByApplicantId(@PathVariable("applicantId") Long id) {
        return narcologistConclusionService.getNarcologistConclusionByApplicantId(id);
    }

    @GetMapping("/getByJshshir/{jshshir}")
    public List<ConclusionResponseDto> getByJshshir(
            @PathVariable("jshshir") String jshshir,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return narcologistConclusionService.getNarcologistConclusionByApplicantJshshir(jshshir, PageRequest.of(page, size));
    }

    @GetMapping("/getByYDate")
    public List<ConclusionResponseDto> getByYDate(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return narcologistConclusionService.getNarcologistConclusionByApplicantByYDate(yDate, PageRequest.of(page, size));
    }

    @GetMapping("/getByBetweenYDate")
    public List<ConclusionResponseDto> findByYDate(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate1,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate2,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return narcologistConclusionService.getNarcologistConclusionByApplicantByYDate(yDate1, yDate2, PageRequest.of(page, size));
    }

}
