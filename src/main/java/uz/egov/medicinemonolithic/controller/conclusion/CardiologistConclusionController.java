package uz.egov.medicinemonolithic.controller.conclusion;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.egov.medicinemonolithic.dto.conclusion.request.CardiologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.service.conclusion.cardio.CardiologistConclusionService;
import uz.egov.medicinemonolithic.util.ApplicationConstants;

import java.time.LocalDate;
import java.util.List;

/**
 * @author f.lapasov on 2/14/2022
 * @project medicine-monolithic
 */
@RestController
@RequestMapping("/api/cardiologist")
@RequiredArgsConstructor
@PreAuthorize("hasAnyRole('ROLE_CARDIOLOGIST', 'ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_HEAD_PHYSICIAN')")
public class CardiologistConclusionController {
    private final CardiologistConclusionService cardiologistConclusionService;

    @PostMapping("/save")
    @PreAuthorize("hasRole('ROLE_CARDIOLOGIST')")
    public ConclusionResponseDto saveCardiologistConclusion(@RequestBody CardiologistConclusionRequestDto requestDto) {
        return cardiologistConclusionService.saveCardiologistConclusion(requestDto);
    }

    @PutMapping("/update/{id}")
    @PreAuthorize("hasRole('ROLE_CARDIOLOGIST')")
    public ConclusionResponseDto updateCardiologistConclusion(@PathVariable Long id,
                                                              @RequestBody CardiologistConclusionRequestDto requestDto) {
        return cardiologistConclusionService.update(id, requestDto);
    }

    @GetMapping("/getById/{id}")
    public ConclusionResponseDto getById(@PathVariable("id") Long id) {
        return cardiologistConclusionService.getConclusionConclusionById(id);
    }

    @GetMapping("/getAll")
    public List<ConclusionResponseDto> getAll(
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        return cardiologistConclusionService.getAllConclusionConclusions(PageRequest.of(page, size));
    }

    @GetMapping("/getBySpecialistId/{specialistId}")
    public List<ConclusionResponseDto> getByConclusionId(
            @PathVariable("specialistId") Long id,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return cardiologistConclusionService.getCardioConclusionsBySpecialistId(id, PageRequest.of(page, size));
    }

    @GetMapping("/getByWorkPlaceId/{workPlaceId}")
    public List<ConclusionResponseDto> getByWorkPlaceId(
            @PathVariable("workPlaceId") Long id,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return cardiologistConclusionService.getCardioConclusionsByIdWorkPlace(id, PageRequest.of(page, size));
    }

    @GetMapping("/getByApplicationId/{applicationId}")
    public ConclusionResponseDto getByApplicationId(@PathVariable("applicationId") Long id) {
        return cardiologistConclusionService.getCardioConclusionByApplicationId(id);
    }

    @GetMapping("/getByApplicantNtkyId/{ntkyId}")
    public ConclusionResponseDto getByApplicantNtkyId(@PathVariable("ntkyId") String ntkyId) {
        return cardiologistConclusionService.getCardioConclusionByApplicantNtkyId(ntkyId);
    }

    @GetMapping("/getByYNumber/{yNumber}")
    public ConclusionResponseDto getByYNumber(@PathVariable("yNumber") String yNumber) {
        return cardiologistConclusionService.getCardioConclusionByApplicantByyNumber(yNumber);
    }

    @GetMapping("/getByApplicantId/{applicantId}")
    public ConclusionResponseDto getByApplicantId(@PathVariable("applicantId") Long id) {
        return cardiologistConclusionService.getCardioConclusionByApplicantId(id);
    }

    @GetMapping("/getByJshshir/{jshshir}")
    public List<ConclusionResponseDto> getByJshshir(
            @PathVariable("jshshir") String jshshir,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return cardiologistConclusionService.getCardioConclusionsByApplicantJshshir(jshshir, PageRequest.of(page, size));
    }

    @GetMapping("/getByYDate")
    public List<ConclusionResponseDto> getByYDate(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return cardiologistConclusionService.getCardioConclusionsByApplicantByYDate(yDate, PageRequest.of(page, size));
    }

    @GetMapping("/getByBetweenYDate")
    public List<ConclusionResponseDto> findByYDate(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate1,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate2,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return cardiologistConclusionService.findCardiologistConclusionsByApplicantByYDate(yDate1, yDate2,
                PageRequest.of(page, size));
    }
}
