package uz.egov.medicinemonolithic.controller.conclusion;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.egov.medicinemonolithic.dto.conclusion.request.PsychiatristConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.service.conclusion.psychiatrist.PsychiatristConclusionService;
import uz.egov.medicinemonolithic.util.ApplicationConstants;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 07-March-2022  Monday 3:25 PM
 **/
@RestController
@RequestMapping("/api/psychiatrist")
@RequiredArgsConstructor
@PreAuthorize("hasAnyRole('ROLE_PSYCHIATRIST', 'ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_HEAD_PHYSICIAN')")
public class PsychiatristConclusionController {
    private final PsychiatristConclusionService psychiatristConclusionService;
    
    @PostMapping("/save")
    @PreAuthorize("hasRole('ROLE_PSYCHIATRIST')")
    public ConclusionResponseDto savePsychiatristConclusion(@RequestBody PsychiatristConclusionRequestDto conclusionDto) {
        return psychiatristConclusionService.save(conclusionDto);
    }

    @PutMapping("/update/{id}")
    @PreAuthorize("hasRole('ROLE_PSYCHIATRIST')")
    public ConclusionResponseDto updatePsychiatristConclusion(
            @PathVariable Long id,
            @RequestBody PsychiatristConclusionRequestDto conclusionDto) {
        return psychiatristConclusionService.update(id, conclusionDto);
    }

    @GetMapping("/getById/{id}")
    public ConclusionResponseDto getById(@PathVariable("id") Long id) {
        return psychiatristConclusionService.getPsychiatristConclusionById(id);
    }

    @GetMapping("/getAll")
    public List<ConclusionResponseDto> getAll(
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        return psychiatristConclusionService.getAllPsychiatristConclusions(PageRequest.of(page, size));
    }

    @GetMapping("/getBySpecialistId/{specialistId}")
    public List<ConclusionResponseDto> getByConclusionId(
            @PathVariable("specialistId") Long id,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return psychiatristConclusionService.getPsychiatristConclusionsBySpecialistId(id, PageRequest.of(page, size));
    }

    @GetMapping("/getByWorkPlaceId/{workPlaceId}")
    public List<ConclusionResponseDto> getByWorkPlaceId(
            @PathVariable("workPlaceId") Long id,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return psychiatristConclusionService.getPsychiatristConclusionsByIdWorkPlace(id, PageRequest.of(page, size));
    }

    @GetMapping("/getByApplicationId/{applicationId}")
    public ConclusionResponseDto getByApplicationId(@PathVariable("applicationId") Long id) {
        return psychiatristConclusionService.getPsychiatristConclusionByApplicationId(id);
    }

    @GetMapping("/getByApplicantNtkyId/{ntkyId}")
    public ConclusionResponseDto getByApplicantNtkyId(@PathVariable("ntkyId") String ntkyId) {
        return psychiatristConclusionService.getPsychiatristConclusionByApplicantNtky_id(ntkyId);
    }

    @GetMapping("/getByYNumber/{yNumber}")
    public ConclusionResponseDto getByYNumber(@PathVariable("yNumber") String yNumber) {
        return psychiatristConclusionService.getPsychiatristConclusionByApplicantByyNumber(yNumber);
    }

    @GetMapping("/getByApplicantId/{applicantId}")
    public ConclusionResponseDto getByApplicantId(@PathVariable("applicantId") Long id) {
        return psychiatristConclusionService.getPsychiatristConclusionByApplicantId(id);
    }

    @GetMapping("/getByJshshir/{jshshir}")
    public List<ConclusionResponseDto> getByJshshir(
            @PathVariable("jshshir") String jshshir,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return psychiatristConclusionService
                .getPsychiatristConclusionByApplicantJShshir(jshshir, PageRequest.of(page, size));
    }

    @GetMapping("/getByYDate")
    public List<ConclusionResponseDto> getByYDate(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return psychiatristConclusionService
                .getPsychiatristConclusionByApplicantByYDate(yDate, PageRequest.of(page, size));
    }

    @GetMapping("/getByBetweenYDate")
    public List<ConclusionResponseDto> findByYDate(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate1,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate2,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return psychiatristConclusionService
                .getPsychiatristConclusionByApplicantByYDate(yDate1, yDate2, PageRequest.of(page, size));
    }

}
