package uz.egov.medicinemonolithic.controller.conclusion;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.egov.medicinemonolithic.dto.conclusion.request.UrologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.service.conclusion.urolog.UrologistConclusionService;
import uz.egov.medicinemonolithic.util.ApplicationConstants;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author: Saidov Og'abek
 * @Created: 21-February-2022  Monday 4:11 PM
 **/
@RestController
@RequestMapping("/api/urologist")
@RequiredArgsConstructor
@PreAuthorize("hasAnyRole('ROLE_UROLOGIST', 'ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_HEAD_PHYSICIAN')")
public class UrologistConclusionController {
    private final UrologistConclusionService urologistConclusionService;

    @PostMapping("/save")
    @PreAuthorize("hasRole('ROLE_UROLOGIST')")
    public ConclusionResponseDto saveUrologistConclusion(@RequestBody UrologistConclusionRequestDto conclusion) {
        return urologistConclusionService.save(conclusion);
    }

    @PutMapping("/update/{id}")
    @PreAuthorize("hasRole('ROLE_UROLOGIST')")
    public ConclusionResponseDto updateUrologistConclusion(@PathVariable Long id,
                                                           @RequestBody UrologistConclusionRequestDto conclusion) {
        return urologistConclusionService.update(id, conclusion);
    }

    @GetMapping("/getById/{id}")
    public ConclusionResponseDto getById(@PathVariable("id") Long id) {
        return urologistConclusionService.getUrologistConclusionById(id);
    }

    @GetMapping("/getAll")
    public List<ConclusionResponseDto> getAll(
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        return urologistConclusionService.getAllUrologistConclusions(PageRequest.of(page, size));
    }


    @GetMapping("/getBySpecialistId/{specialistId}")
    public List<ConclusionResponseDto> getByConclusionId(
            @PathVariable("specialistId") Long id,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        return urologistConclusionService.getUrologistConclusionsBySpecialistId(id, PageRequest.of(page, size));
    }

    @GetMapping("/getByWorkPlaceId/{workPlaceId}")
    public List<ConclusionResponseDto> getByWorkPlaceId(
            @PathVariable("workPlaceId") Long id,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return urologistConclusionService.getUrologistConclusionsByIdWorkPlace(id, PageRequest.of(page, size));
    }

    @GetMapping("/getByApplicationId/{applicationId}")
    public ConclusionResponseDto getByApplicationId(@PathVariable("applicationId") Long id) {
        return urologistConclusionService.getUrologistConclusionByApplicationId(id);
    }

    @GetMapping("/getByApplicantNtkyId/{ntkyId}")
    public ConclusionResponseDto getByApplicantNtkyId(@PathVariable("ntkyId") String ntkyId) {
        return urologistConclusionService.getUrologistConclusionByApplicantNtky_id(ntkyId);
    }

    @GetMapping("/getByYNumber/{yNumber}")
    public ConclusionResponseDto getByYNumber(@PathVariable("yNumber") String yNumber) {
        return urologistConclusionService.getUrologistConclusionByApplicantByyNumber(yNumber);
    }

    @GetMapping("/getByApplicantId/{applicantId}")
    public ConclusionResponseDto getByApplicantId(@PathVariable("applicantId") Long id) {
        return urologistConclusionService.getUrologistConclusionByApplicantId(id);
    }

    @GetMapping("/getByJshshir/{jshshir}")
    public List<ConclusionResponseDto> getByJshshir(
            @PathVariable("jshshir") String jshshir,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return urologistConclusionService.getUrologistConclusionByApplicantJShshir(jshshir, PageRequest.of(page, size));
    }

    @GetMapping("/getByYDate")
    public List<ConclusionResponseDto> getByYDate(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return urologistConclusionService.getUrologistConclusionByApplicantByYDate(yDate, PageRequest.of(page, size));
    }

    @GetMapping("/getByBetweenYDate")
    public List<ConclusionResponseDto> findByYDate(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate1,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate2,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return urologistConclusionService
                .getUrologistConclusionByApplicantByYDate(yDate1, yDate2, PageRequest.of(page, size));
    }

}
