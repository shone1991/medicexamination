package uz.egov.medicinemonolithic.controller.conclusion;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.egov.medicinemonolithic.dto.conclusion.request.ReumathologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.service.conclusion.reumath.ReumathologistConclusionService;
import uz.egov.medicinemonolithic.util.ApplicationConstants;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author: Saidov Og'abek
 * @Created: 21-February-2022  Monday 3:46 PM
 **/
@RestController
@RequestMapping("/api/reumathologist")
@RequiredArgsConstructor
@PreAuthorize("hasAnyRole('ROLE_REUMATHOLOGIST', 'ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_HEAD_PHYSICIAN')")
public class ReumathologistConclusionController {
    private final ReumathologistConclusionService reumathologistConclusionService;

    @PostMapping("/save")
    @PreAuthorize("hasRole('ROLE_REUMATHOLOGIST')")
    public ConclusionResponseDto saveReumathologistConclusion(@RequestBody ReumathologistConclusionRequestDto conclusionDto) {
        return reumathologistConclusionService.save(conclusionDto);
    }

    @PutMapping("/update/{id}")
    @PreAuthorize("hasRole('ROLE_REUMATHOLOGIST')")
    public ConclusionResponseDto updateReumathologistConclusion(
            @PathVariable Long id,
            @RequestBody ReumathologistConclusionRequestDto conclusionDto) {
        return reumathologistConclusionService.update(id, conclusionDto);
    }

    @GetMapping("/getById/{id}")
    public ConclusionResponseDto getById(@PathVariable("id") Long id) {
        return reumathologistConclusionService.getReumathologistConclusionById(id);
    }

    @GetMapping("/getAll")
    public List<ConclusionResponseDto> getAll(
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        return reumathologistConclusionService.getAllReumathologistConclusions(PageRequest.of(page, size));
    }

    @GetMapping("/getBySpecialistId/{specialistId}")
    public List<ConclusionResponseDto> getByConclusionId(
            @PathVariable("specialistId") Long id,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return reumathologistConclusionService.getReumathologistConclusionsBySpecialistId(id, PageRequest.of(page, size));
    }

    @GetMapping("/getByWorkPlaceId/{workPlaceId}")
    public List<ConclusionResponseDto> getByWorkPlaceId(
            @PathVariable("workPlaceId") Long id,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return reumathologistConclusionService.getReumathologistConclusionsByIdWorkPlace(id, PageRequest.of(page, size));
    }

    @GetMapping("/getByApplicationId/{applicationId}")
    public ConclusionResponseDto getByApplicationId(@PathVariable("applicationId") Long id) {
        return reumathologistConclusionService.getReumathologistConclusionByApplicationId(id);
    }

    @GetMapping("/getByApplicantNtkyId/{ntkyId}")
    public ConclusionResponseDto getByApplicantNtkyId(@PathVariable("ntkyId") String ntkyId) {
        return reumathologistConclusionService.getReumathologistConclusionByApplicantNtky_id(ntkyId);
    }

    @GetMapping("/getByYNumber/{yNumber}")
    public ConclusionResponseDto getByYNumber(@PathVariable("yNumber") String yNumber) {
        return reumathologistConclusionService.getReumathologistConclusionByApplicantByyNumber(yNumber);
    }

    @GetMapping("/getByApplicantId/{applicantId}")
    public ConclusionResponseDto getByApplicantId(@PathVariable("applicantId") Long id) {
        return reumathologistConclusionService.getReumathologistConclusionByApplicantId(id);
    }

    @GetMapping("/getByJshshir/{jshshir}")
    public List<ConclusionResponseDto> getByJshshir(
            @PathVariable("jshshir") String jshshir,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return reumathologistConclusionService
                .getReumathologistConclusionByApplicantJShshir(jshshir, PageRequest.of(page, size));
    }

    @GetMapping("/getByYDate")
    public List<ConclusionResponseDto> getByYDate(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return reumathologistConclusionService
                .getReumathologistConclusionByApplicantByYDate(yDate, PageRequest.of(page, size));
    }

    @GetMapping("/getByBetweenYDate")
    public List<ConclusionResponseDto> findByYDate(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate1,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate yDate2,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(defaultValue = ApplicationConstants.DEFAULT_PAGE_SIZE) Integer size) {
        return reumathologistConclusionService
                .getReumathologistConclusionByApplicantByYDate(yDate1, yDate2, PageRequest.of(page, size));
    }

}
