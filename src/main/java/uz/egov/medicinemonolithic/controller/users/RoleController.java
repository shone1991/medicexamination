package uz.egov.medicinemonolithic.controller.users;

import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import uz.egov.medicinemonolithic.entity.users.Role;
import uz.egov.medicinemonolithic.service.user.role.RoleService;

import java.util.List;

/**
 * @Author: Saidov Og'abek
 * @Created: 22-February-2022  Tuesday 2:16 PM
 **/
@RestController
@RequestMapping("/api/role")
@RequiredArgsConstructor
@PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
public class RoleController {
    private static final Logger LOGGER = LogManager.getLogger();

    private final RoleService roleService;

    @GetMapping("/getById/{id}")
    public Role getById(@PathVariable Integer id) {
        Role roleById = roleService.getRoleById(id);
        LOGGER.info("Get role by this id [{}]", id);
        return roleById;
    }

    @GetMapping("/getByName/{roleName}")
    public Role getByName(@PathVariable("roleName") String name) {

        Role roleByName = roleService.getRoleByName(name);
            LOGGER.info("Get role by this ROLE_NAME [{}]", name);
        return roleByName;
    }

    @GetMapping("/getAll")
    public List<Role> getAll() {
        List<Role> roleList = roleService.getRoles();
        LOGGER.info("Get all roles by this username [{}]",
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return roleList;
    }

    @PostMapping("/add")
    public Role save(@RequestBody Role role) {
        Role saveRole = roleService.saveRole(role);
        LOGGER.info("SAVE role by this username [{}]",
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return saveRole;
    }

    @PutMapping("/update/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Role update(@PathVariable Integer id, @RequestBody Role role) {
        Role updateRole = roleService.updateRole(id, role);
        LOGGER.info("UPDATE role by this username [{}]",
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return updateRole;
    }

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Role delete(@PathVariable Integer id) {
        LOGGER.info("DELETE role by this username [{}]",
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return roleService.deleteRole(id);
    }


}
