package uz.egov.medicinemonolithic.controller.users;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import uz.egov.medicinemonolithic.entity.users.User;
import uz.egov.medicinemonolithic.service.user.UserService;

import java.util.List;

/**
 * @Author: Saidov Og'abek
 * @Created: 22-February-2022  Tuesday 2:15 PM
 **/
@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/getById/{id}")
    public User getById(@PathVariable Long id) {
        return userService.getUserById(id);
    }

    @GetMapping("/getByUsername/{username}")
    public User getByUsername(@PathVariable String username) {
        return userService.getUserByUsername(username);
    }

    @GetMapping("/getByPhone/{phone}")
    public User getByPhone(@PathVariable String phone) {
        return userService.getUserByPhone(phone);
    }

    @GetMapping("/getByPinfl/{pinfl}")
    public User getByPinfl(@PathVariable String pinfl) {
        return userService.getUserByPinfl(pinfl);
    }

    @GetMapping("/getAll")
    public List<User> getAllUser() {
        return userService.getAllUser();
    }

    @PutMapping("/update/{id}")
    public User updateUser(@PathVariable Long id, @RequestBody User user) {
        return userService.update(id, user);
    }

}
