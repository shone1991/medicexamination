package uz.egov.medicinemonolithic.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.egov.medicinemonolithic.entity.Clinic;
import uz.egov.medicinemonolithic.service.clinic.ClinicService;

import java.util.List;

/**
 * @Author: Saidov Og'abek
 * @Created: 17-February-2022  Thursday 4:01 PM
 **/
@RestController
@RequestMapping("/api/clinic")
@RequiredArgsConstructor
public class ClinicController {
    private final ClinicService clinicService;

    @PostMapping("/save")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MODERATOR')")
    public Clinic saveClinic(@RequestBody Clinic clinic) {
        return clinicService.save(clinic);
    }

    @PutMapping("/update/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_HEAD_PHYSICIAN')")
    public Clinic updateClinic(@PathVariable Long id, @RequestBody Clinic clinic) {
        return clinicService.update(id, clinic);
    }

    @GetMapping("/getById/{id}")
    public Clinic getByClinic(@PathVariable Long id) {
        return clinicService.getById(id);
    }

    @GetMapping("/getAll")
    public List<Clinic> getAllClinics() {
        return clinicService.getAll();
    }

    @GetMapping("/getByName")
    public List<Clinic> getLikeName(@RequestParam String clinicName) {
        return clinicService.getByName(clinicName);
    }
}
