package uz.egov.medicinemonolithic.service.clinic;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.entity.Clinic;
import uz.egov.medicinemonolithic.repository.ClinicRepository;

import java.util.List;
import java.util.Optional;

/**
 * @Author: Saidov Og'abek
 * @Created: 17-February-2022  Thursday 3:53 PM
 **/
@Service
@RequiredArgsConstructor
public class ClinicServiceImpl implements ClinicService {
    private final ClinicRepository repository;
    private final ObjectMapper mapper;

    @Override
    public Clinic save(Clinic clinic) {
        clinic.setId(null);
        return repository.save(clinic);
    }

    @SneakyThrows
    @Override
    public Clinic update(Long id, Clinic clinic) {
        Optional<Clinic> optionalClinic = repository.findById(id);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        if (optionalClinic.isPresent()) {
            Clinic updateValue = mapper.updateValue(optionalClinic.get(), clinic);
            updateValue.setId(id);
            return repository.save(updateValue);
        }
        throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, String.format("Clinic not found this id -> [%d]!!", id));
    }

    @Override
    public Clinic getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("Clinic not found this id -> [%d]!!", id)));
    }

    @Override
    public List<Clinic> getAll() {
        return repository.findAll();
    }

    @Override
    public List<Clinic> getByName(String name) {
        return repository.findAllByNameLike(name);
    }
}
