package uz.egov.medicinemonolithic.service.clinic;

import uz.egov.medicinemonolithic.entity.Clinic;

import java.util.List;

/**
 * @Author: Saidov Og'abek
 * @Created: 17-February-2022  Thursday 3:50 PM
 **/
public interface ClinicService {
    Clinic save(Clinic clinic);

    Clinic update(Long id, Clinic clinic);

    Clinic getById(Long id);

    List<Clinic> getAll();

    List<Clinic> getByName(String name);
}
