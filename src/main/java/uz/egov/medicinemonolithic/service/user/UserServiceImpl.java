package uz.egov.medicinemonolithic.service.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.entity.users.ERole;
import uz.egov.medicinemonolithic.entity.users.User;
import uz.egov.medicinemonolithic.repository.user_role.RoleRepository;
import uz.egov.medicinemonolithic.repository.user_role.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: Saidov Og'abek
 * @Created: 22-February-2022  Tuesday 4:47 PM
 **/
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ObjectMapper mapper;
    private final PasswordEncoder encoder;
    private final RoleRepository roleRepository;

    @Override
    public User getUserById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new ResponseStatusException(
                HttpStatus.BAD_REQUEST, String.format("User not found this id: [%d]", id)));
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(() -> new ResponseStatusException(
                HttpStatus.BAD_REQUEST, String.format("User not found this username: [%s]", username)));
    }

    @Override
    public User getUserByPhone(String phone) {
        return userRepository.findByPhone(phone).orElseThrow(() -> new ResponseStatusException(
                HttpStatus.BAD_REQUEST, String.format("User not found this phone number: [%s]", phone)));

    }

    @Override
    public User getUserByPinfl(String pinfl) {
        return userRepository.findByPinfl(pinfl).orElseThrow(() -> new ResponseStatusException(
                HttpStatus.BAD_REQUEST, String.format("User not found this pinfl: [%s]", pinfl)));
    }

    @Override
    public List<User> getAllUser() {
        return userRepository.findAll().stream().map(user -> user.setAndReturn(null)).collect(Collectors.toList());
    }

    @SneakyThrows
    @Override
    public User update(Long id, User user) {
        User foundUser = userRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("User not found this id: [%d]", id)));

        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        user.setUserDetail(mapper.updateValue(foundUser.getUserDetail(), user.getUserDetail()));
        user.setIsActive(null);
        if (user.getRoles().size() == 0) user.setRoles(null);
        else user.setRoles(user
                .getRoles()
                .stream()
                .filter(r -> !((r.getName().name().equals(ERole.ROLE_ADMIN.name())) ||
                        r.getName().name().equals(ERole.ROLE_MODERATOR.name())))
                .map(roleString -> roleRepository.findByName(ERole.valueOf(roleString.getName().name()))
                        .orElseThrow(() -> new RuntimeException("Error: Role is not found.")))
                .collect(Collectors.toSet()));
        if (user.getPassword() != null)
            user.setPassword(encoder.encode(user.getPassword()));
        User updateValue = mapper.updateValue(foundUser, user);
        updateValue.setId(id);
        return userRepository.save(updateValue).setAndReturn(null);
    }
}
