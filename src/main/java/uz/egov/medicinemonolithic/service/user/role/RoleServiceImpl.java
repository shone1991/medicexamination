package uz.egov.medicinemonolithic.service.user.role;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.entity.users.ERole;
import uz.egov.medicinemonolithic.entity.users.Role;
import uz.egov.medicinemonolithic.repository.user_role.RoleRepository;

import java.util.List;

/**
 * @Author: Saidov Og'abek
 * @Created: 22-February-2022  Tuesday 2:40 PM
 **/
@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {
    private final RoleRepository repository;

    @Override
    public Role getRoleById(Integer id) {
        return repository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("Role not found this id: [%d]", id)));
    }

    @Override
    public Role getRoleByName(String roleName) {
        return repository.findByName(ERole.getEnum(roleName)).orElseThrow(()->
                new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("Role not found this name: [%s]", roleName)));
    }

    @Override
    public List<Role> getRoles() {
        return repository.findAll();
    }

    @Override
    public Role saveRole(Role role) {
        existsRole(role);
        role.setId(null);
        return repository.save(role);
    }

    @Override
    public Role updateRole(Integer id, Role role) {
        Role dbRole = repository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("Role not found this id: [%d]", id)));
        existsRole(role);
        dbRole.setName(role.getName());
        return repository.save(dbRole);
    }

    @Override
    public Role deleteRole(Integer id) {
        Role dbRole = repository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("Role not found this id: [%d]", id)));
        repository.deleteById(id);
        return dbRole;
    }

    private void existsRole(Role role) {
        if (repository.existsByName(role.getName()))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("[%s] role already exists !!", role.getName()));
    }
}
