package uz.egov.medicinemonolithic.service.user;

import uz.egov.medicinemonolithic.entity.users.User;

import java.util.List;

/**
 * @Author: Saidov Og'abek
 * @Created: 18-February-2022  Friday 2:42 PM
 **/
public interface UserService {

    User getUserById(Long id);

    User getUserByUsername(String username);

   User getUserByPhone(String phone);

   User getUserByPinfl(String pinfl);

   List<User> getAllUser();

   User update(Long id, User user);
}
