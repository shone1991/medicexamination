package uz.egov.medicinemonolithic.service.user.role;

import uz.egov.medicinemonolithic.entity.users.Role;

import java.util.List;

/**
 * @Author: Saidov Og'abek
 * @Created: 22-February-2022  Tuesday 2:38 PM
 **/
public interface RoleService {
    Role getRoleById(Integer id);

    List<Role> getRoles();

    Role saveRole(Role role);

    Role updateRole(Integer id, Role role);

    Role deleteRole(Integer id);

    Role getRoleByName(String roleName);
}
