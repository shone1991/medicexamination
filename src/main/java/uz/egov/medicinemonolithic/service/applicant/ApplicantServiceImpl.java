package uz.egov.medicinemonolithic.service.applicant;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.exchange.request.MinJusticeToMinHealthRequest;
import uz.egov.medicinemonolithic.entity.Applicant;
import uz.egov.medicinemonolithic.mapper.NTYKDataApplicant;
import uz.egov.medicinemonolithic.repository.ApplicantRepository;
import uz.egov.medicinemonolithic.util.CommonUtils;

import java.time.LocalDate;
import java.util.List;



/**
 * @author f.lapasov on 2/8/2022
 * @project medicine
 */
@Service
@RequiredArgsConstructor
public class ApplicantServiceImpl implements ApplicantService {
    private final ApplicantRepository applicantRepository;
    private final NTYKDataApplicant ntykDataApplicantMapper;

    @Override
    public Page<Applicant> getAllApplicants(Integer page, Integer size) {
        return applicantRepository.findAll(CommonUtils.sortedByCreatedAt(page, size));
    }

    @Override
    public Applicant getApplicantById(Long id) {
        return applicantRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NO_CONTENT,
                String.format("Applicant not found with this id -> [%d]!!", id)));
    }

    @Override
    public Applicant getApplicantByNtykId(String ntyk_id) {
        return applicantRepository.findApplicantByNtky_id(ntyk_id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NO_CONTENT,
                String.format("Applicant not found with this ntyk_id -> [%s]!!", ntyk_id)));
    }

    @Override
    public Applicant getApplicantByYNumber(String yNumber) {
        return applicantRepository.findApplicantByYnumber(yNumber).orElseThrow(() -> new ResponseStatusException(HttpStatus.NO_CONTENT,
                String.format("Applicant not found with this yNumber -> [%s]!!", yNumber)));
    }

    @Override
    public List<Applicant> getAllApplicantsByBranchAndDate(String branch, LocalDate yDate, Pageable pageable) {
        return applicantRepository.findApplicantByBranchAndYDate(branch,yDate,pageable).orElseThrow(()->new ResponseStatusException(HttpStatus.NO_CONTENT,
                "Applicants with given params not found"));
    }


    @Override
    public Applicant addApplicant(Applicant applicant) {
        return applicantRepository.save(applicant);
    }

    @Override
    public Applicant saveFromDto(MinJusticeToMinHealthRequest minJusticeToMinHealthRequest) {
        Applicant applicant = ntykDataApplicantMapper.ntykDataToApplicant(
                minJusticeToMinHealthRequest.getNtykInformation().getNtykData());
        return applicantRepository.save(applicant);

    }

    @Override
    public Applicant updateApplicant(Long id, Applicant applicant) {
        Applicant foundApplicant = applicantRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NO_CONTENT,
                String.format("Applicant not found with this id -> [%d]!!", id)));
        foundApplicant.setBranch(applicant.getBranch());
        foundApplicant.setFHDY_Bosh(applicant.getFHDY_Bosh());
        foundApplicant.setInVoiceD(applicant.getInVoiceD());
        foundApplicant.setJshshir(applicant.getJshshir());
        foundApplicant.setInVoiceN(applicant.getInVoiceN());
        foundApplicant.setNSh_FIO(applicant.getNSh_FIO());
        foundApplicant.setNtky_id(applicant.getNtky_id());
        foundApplicant.setYDate(applicant.getYDate());
        foundApplicant.setNSh_Address(applicant.getNSh_Address());
        foundApplicant.setNSh_BDate(applicant.getNSh_BDate());
        foundApplicant.setYNumber(applicant.getYNumber());
        return applicantRepository.save(foundApplicant);
    }
}
