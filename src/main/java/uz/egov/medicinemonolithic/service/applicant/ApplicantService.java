package uz.egov.medicinemonolithic.service.applicant;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import uz.egov.medicinemonolithic.dto.exchange.request.MinJusticeToMinHealthRequest;
import uz.egov.medicinemonolithic.entity.Applicant;

import java.time.LocalDate;
import java.util.List;


/**
 * @Author: Saidov Og'abek
 * @Created: 14-February-2022  Monday 5:32 PM
 **/
public interface ApplicantService {
    Applicant addApplicant(Applicant applicant);

    Applicant saveFromDto(MinJusticeToMinHealthRequest minJusticeToMinHealthRequest);

    Applicant updateApplicant(Long id, Applicant applicant);

    Page<Applicant> getAllApplicants(Integer page, Integer size);

    Applicant getApplicantById(Long id);

    Applicant getApplicantByNtykId(String ntyk_id);

    Applicant getApplicantByYNumber(String yNumber);

    List<Applicant> getAllApplicantsByBranchAndDate(String branch, LocalDate yDate, Pageable pageable);

}
