package uz.egov.medicinemonolithic.service.application;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.application.request.ApplicationExchangeDto;
import uz.egov.medicinemonolithic.dto.application.request.ApplicationRequestDto;
import uz.egov.medicinemonolithic.entity.Applicant;
import uz.egov.medicinemonolithic.entity.Application;
import uz.egov.medicinemonolithic.entity.ApplicationStatus;
import uz.egov.medicinemonolithic.repository.ApplicantRepository;
import uz.egov.medicinemonolithic.repository.ApplicationRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author f.lapasov on 2/8/2022
 * @project medicine
 */
@Service
@RequiredArgsConstructor
public class ApplicationServiceImpl implements ApplicationService {
    private final ApplicationRepository applicationRepository;
    private final ApplicantRepository applicantRepository;

    @Override
    public Application saveApplication(Application application) {
        return applicationRepository.save(application);
    }

    @Override
    public Application saveApplicationFromExchange(ApplicationExchangeDto applicationExchangeDto) {
        Application application=new Application();
        application.setApplicant(application.getApplicant());
        application.setInformation_date(applicationExchangeDto.getInformation_date());
        return applicationRepository.save(application);
    }

    @Override
    public Application saveApplicationFromDto(ApplicationRequestDto applicationRequestDto) {
        Applicant applicant = applicantRepository
                .findById(applicationRequestDto.getApplicantId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NO_CONTENT,
                        String.format("Applicant not found this id: [%d]", applicationRequestDto.getApplicantId())));
        Application application=new Application(applicant, LocalDateTime.now(),ApplicationStatus.ACCEPTED);
        return applicationRepository.save(application);
    }

    @Override
    public List<Application> findAllApplication(Pageable pageable) {
        return applicationRepository.findAllApplication(pageable);
    }

    @Override
    public List<Application> findAllApplicationByStatus(ApplicationStatus status, Pageable pageable) {
        return applicationRepository.findAllApplicationByStatus(status,pageable);
    }

    @Override
    public List<Application> findAllApplicationByStatusAndDate(ApplicationStatus status, LocalDate dateInfo,
                                                               Pageable pageable) {
        return applicationRepository.findAllApplicationByStatusAndDate(status,dateInfo,pageable);
    }

    @Override
    public List<Application> findAllApplicationByStatusAndDateBetween(ApplicationStatus status, LocalDate dateInfo1,
                                                                      LocalDate dateInfo2, Pageable pageable) {
//        return applicationRepository.findAllApplicationByStatusAndDateBetween(status,dateInfo1,dateInfo2,pageable);
        return null;
    }

    @Override
    public List<Application> findAllApplicationByApplicantBranchAndDate(String branch, LocalDate dateInfo,
                                                                        Pageable pageable) {
        return applicationRepository.findAllApplicationByApplicantBranchAndDate(branch,dateInfo,pageable);
    }

    @Override
    public List<Application> findAllApplicationByApplicantBranchAndDateBetween(String branch, LocalDate dateInfo1,
                                                                               LocalDate dateInfo2, Pageable pageable) {
        return applicationRepository.findAllApplicationByApplicantBranchAndDate(branch, dateInfo1, dateInfo2, pageable);
    }

    @Override
    public List<Application> findApplicationByApplicantJshshir(String jshshir) {
        return applicationRepository.findApplicationByApplicantJshshir(jshshir);
    }

    @Override
    public Application findApplicationByApplicationId(Long id) {
        return applicationRepository.findApplicationByApplicationId(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NO_CONTENT,
                String.format("Application not found this id: [%d]", id)));
    }

    @Override
    public Application findApplicationByApplicantId(Long id) {
        return applicationRepository.findApplicationByApplicantId(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NO_CONTENT,
                String.format("Application not found for this applicant id: [%d]", id)));
    }

    @Override
    public Application findApplicationByApplicantNtkyId(String ntykId) {
        return applicationRepository.findApplicationByApplicantNtkyId(ntykId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NO_CONTENT,
                String.format("Application not found for this applicant ntykId: [%s]", ntykId)));
    }

    @Override
    public Application findApplicationByApplicantYNumber(String yNumber, LocalDate yDate) {
        return applicationRepository.findApplicationByApplicantYnumber(yNumber,yDate).orElseThrow(() -> new ResponseStatusException(HttpStatus.NO_CONTENT,
                String.format("Application not found for this applicant yNumber: [%s] and yDate: [%s]", yNumber,yDate)));
    }

    @Override
    public Application updateApplication(Long idApplication, Application application) {
        Application found = findApplicationByApplicationId(idApplication);
        found.setApplicant(application.getApplicant());
        found.setApplicationStatus(application.getApplicationStatus());
        return applicationRepository.save(found);
    }

    @Override
    public Application updateApplication(Long idApplication, ApplicationStatus status) {
        Application found = findApplicationByApplicationId(idApplication);
        found.setApplicationStatus(status);
        return applicationRepository.save(found);
    }

}
