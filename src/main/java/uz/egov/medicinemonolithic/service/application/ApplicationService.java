package uz.egov.medicinemonolithic.service.application;

import org.springframework.data.domain.Pageable;
import uz.egov.medicinemonolithic.dto.application.request.ApplicationExchangeDto;
import uz.egov.medicinemonolithic.dto.application.request.ApplicationRequestDto;
import uz.egov.medicinemonolithic.entity.Application;
import uz.egov.medicinemonolithic.entity.ApplicationStatus;

import java.time.LocalDate;
import java.util.List;

/**
 * @author f.lapasov on 2/23/2022
 * @project medicine-monolithic
 */
public interface ApplicationService {
    Application saveApplicationFromExchange(ApplicationExchangeDto applicationExchangeDto);

    Application saveApplicationFromDto(ApplicationRequestDto applicationRequestDto);

    Application saveApplication(Application application);

    List<Application> findAllApplication(Pageable pageable);

    List<Application> findAllApplicationByStatus(ApplicationStatus status, Pageable pageable);

    List<Application> findAllApplicationByStatusAndDate(ApplicationStatus status, LocalDate dateInfo, Pageable pageable);

    List<Application> findAllApplicationByStatusAndDateBetween(ApplicationStatus status, LocalDate dateInfo1,
                                                               LocalDate dateInfo2, Pageable pageable);

    List<Application> findAllApplicationByApplicantBranchAndDate(String branch, LocalDate dateInfo, Pageable pageable);

    List<Application> findAllApplicationByApplicantBranchAndDateBetween(String branch, LocalDate dateInfo1,
                                                                        LocalDate dateInfo2, Pageable pageable);

    List<Application> findApplicationByApplicantJshshir(String jshshir);

    Application findApplicationByApplicationId(Long id);

    Application findApplicationByApplicantId(Long id);

    Application findApplicationByApplicantNtkyId(String ntykId);

    Application findApplicationByApplicantYNumber(String yNumber, LocalDate yDate);

    Application updateApplication(Long idApplication, Application application);

    Application updateApplication(Long idApplication, ApplicationStatus status);


}
