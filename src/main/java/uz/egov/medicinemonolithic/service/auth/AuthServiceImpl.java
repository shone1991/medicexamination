package uz.egov.medicinemonolithic.service.auth;

import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.auth.request.LoginRequest;
import uz.egov.medicinemonolithic.dto.auth.request.SignupRequest;
import uz.egov.medicinemonolithic.dto.auth.response.JwtResponse;
import uz.egov.medicinemonolithic.dto.auth.response.MessageResponse;
import uz.egov.medicinemonolithic.dto.specialist.request.SpecialistRequestDto;
import uz.egov.medicinemonolithic.entity.Clinic;
import uz.egov.medicinemonolithic.entity.users.ERole;
import uz.egov.medicinemonolithic.entity.users.Role;
import uz.egov.medicinemonolithic.entity.users.User;
import uz.egov.medicinemonolithic.entity.users.UserDetail;
import uz.egov.medicinemonolithic.repository.user_role.RoleRepository;
import uz.egov.medicinemonolithic.repository.user_role.UserRepository;
import uz.egov.medicinemonolithic.security.jwt.JwtUtils;
import uz.egov.medicinemonolithic.security.services.UserDetailsImpl;
import uz.egov.medicinemonolithic.service.clinic.ClinicService;
import uz.egov.medicinemonolithic.service.specialist.SpecialistService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @Author: Saidov Og'abek
 * @Created: 21-February-2022  Monday 5:29 PM
 **/
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String MOBILE_NUMBERS_PATTERN = "^(\\d{1,3}( )?)?((\\(\\d{1,3}\\))|\\d{1,3})[- .]?\\d{3,4}[- .]?\\d{4}$";

    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final ClinicService clinicService;
    private final SpecialistService specialistService;
    private final PasswordEncoder encoder;
    private final JwtUtils jwtUtils;

    @Override
    public JwtResponse login(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
/*
token,
userId,
username,
name
username
clinic
[role]
* */

//        return new JwtResponse(jwt,
//                userDetails.getId(),
//                userDetails.getUsername(),
//                userDetails.getEmail(),
//                roles);

        LOGGER.info(String.format("Login user via this username : [%s] ", loginRequest.getUsername()));
        return new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getName(),
                userDetails.getSurname(),
                userDetails.getClinic(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles);
    }

    @Override
    @Transactional
    public ResponseEntity<MessageResponse> signUp(SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            LOGGER.error(String.format("Error: [%s] username is already taken!", signUpRequest.getUsername()));
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByPhone(signUpRequest.getPhone())) {
            LOGGER.error(String.format("Error: [%s] phone is already in use!", signUpRequest.getPhone()));
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Phone is already in use!"));
        }

        if (!Pattern.matches(MOBILE_NUMBERS_PATTERN, signUpRequest.getPhone())) {
            LOGGER.error(String.format("Error: [%s] phone number has a wrong format !!", signUpRequest.getPhone()));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Phone number has a wrong format !!");
        }

        // Create new user detail
        UserDetail userDetail = new UserDetail(signUpRequest.getName(), signUpRequest.getSurname(),
                signUpRequest.getBirthDate(), signUpRequest.getPinfl(), signUpRequest.getPassportSerialNumber());
        // Create new auth account
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getPhone(),
                encoder.encode(signUpRequest.getPassword()), userDetail);

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null || strRoles.size() == 0) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.removeIf(roleName -> roleName.equals(ERole.ROLE_ADMIN.name()));

            strRoles.forEach(role -> {

                Role adminRole = roleRepository.findByName(ERole.valueOf(role))
                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                roles.add(adminRole);

            });
        }

        user.setRoles(roles);
        userRepository.save(user);

        Clinic clinic = clinicService.getById(signUpRequest.getClinicId());

        specialistService.saveSpecialist(new SpecialistRequestDto(user.getId(), clinic.getId()));

        LOGGER.info(String.format("Success: [%s] user registered successfully!", signUpRequest.getUsername()));
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
}
