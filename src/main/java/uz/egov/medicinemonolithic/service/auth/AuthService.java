package uz.egov.medicinemonolithic.service.auth;

import org.springframework.http.ResponseEntity;
import uz.egov.medicinemonolithic.dto.auth.request.LoginRequest;
import uz.egov.medicinemonolithic.dto.auth.request.SignupRequest;
import uz.egov.medicinemonolithic.dto.auth.response.JwtResponse;
import uz.egov.medicinemonolithic.dto.auth.response.MessageResponse;

/**
 * @Author: Saidov Og'abek
 * @Created: 21-February-2022  Monday 5:27 PM
 **/
public interface AuthService {
    JwtResponse login(LoginRequest loginRequest);

    ResponseEntity<MessageResponse> signUp(SignupRequest signupRequest);
}
