package uz.egov.medicinemonolithic.service.specialist;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.specialist.request.SpecialistRequestDto;
import uz.egov.medicinemonolithic.dto.specialist.response.SpecialistResponseDto;
import uz.egov.medicinemonolithic.entity.Specialist;
import uz.egov.medicinemonolithic.repository.SpecialistRepository;
import uz.egov.medicinemonolithic.util.specialist.SpecialistUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Author: Saidov Og'abek
 * @Created: 17-February-2022  Thursday 6:28 PM
 **/
@Service
@RequiredArgsConstructor
public class SpecialistServiceImpl implements SpecialistService {
    private final SpecialistRepository repository;
    private final ObjectMapper mapper;

    @Override
    public SpecialistResponseDto saveSpecialist(SpecialistRequestDto requestDto) {
        return SpecialistUtils.convertToResponseDto(repository.save(SpecialistUtils.convertToEntity(requestDto)));
    }

    @SneakyThrows
    @Override
    public SpecialistResponseDto updateSpecialist(Long id, SpecialistRequestDto requestDto) {
        Optional<Specialist> optionalSpecialist = repository.findById(id);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        if (optionalSpecialist.isPresent()) {
            SpecialistRequestDto dto = SpecialistUtils.convertToRequestDto(optionalSpecialist.get());
            SpecialistRequestDto updateValue = mapper.updateValue(dto, requestDto);
            Specialist specialist = SpecialistUtils.convertToEntity(updateValue);
            specialist.setId(id);
            specialist.setCreatedAt(optionalSpecialist.get().getCreatedAt());
            return SpecialistUtils.convertToResponseDto(repository.save(specialist));
        } else {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, String.format("Specialist not found this id -> [%d]!!", id));
        }
    }

    @Override
    public SpecialistResponseDto getByPinfl(String pinfl) {
        return SpecialistUtils.convertToResponseDto(
                repository.findSpecialistByPinfl(pinfl).orElseThrow(
                        ()->new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Specialist not found this pinfl -> [%s]!!", pinfl)))
        );
    }

    @Override
    public SpecialistResponseDto getByUsername(String username) {
        return SpecialistUtils.convertToResponseDto(
                repository.findSpecialistByUsername(username)
                        .orElseThrow(
                                ()->new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                        String.format("Specialist not found this username -> [%s]!!", username))
        ));
    }

    @Override
    public SpecialistResponseDto findById(Long id) {
        return SpecialistUtils.convertToResponseDto(repository.findSpecialistById(id));
    }

    @Override
    public List<SpecialistResponseDto> getAll(Pageable pageable) {
        return repository
                .findAll()
                .stream()
                .map(SpecialistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public SpecialistResponseDto findSpecialistByUserId(Long id) {
        return SpecialistUtils.convertToResponseDto(repository
                .findSpecialistByUserId(id)
                .orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.NO_CONTENT,
                                String.format("Specialist not found with this id -> [%d]!!", id))));
    }
}
