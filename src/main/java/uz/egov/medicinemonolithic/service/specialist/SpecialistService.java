package uz.egov.medicinemonolithic.service.specialist;

import org.springframework.data.domain.Pageable;
import uz.egov.medicinemonolithic.dto.specialist.request.SpecialistRequestDto;
import uz.egov.medicinemonolithic.dto.specialist.response.SpecialistResponseDto;

import java.util.List;

/**
 * @Author: Saidov Og'abek
 * @Created: 17-February-2022  Thursday 6:21 PM
 **/
public interface SpecialistService {
    SpecialistResponseDto saveSpecialist(SpecialistRequestDto requestDto);

    SpecialistResponseDto updateSpecialist(Long id, SpecialistRequestDto requestDto);

    SpecialistResponseDto getByPinfl(String pinfl);

    SpecialistResponseDto getByUsername(String username);

    SpecialistResponseDto findById(Long id);

    List<SpecialistResponseDto> getAll(Pageable pageable);

    SpecialistResponseDto findSpecialistByUserId(Long id);
}
