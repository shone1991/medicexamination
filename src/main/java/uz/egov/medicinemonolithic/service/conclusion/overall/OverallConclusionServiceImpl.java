package uz.egov.medicinemonolithic.service.conclusion.overall;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.conclusion.request.OverallConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.entity.conclusion.OverallConclusion;
import uz.egov.medicinemonolithic.repository.conclusion.OverallConclusionRepository;
import uz.egov.medicinemonolithic.util.conclusion.OverallUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OverallConclusionServiceImpl implements OverallConclusionService {
    private final OverallConclusionRepository repository;
    private final ObjectMapper mapper;

    @Override
    public ConclusionResponseDto save(OverallConclusionRequestDto conclusionDto) {
        OverallConclusion conclusion = OverallUtils.convertToEntity(conclusionDto);
        return OverallUtils.convertToResponseDto(repository.save(conclusion));
    }

    @SneakyThrows
    @Override
    public ConclusionResponseDto update(Long id, OverallConclusionRequestDto conclusionDto) {
        Optional<OverallConclusion> conclusionOptional = repository.findById(id);
        if (conclusionOptional.isPresent()) {
            OverallConclusionRequestDto requestDto = OverallUtils.convertToRequestDto(conclusionOptional.get());
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            conclusionDto.setApplicationId(null);
            conclusionDto.setSpecialistId(null);
            OverallConclusionRequestDto updateValue = mapper.updateValue(requestDto, conclusionDto);
            OverallConclusion conclusion = OverallUtils.convertToEntity(updateValue);
            conclusion.setCreatedAt(conclusionOptional.get().getCreatedAt());
            conclusion.setId(id);
            return OverallUtils.convertToResponseDto(repository.save(conclusion));
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("OverallConclusion not found this id -> [%d]!!", id));
    }

    @Override
    public ConclusionResponseDto getOverallConclusionById(Long id) {
        return OverallUtils.convertToResponseDto(repository.findById(id).orElseThrow(()
                -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("OverallConclusion not found this id -> [%d]!!", id))));
    }

    @Override
    public List<ConclusionResponseDto> getAllOverallConclusions(Pageable pageable) {
        return repository.findAll(pageable)
                .stream()
                .map(OverallUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getOverallConclusionsBySpecialistId(Long specialistId, Pageable pageable) {
        return repository
                .findOverallConclusionsBySpecialistId(specialistId, pageable)
                .stream()
                .map(OverallUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getOverallConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable) {
        return repository
                .findOverallConclusionsByIdWorkPlace(idWorkPlace, pageable)
                .stream()
                .map(OverallUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public ConclusionResponseDto getOverallConclusionByApplicationId(Long applicationId) {
        return OverallUtils
                .convertToResponseDto(repository.findOverallConclusionByApplicationId(applicationId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("OverallConclusion not found this applicationId -> [%d]!!", applicationId))));
    }

    @Override
    public ConclusionResponseDto getOverallConclusionByApplicantNtky_id(String ntky_id) {
        return OverallUtils
                .convertToResponseDto(repository.findOverallConclusionByApplicantNtky_id(ntky_id).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("OverallConclusion not found this ntky_id -> [%s]!!", ntky_id))));
    }

    @Override
    public ConclusionResponseDto getOverallConclusionByApplicantByyNumber(String yNumber) {
        return OverallUtils
                .convertToResponseDto(repository.findOverallConclusionByApplicantByyNumber(yNumber).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("OverallConclusion not found this yNumber -> [%s]!!", yNumber))));
    }

    @Override
    public ConclusionResponseDto getOverallConclusionByApplicantId(Long applicantId) {
        return OverallUtils
                .convertToResponseDto(repository.findOverallConclusionByApplicantId(applicantId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("OverallConclusion not found this applicantId -> [%d]!!", applicantId))));
    }

    @Override
    public List<ConclusionResponseDto> getOverallConclusionByApplicantJShshir(String jshshir, Pageable pageable) {
        return repository
                .findOverallConclusionByApplicantJShshir(jshshir, pageable)
                .stream()
                .map(OverallUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getOverallConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable) {
        return repository
                .findOverallConclusionByApplicantByYDate(yDate, pageable)
                .stream()
                .map(OverallUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getOverallConclusionByApplicantByYDate(LocalDate yDate1,
                                                                                   LocalDate yDate2,
                                                                                   Pageable pageable) {
        return repository
                .findOverallConclusionByApplicantByYDate(yDate1, yDate2, pageable)
                .stream()
                .map(OverallUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }
}
