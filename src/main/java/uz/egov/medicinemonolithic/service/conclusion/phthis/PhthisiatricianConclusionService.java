package uz.egov.medicinemonolithic.service.conclusion.phthis;

import org.springframework.data.domain.Pageable;
import uz.egov.medicinemonolithic.dto.conclusion.request.PhthisiatricianConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author: Saidov Og'abek
 **/
public interface PhthisiatricianConclusionService {
    ConclusionResponseDto save(PhthisiatricianConclusionRequestDto conclusionDto);

    ConclusionResponseDto update(Long id, PhthisiatricianConclusionRequestDto conclusion);

    ConclusionResponseDto getPhthisiatricianConclusionById(Long id);

    List<ConclusionResponseDto> getAllPhthisiatricianConclusions(Pageable pageable);

    List<ConclusionResponseDto> getPhthisiatricianConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    List<ConclusionResponseDto> getPhthisiatricianConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    ConclusionResponseDto getPhthisiatricianConclusionByApplicationId(Long applicationId);

    ConclusionResponseDto getPhthisiatricianConclusionByApplicantNtky_id(String ntky_id);

    ConclusionResponseDto getPhthisiatricianConclusionByApplicantByyNumber(String yNumber);

    ConclusionResponseDto getPhthisiatricianConclusionByApplicantId(Long applicantId);

    List<ConclusionResponseDto> getPhthisiatricianConclusionByApplicantJShshir(String jshshir, Pageable pageable);

    List<ConclusionResponseDto> getPhthisiatricianConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    List<ConclusionResponseDto> getPhthisiatricianConclusionByApplicantByYDate(LocalDate yDate1,
                                                                               LocalDate yDate2,
                                                                               Pageable pageable);
}
