package uz.egov.medicinemonolithic.service.conclusion.cardio;

import org.springframework.data.domain.Pageable;
import uz.egov.medicinemonolithic.dto.conclusion.request.CardiologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author: Saidov Og'abek
 **/
public interface CardiologistConclusionService {

    ConclusionResponseDto saveCardiologistConclusion(CardiologistConclusionRequestDto requestDto);

    ConclusionResponseDto update(Long id, CardiologistConclusionRequestDto requestDto);

    ConclusionResponseDto getConclusionConclusionById(Long id);

    List<ConclusionResponseDto> getAllConclusionConclusions(Pageable pageable);

    List<ConclusionResponseDto> getCardioConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    List<ConclusionResponseDto> getCardioConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    ConclusionResponseDto getCardioConclusionByApplicationId(Long applicationId);

    ConclusionResponseDto getCardioConclusionByApplicantNtkyId(String ntkyId);

    ConclusionResponseDto getCardioConclusionByApplicantByyNumber(String yNumber);

    ConclusionResponseDto getCardioConclusionByApplicantId(Long applicantId);

    List<ConclusionResponseDto> getCardioConclusionsByApplicantJshshir(String jshshir, Pageable pageable);

    List<ConclusionResponseDto> getCardioConclusionsByApplicantByYDate(LocalDate yDate, Pageable pageable);

    List<ConclusionResponseDto> findCardiologistConclusionsByApplicantByYDate(LocalDate yDate1,
                                                                              LocalDate yDate2,
                                                                              Pageable pageable);
}
