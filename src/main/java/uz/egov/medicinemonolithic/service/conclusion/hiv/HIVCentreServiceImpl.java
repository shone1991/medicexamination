package uz.egov.medicinemonolithic.service.conclusion.hiv;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.conclusion.request.HIVCentreConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.entity.conclusion.HIVCentreConclusion;
import uz.egov.medicinemonolithic.repository.conclusion.HivCentreConclusionRepository;
import uz.egov.medicinemonolithic.util.conclusion.HIVCentreUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 04-March-2022  Friday 5:42 PM
 **/
@Service
@RequiredArgsConstructor
public class HIVCentreServiceImpl implements HIVCentreService {
    private final ObjectMapper mapper;
    private final HivCentreConclusionRepository repository;


    @Override
    public ConclusionResponseDto save(HIVCentreConclusionRequestDto conclusionDto) {
        HIVCentreConclusion conclusion = HIVCentreUtils.convertToEntity(conclusionDto);
        return HIVCentreUtils.convertToResponseDto(repository.save(conclusion));
    }

    @SneakyThrows
    @Override
    public ConclusionResponseDto update(Long id, HIVCentreConclusionRequestDto conclusionDto) {
        Optional<HIVCentreConclusion> conclusionOptional = repository.findById(id);
        if (conclusionOptional.isPresent()) {
            HIVCentreConclusionRequestDto requestDto = HIVCentreUtils.convertToRequestDto(conclusionOptional.get());
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            conclusionDto.setApplicationId(null);
            conclusionDto.setSpecialistId(null);
            HIVCentreConclusionRequestDto updateValue = mapper.updateValue(requestDto, conclusionDto);
            HIVCentreConclusion conclusion = HIVCentreUtils.convertToEntity(updateValue);
            conclusion.setCreatedAt(conclusionOptional.get().getCreatedAt());
            conclusion.setId(id);
            return HIVCentreUtils.convertToResponseDto(repository.save(conclusion));
        }
        throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, String.format("HIVCentreConclusion not found this id -> [%d]!!", id));
    }

    @Override
    public ConclusionResponseDto getUrologistConclusionById(Long id) {
        return HIVCentreUtils.convertToResponseDto(repository.findById(id).orElseThrow(()
                -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("HIVCentreConclusion not found this id -> [%d]!!", id))));
    }

    @Override
    public List<ConclusionResponseDto> getAllUrologistConclusions(Pageable pageable) {
        return repository.findAll(pageable)
                .stream()
                .map(HIVCentreUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getEndocrinoConclusionsBySpecialistId(Long specialistId, Pageable pageable) {
        return repository
                .findHIVCentreConclusionsBySpecialistId(specialistId, pageable)
                .stream()
                .map(HIVCentreUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getEndocrinoConclusionsByIdWorkPlace(Long idWorkPlace,
                                                                            Pageable pageable) {
        return repository
                .findHIVCentreConclusionsByIdWorkPlace(idWorkPlace, pageable)
                .stream()
                .map(HIVCentreUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public ConclusionResponseDto getEndocrinoConclusionByApplicationId(Long applicationId) {
        return HIVCentreUtils.convertToResponseDto(repository.findHIVCentreConclusionByApplicationId(applicationId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("HIVCentreConclusion not found this applicationId -> [%d]!!", applicationId))));

    }

    @Override
    public ConclusionResponseDto getEndocrinoConclusionByApplicantNtky_id(String ntkyId) {
        return HIVCentreUtils
                .convertToResponseDto(repository.findHIVCentreConclusionByApplicantNtky_id(ntkyId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("HIVCentreConclusion not found this ntky_id -> [%s]!!", ntkyId))));
    }

    @Override
    public ConclusionResponseDto getEndocrinoConclusionByApplicantByyNumber(String yNumber) {
        return HIVCentreUtils
                .convertToResponseDto(repository.findHIVCentreConclusionByApplicantByyNumber(yNumber).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("HIVCentreConclusion not found this yNumber -> [%s]!!", yNumber))));
    }

    @Override
    public ConclusionResponseDto getEndocrinoConclusionByApplicantId(Long applicantId) {
        return HIVCentreUtils
                .convertToResponseDto(repository.findHIVCentreConclusionByApplicantId(applicantId).orElseThrow(
                        ()-> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("HIVCentreConclusion not found this applicantId -> [%d]!!", applicantId))));
    }

    @Override
    public List<ConclusionResponseDto> getEndocrinoConclusionByApplicantJShshir(String jshshir,
                                                                                Pageable pageable) {
        return repository
                .findHIVCentreConclusionByApplicantJshshir(jshshir, pageable)
                .stream()
                .map(HIVCentreUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getEndocrinoConclusionByApplicantByYDate(LocalDate yDate,
                                                                                Pageable pageable) {
        return repository
                .findHIVCentreConclusionByApplicantByYDate(yDate, pageable)
                .stream()
                .map(HIVCentreUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getEndocrinoConclusionByApplicantByYDate(LocalDate yDate1, LocalDate yDate2,
                                                                                Pageable pageable) {
        return repository
                .findHIVCentreConclusionByApplicantByYDate(yDate1, yDate2, pageable)
                .stream()
                .map(HIVCentreUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }
}
