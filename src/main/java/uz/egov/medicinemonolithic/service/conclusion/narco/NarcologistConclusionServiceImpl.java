package uz.egov.medicinemonolithic.service.conclusion.narco;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.conclusion.request.NarcologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.entity.conclusion.NarcologistConclusion;
import uz.egov.medicinemonolithic.repository.conclusion.NarcologistConclusionRepository;
import uz.egov.medicinemonolithic.util.conclusion.NarcologistUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class NarcologistConclusionServiceImpl implements NarcologistConclusionService {
    private final NarcologistConclusionRepository repository;
    private final ObjectMapper mapper;

    @Override
    public ConclusionResponseDto save(NarcologistConclusionRequestDto conclusionDto) {
        NarcologistConclusion conclusion = NarcologistUtils.convertToEntity(conclusionDto);
        return NarcologistUtils.convertToResponseDto(repository.save(conclusion));
    }

    @SneakyThrows
    @Override
    public ConclusionResponseDto update(Long id, NarcologistConclusionRequestDto conclusionDto) {
        Optional<NarcologistConclusion> conclusionOptional = repository.findById(id);
        if (conclusionOptional.isPresent()) {
            NarcologistConclusionRequestDto requestDto = NarcologistUtils.convertToRequestDto(conclusionOptional.get());
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            conclusionDto.setApplicationId(null);
            conclusionDto.setSpecialistId(null);
            NarcologistConclusionRequestDto updateValue = mapper.updateValue(requestDto, conclusionDto);
            NarcologistConclusion conclusion = NarcologistUtils.convertToEntity(updateValue);
            conclusion.setCreatedAt(conclusionOptional.get().getCreatedAt());
            conclusion.setId(id);
            return NarcologistUtils.convertToResponseDto(repository.save(conclusion));
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("NarcologistConclusion not found this id -> [%d]!!", id));
    }

    @Override
    public ConclusionResponseDto getUrologistConclusionById(Long id) {
        return NarcologistUtils.convertToResponseDto(repository.findById(id).orElseThrow(()
                -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("NarcologistConclusion not found this id -> [%d]!!", id))));
    }

    @Override
    public List<ConclusionResponseDto> getAllUrologistConclusions(Pageable pageable) {
        return repository.findAll(pageable)
                .stream()
                .map(NarcologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getNarcologistConclusionsBySpecialistId(Long specialistId, Pageable pageable) {
        return repository
                .findNarcologistConclusionsBySpecialistId(specialistId, pageable)
                .stream()
                .map(NarcologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getNarcologistConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable) {
        return repository
                .findNarcologistConclusionsByIdWorkPlace(idWorkPlace, pageable)
                .stream()
                .map(NarcologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public ConclusionResponseDto getNarcologistConclusionByApplicationId(Long applicationId) {
        return NarcologistUtils.convertToResponseDto
                (repository.findNarcologistConclusionByApplicationId(applicationId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("NarcologistConclusion not found this applicationId -> [%d]!!",
                                        applicationId))));
    }

    @Override
    public ConclusionResponseDto getNarcologistConclusionByApplicantNtky_id(String ntkyId) {
        return NarcologistUtils
                .convertToResponseDto(repository.findNarcologistConclusionByApplicantNtky_id(ntkyId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("NarcologistConclusion not found this ntky_id -> [%s]!!", ntkyId))));

    }

    @Override
    public ConclusionResponseDto getNarcologistConclusionByApplicantByyNumber(String yNumber) {
        return NarcologistUtils
                .convertToResponseDto(repository.findNarcologistConclusionByApplicantByyNumber(yNumber).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("NarcologistConclusion not found this yNumber -> [%s]!!", yNumber))));

    }

    @Override
    public ConclusionResponseDto getNarcologistConclusionByApplicantId(Long applicantId) {
        return NarcologistUtils
                .convertToResponseDto(repository.findNarcologistConclusionByApplicantId(applicantId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("NarcologistConclusion not found this applicantId -> [%d]!!",
                                        applicantId))));

    }

    @Override
    public List<ConclusionResponseDto> getNarcologistConclusionByApplicantJshshir(String jshshir, Pageable pageable) {
        return repository
                .findNarcologistConclusionByApplicantJshshir(jshshir, pageable)
                .stream()
                .map(NarcologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getNarcologistConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable) {
        return repository
                .findNarcologistConclusionByApplicantByYDate(yDate, pageable)
                .stream()
                .map(NarcologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getNarcologistConclusionByApplicantByYDate(LocalDate yDate1, LocalDate yDate2,
                                                                                  Pageable pageable) {
        return repository
                .findNarcologistConclusionByApplicantByYDate(yDate1, yDate2, pageable)
                .stream()
                .map(NarcologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }
}
