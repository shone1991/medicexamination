package uz.egov.medicinemonolithic.service.conclusion.psychiatrist;

import org.springframework.data.domain.Pageable;
import uz.egov.medicinemonolithic.dto.conclusion.request.PsychiatristConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 07-March-2022  Monday 3:12 PM
 **/
public interface PsychiatristConclusionService {
    ConclusionResponseDto save(PsychiatristConclusionRequestDto conclusion);

    ConclusionResponseDto update(Long id, PsychiatristConclusionRequestDto conclusion);

    List<ConclusionResponseDto> getAllPsychiatristConclusions(Pageable pageable);

    ConclusionResponseDto getPsychiatristConclusionById(Long id);

    List<ConclusionResponseDto> getPsychiatristConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    List<ConclusionResponseDto> getPsychiatristConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    ConclusionResponseDto getPsychiatristConclusionByApplicationId(Long applicationId);

    ConclusionResponseDto getPsychiatristConclusionByApplicantNtky_id(String ntky_id);

    ConclusionResponseDto getPsychiatristConclusionByApplicantByyNumber(String yNumber);

    ConclusionResponseDto getPsychiatristConclusionByApplicantId(Long applicantId);

    List<ConclusionResponseDto> getPsychiatristConclusionByApplicantJShshir(String jshshir, Pageable pageable);

    List<ConclusionResponseDto> getPsychiatristConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    List<ConclusionResponseDto> getPsychiatristConclusionByApplicantByYDate(LocalDate yDate1,
                                                                              LocalDate yDate2,
                                                                              Pageable pageable);

}
