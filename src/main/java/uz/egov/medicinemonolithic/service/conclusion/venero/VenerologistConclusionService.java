package uz.egov.medicinemonolithic.service.conclusion.venero;

import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;
import uz.egov.medicinemonolithic.dto.conclusion.request.VenerologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author: Saidov Og'abek
 * @Created: 08-February-2022  Tuesday 4:40 PM
 **/
public interface VenerologistConclusionService {
    //    ConclusionResponseDto save(VenerologistConclusionRequestDto conclusion);
    ConclusionResponseDto save(VenerologistConclusionRequestDto conclusion, final MultipartFile[] files);


    ConclusionResponseDto update(Long id, VenerologistConclusionRequestDto conclusion);

    ConclusionResponseDto getVenerologistConclusionById(Long id);

    List<ConclusionResponseDto> getAllVenerologistConclusions(Pageable pageable);

    List<ConclusionResponseDto> getVenerologistConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    List<ConclusionResponseDto> getVenerologistConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    ConclusionResponseDto getVenerologistConclusionByApplicationId(Long applicationId);

    ConclusionResponseDto getVenerologistConclusionByApplicantNtky_id(String ntky_id);

    ConclusionResponseDto getVenerologistConclusionByApplicantByyNumber(String yNumber);

    ConclusionResponseDto getVenerologistConclusionByApplicantId(Long applicantId);

    List<ConclusionResponseDto> getVenerologistConclusionByApplicantJShshir(String jshshir, Pageable pageable);

    List<ConclusionResponseDto> getVenerologistConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    List<ConclusionResponseDto> getVenerologistConclusionByApplicantByYDate(LocalDate yDate1,
                                                                            LocalDate yDate2,
                                                                            Pageable pageable);
}
