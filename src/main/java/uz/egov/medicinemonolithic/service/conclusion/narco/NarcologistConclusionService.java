package uz.egov.medicinemonolithic.service.conclusion.narco;

import org.springframework.data.domain.Pageable;
import uz.egov.medicinemonolithic.dto.conclusion.request.NarcologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author: Saidov Og'abek
 **/
public interface NarcologistConclusionService {
    ConclusionResponseDto save(NarcologistConclusionRequestDto conclusion);

    ConclusionResponseDto update(Long id, NarcologistConclusionRequestDto conclusion);

    ConclusionResponseDto getUrologistConclusionById(Long id);

    List<ConclusionResponseDto> getAllUrologistConclusions(Pageable pageable);

    List<ConclusionResponseDto> getNarcologistConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    List<ConclusionResponseDto> getNarcologistConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    ConclusionResponseDto getNarcologistConclusionByApplicationId(Long applicationId);

    ConclusionResponseDto getNarcologistConclusionByApplicantNtky_id(String ntkyId);

    ConclusionResponseDto getNarcologistConclusionByApplicantByyNumber(String yNumber);

    ConclusionResponseDto getNarcologistConclusionByApplicantId(Long applicantId);

    List<ConclusionResponseDto> getNarcologistConclusionByApplicantJshshir(String jshshir, Pageable pageable);

    List<ConclusionResponseDto> getNarcologistConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    List<ConclusionResponseDto> getNarcologistConclusionByApplicantByYDate(LocalDate yDate1, LocalDate yDate2, Pageable pageable);
}
