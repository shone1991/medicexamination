package uz.egov.medicinemonolithic.service.conclusion.urolog;


import org.springframework.data.domain.Pageable;
import uz.egov.medicinemonolithic.dto.conclusion.request.UrologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author: Saidov Og'abek
 **/
public interface UrologistConclusionService {
    ConclusionResponseDto save(UrologistConclusionRequestDto conclusion);

    ConclusionResponseDto update(Long id, UrologistConclusionRequestDto conclusion);

    ConclusionResponseDto getUrologistConclusionById(Long id);

    List<ConclusionResponseDto> getAllUrologistConclusions(Pageable pageable);

    List<ConclusionResponseDto> getUrologistConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    List<ConclusionResponseDto> getUrologistConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    ConclusionResponseDto getUrologistConclusionByApplicationId(Long applicationId);

    ConclusionResponseDto getUrologistConclusionByApplicantNtky_id(String ntky_id);

    ConclusionResponseDto getUrologistConclusionByApplicantByyNumber(String yNumber);

    ConclusionResponseDto getUrologistConclusionByApplicantId(Long applicantId);

    List<ConclusionResponseDto> getUrologistConclusionByApplicantJShshir(String jshshir, Pageable pageable);

    List<ConclusionResponseDto> getUrologistConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    List<ConclusionResponseDto> getUrologistConclusionByApplicantByYDate(LocalDate yDate1,
                                                                         LocalDate yDate2,
                                                                         Pageable pageable);
}
