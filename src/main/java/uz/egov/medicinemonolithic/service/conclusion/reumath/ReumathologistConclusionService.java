package uz.egov.medicinemonolithic.service.conclusion.reumath;

import org.springframework.data.domain.Pageable;
import uz.egov.medicinemonolithic.dto.conclusion.request.ReumathologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author: Saidov Og'abek
 **/
public interface ReumathologistConclusionService {
    ConclusionResponseDto save(ReumathologistConclusionRequestDto conclusion);

    ConclusionResponseDto update(Long id, ReumathologistConclusionRequestDto conclusion);

    List<ConclusionResponseDto> getAllReumathologistConclusions(Pageable pageable);

    ConclusionResponseDto getReumathologistConclusionById(Long id);

    List<ConclusionResponseDto> getReumathologistConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    List<ConclusionResponseDto> getReumathologistConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    ConclusionResponseDto getReumathologistConclusionByApplicationId(Long applicationId);

    ConclusionResponseDto getReumathologistConclusionByApplicantNtky_id(String ntky_id);

    ConclusionResponseDto getReumathologistConclusionByApplicantByyNumber(String yNumber);

    ConclusionResponseDto getReumathologistConclusionByApplicantId(Long applicantId);

    List<ConclusionResponseDto> getReumathologistConclusionByApplicantJShshir(String jshshir, Pageable pageable);

    List<ConclusionResponseDto> getReumathologistConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    List<ConclusionResponseDto> getReumathologistConclusionByApplicantByYDate(LocalDate yDate1,
                                                                              LocalDate yDate2,
                                                                              Pageable pageable);
}
