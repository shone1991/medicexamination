package uz.egov.medicinemonolithic.service.conclusion.psychiatrist;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.conclusion.request.PsychiatristConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.entity.conclusion.PsychiatristConclusion;
import uz.egov.medicinemonolithic.repository.conclusion.PsychiatristConclusionRepository;
import uz.egov.medicinemonolithic.util.conclusion.PsychiatristUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 07-March-2022  Monday 3:18 PM
 **/
@Service
@RequiredArgsConstructor
public class PsychiatristConclusionServiceImpl implements PsychiatristConclusionService {
    private final PsychiatristConclusionRepository repository;
    private final ObjectMapper mapper;

    @Override
    public ConclusionResponseDto save(PsychiatristConclusionRequestDto conclusionDto) {
        PsychiatristConclusion conclusion = PsychiatristUtils.convertToEntity(conclusionDto);
        return PsychiatristUtils.convertToResponseDto(repository.save(conclusion));
    }

    @SneakyThrows
    @Override
    public ConclusionResponseDto update(Long id, PsychiatristConclusionRequestDto conclusionDto) {
        Optional<PsychiatristConclusion> conclusionOptional = repository.findById(id);
        if (conclusionOptional.isPresent()) {
            PsychiatristConclusionRequestDto requestDto = PsychiatristUtils.convertToRequestDto(conclusionOptional.get());
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            conclusionDto.setApplicationId(null);
            conclusionDto.setSpecialistId(null);
            PsychiatristConclusionRequestDto updateValue = mapper.updateValue(requestDto, conclusionDto);
            PsychiatristConclusion conclusion = PsychiatristUtils.convertToEntity(updateValue);
            conclusion.setCreatedAt(conclusionOptional.get().getCreatedAt());
            conclusion.setId(id);
            return PsychiatristUtils.convertToResponseDto(repository.save(conclusion));
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("PsychiatristConclusion not found this id -> [%d]!!", id));
    }

    @Override
    public ConclusionResponseDto getPsychiatristConclusionById(Long id) {
        return PsychiatristUtils.convertToResponseDto(repository.findById(id).orElseThrow(()
                -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("PsychiatristConclusion not found this id -> [%d]!!", id))));
    }

    @Override
    public List<ConclusionResponseDto> getAllPsychiatristConclusions(Pageable pageable) {
        return repository.findAll(pageable)
                .stream()
                .map(PsychiatristUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getPsychiatristConclusionsBySpecialistId(Long specialistId, Pageable pageable) {
        return repository
                .findPsychiatristConclusionsBySpecialistId(specialistId, pageable)
                .stream()
                .map(PsychiatristUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getPsychiatristConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable) {
        return repository
                .findPsychiatristConclusionsByIdWorkPlace(idWorkPlace, pageable)
                .stream()
                .map(PsychiatristUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public ConclusionResponseDto getPsychiatristConclusionByApplicationId(Long applicationId) {
        return PsychiatristUtils
                .convertToResponseDto(repository.findPsychiatristConclusionByApplicationId(applicationId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("PsychiatristConclusion not found this applicationId -> [%d]!!", applicationId))));
    }

    @Override
    public ConclusionResponseDto getPsychiatristConclusionByApplicantNtky_id(String ntky_id) {
        return PsychiatristUtils
                .convertToResponseDto(repository.findPsychiatristConclusionByApplicantNtky_id(ntky_id).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("PsychiatristConclusion not found this ntky_id -> [%s]!!", ntky_id))));
    }

    @Override
    public ConclusionResponseDto getPsychiatristConclusionByApplicantByyNumber(String yNumber) {
        return PsychiatristUtils
                .convertToResponseDto(repository.findPsychiatristConclusionByApplicantByyNumber(yNumber).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("PsychiatristConclusion not found this yNumber -> [%s]!!", yNumber))));
    }

    @Override
    public ConclusionResponseDto getPsychiatristConclusionByApplicantId(Long applicantId) {
        return PsychiatristUtils
                .convertToResponseDto(repository.findPsychiatristConclusionByApplicantId(applicantId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("PsychiatristConclusion not found this applicantId -> [%d]!!", applicantId))));
    }

    @Override
    public List<ConclusionResponseDto> getPsychiatristConclusionByApplicantJShshir(String jshshir, Pageable pageable) {
        return repository
                .findPsychiatristConclusionByApplicantJShshir(jshshir, pageable)
                .stream()
                .map(PsychiatristUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getPsychiatristConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable) {
        return repository
                .findPsychiatristConclusionByApplicantByYDate(yDate, pageable)
                .stream()
                .map(PsychiatristUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getPsychiatristConclusionByApplicantByYDate(LocalDate yDate1,
                                                                                     LocalDate yDate2,
                                                                                     Pageable pageable) {
        return repository
                .findPsychiatristConclusionByApplicantByYDate(yDate1, yDate2, pageable)
                .stream()
                .map(PsychiatristUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

}
