package uz.egov.medicinemonolithic.service.conclusion.venero;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.conclusion.request.VenerologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.entity.conclusion.VenerologistConclusion;
import uz.egov.medicinemonolithic.repository.conclusion.VenerologistConclusionRepository;
import uz.egov.medicinemonolithic.util.conclusion.VenerologistUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VenerologistConclusionServiceImpl implements VenerologistConclusionService {
    private final VenerologistConclusionRepository repository;
    private final ObjectMapper mapper;

//    @Override
//    public ConclusionResponseDto save(VenerologistConclusionRequestDto conclusionDto) {
//        VenerologistConclusion venerologistConclusion = VenerologistUtils.convertToEntity(conclusionDto);
//        return VenerologistUtils.convertToResponseDto(repository.save(venerologistConclusion));
//    }

    @Override
    public ConclusionResponseDto save(VenerologistConclusionRequestDto conclusionDto, MultipartFile[] files) {
        if (files != null && files.length > 0) {
//            Arrays.stream(files).forEach(multipartFile -> multipartFile.getOriginalFilename()+ UUID.randomUUID())
        }
        VenerologistConclusion venerologistConclusion = VenerologistUtils.convertToEntity(conclusionDto);

        return VenerologistUtils.convertToResponseDto(repository.save(venerologistConclusion));
    }

    @SneakyThrows
    @Override
    public ConclusionResponseDto update(Long id, VenerologistConclusionRequestDto conclusionDto) {
        Optional<VenerologistConclusion> conclusionOptional = repository.findById(id);

        if (conclusionOptional.isPresent()) {
            VenerologistConclusionRequestDto requestDto = VenerologistUtils.convertToRequestDto(conclusionOptional.get());
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            conclusionDto.setApplicationId(null);
            conclusionDto.setSpecialistId(null);
            VenerologistConclusionRequestDto updateValue = mapper.updateValue(requestDto, conclusionDto);
            VenerologistConclusion conclusion = VenerologistUtils.convertToEntity(updateValue);
            conclusion.setCreatedAt(conclusionOptional.get().getCreatedAt());
            conclusion.setId(id);
            return VenerologistUtils.convertToResponseDto(repository.save(conclusion));
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("VenerologistConclusion not found this id -> [%d]!!", id));
    }

    @Override
    public ConclusionResponseDto getVenerologistConclusionById(Long id) {
        return VenerologistUtils.convertToResponseDto(repository.findById(id).orElseThrow(()
                -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("VenerologistConclusion not found this id -> [%d]!!", id))));
    }

    @Override
    public List<ConclusionResponseDto> getAllVenerologistConclusions(Pageable pageable) {
        return repository.findAll(pageable)
                .stream()
                .map(VenerologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getVenerologistConclusionsBySpecialistId(Long specialistId, Pageable pageable) {
        return repository
                .findVenerologistConclusionsBySpecialistId(specialistId, pageable)
                .stream()
                .map(VenerologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getVenerologistConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable) {
        return repository
                .findVenerologistConclusionsByIdWorkPlace(idWorkPlace, pageable)
                .stream()
                .map(VenerologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public ConclusionResponseDto getVenerologistConclusionByApplicationId(Long applicationId) {
        return VenerologistUtils
                .convertToResponseDto(repository.findVenerologistConclusionByApplicationId(applicationId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("VenerologistConclusion not found this applicationId -> [%d]!!", applicationId))));
    }

    @Override
    public ConclusionResponseDto getVenerologistConclusionByApplicantNtky_id(String ntky_id) {
        return VenerologistUtils
                .convertToResponseDto(repository.findVenerologistConclusionByApplicantNtky_id(ntky_id).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("VenerologistConclusion not found this ntky_id -> [%s]!!", ntky_id))
                ));
    }

    @Override
    public ConclusionResponseDto getVenerologistConclusionByApplicantByyNumber(String yNumber) {
        return VenerologistUtils.convertToResponseDto(repository.findVenerologistConclusionByApplicantByyNumber(yNumber)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("VenerologistConclusion not found this yNumber -> [%s]!!", yNumber))));
    }

    @Override
    public ConclusionResponseDto getVenerologistConclusionByApplicantId(Long applicantId) {
        return VenerologistUtils.convertToResponseDto(repository
                .findVenerologistConclusionByApplicantId(applicantId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("VenerologistConclusion not found this applicantId -> [%s]!!", applicantId))));
    }

    @Override
    public List<ConclusionResponseDto> getVenerologistConclusionByApplicantJShshir(String jshshir, Pageable pageable) {
        return repository
                .findVenerologistConclusionByApplicantJShshir(jshshir, pageable)
                .stream()
                .map(VenerologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getVenerologistConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable) {
        return repository
                .findVenerologistConclusionByApplicantByYDate(yDate, pageable)
                .stream()
                .map(VenerologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getVenerologistConclusionByApplicantByYDate(LocalDate yDate1,
                                                                                   LocalDate yDate2,
                                                                                   Pageable pageable) {
        return repository
                .findVenerologistConclusionByApplicantByYDate(yDate1, yDate2, pageable)
                .stream()
                .map(VenerologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }
}
