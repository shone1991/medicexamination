package uz.egov.medicinemonolithic.service.conclusion.cardio;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.conclusion.request.CardiologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.entity.conclusion.CardiologistConclusion;
import uz.egov.medicinemonolithic.repository.conclusion.CardiologistConclusionRepository;
import uz.egov.medicinemonolithic.util.conclusion.CardiologistUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CardiologistConclusionServiceImpl implements CardiologistConclusionService {
    private static final Logger LOGGER = LogManager.getLogger();

    private final CardiologistConclusionRepository repository;
    private final ObjectMapper objectMapper;

    @Override
    public ConclusionResponseDto saveCardiologistConclusion(CardiologistConclusionRequestDto requestDto) {
        CardiologistConclusion conclusion = CardiologistUtils.convertToEntity(requestDto);
        ConclusionResponseDto responseDto = CardiologistUtils.convertToResponseDto(repository.save(conclusion));
        LOGGER.info("Save cardiologist [{}}] id and by [{}] username !!", responseDto.getId(),
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return responseDto;
    }

    @Override
    public ConclusionResponseDto update(Long id, CardiologistConclusionRequestDto conclusionDto) {
        Optional<CardiologistConclusion> cardiologistConclusionOptional = repository.findById(id);
        if (cardiologistConclusionOptional.isPresent()) {
            try {
                CardiologistConclusionRequestDto requestDto = CardiologistUtils
                        .convertToRequestDto(cardiologistConclusionOptional.get());

                objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                conclusionDto.setApplicationId(null);
                conclusionDto.setSpecialistId(null);
                CardiologistConclusionRequestDto updateValue = objectMapper
                        .updateValue(requestDto, conclusionDto);

                CardiologistConclusion conclusion = CardiologistUtils.convertToEntity(updateValue);
                conclusion.setCreatedAt(cardiologistConclusionOptional.get().getCreatedAt());
                conclusion.setId(id);

                ConclusionResponseDto responseDto = CardiologistUtils.convertToResponseDto(repository.save(conclusion));
                LOGGER.info("Update cardiologist [{}] id and by [{}] username !!", responseDto.getId(),
                        ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

                return responseDto;
            } catch (JsonMappingException e) {
                LOGGER.error("ObjectMapper can't parse on update CardiologistConclusion !!");
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ObjectMapper can't parse !!");
            }
        }
        LOGGER.error(String.format("CardiologistConclusion not found this id -> [%d]!!", id));
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("CardiologistConclusion not found this id -> [%d]!!", id));
    }

    @Override
    public ConclusionResponseDto getConclusionConclusionById(Long id) {
        ConclusionResponseDto responseDto = CardiologistUtils
                .convertToResponseDto(repository.findById(id)
                        .orElseThrow(
                                () -> {
                                    LOGGER.error("CardiologistConclusion not found this id -> [{}] !!", id);
                                    return new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                            String.format("CardiologistConclusion not found this id -> [%d]!!", id));
                                }));
        LOGGER.info("GetConclusionConclusionById [{}] id and by [{}] username !!", responseDto.getId(),
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return responseDto;
    }

    @Override
    public List<ConclusionResponseDto> getAllConclusionConclusions(Pageable pageable) {
        List<ConclusionResponseDto> dtoList = repository.findAll(pageable)
                .stream()
                .map(CardiologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
        LOGGER.info("GetAllConclusionConclusions by [{}] username !!",
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return dtoList;
    }

    @Override
    public List<ConclusionResponseDto> getCardioConclusionsBySpecialistId(Long specialistId, Pageable pageable) {
        List<ConclusionResponseDto> dtoList = repository
                .findCardiologistConclusionsBySpecialistId(specialistId)
                .stream()
                .map(CardiologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
        LOGGER.info("GetCardioConclusionsBySpecialistId [{}] specialistId and by [{}] username !!",
                specialistId,
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return dtoList;
    }

    @Override
    public List<ConclusionResponseDto> getCardioConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable) {
        List<ConclusionResponseDto> dtoList = repository
                .findCardiologistConclusionsByIdWorkPlace(idWorkPlace, pageable)
                .stream()
                .map(CardiologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
        LOGGER.info("GetCardioConclusionsByIdWorkPlace [{}] idWorkPlace and by [{}] username !!",
                idWorkPlace,
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return dtoList;
    }

    @Override
    public ConclusionResponseDto getCardioConclusionByApplicationId(Long applicationId) {
        ConclusionResponseDto responseDto = CardiologistUtils
                .convertToResponseDto(repository.findCardiologistConclusionByApplicationId(applicationId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("CardiologistConclusion not found this applicationId -> [%d]!!", applicationId))));
        LOGGER.info("GetCardioConclusionByApplicationId [{}] applicationId and by [{}] username !!",
                applicationId,
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return responseDto;


    }

    @Override
    public ConclusionResponseDto getCardioConclusionByApplicantNtkyId(String ntkyId) {
        ConclusionResponseDto responseDto = CardiologistUtils
                .convertToResponseDto(repository.findCardiologistConclusionByApplicantNtky_id(ntkyId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("CardiologistConclusion not found this ntky_id -> [%s]!!", ntkyId))));
        LOGGER.info("GetCardioConclusionByApplicantNtkyId [{}] ntkyId and by [{}] username !!",
                ntkyId,
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return responseDto;

    }

    @Override
    public ConclusionResponseDto getCardioConclusionByApplicantByyNumber(String yNumber) {
        ConclusionResponseDto responseDto = CardiologistUtils
                .convertToResponseDto(repository.findCardiologistConclusionByApplicantByyNumber(yNumber).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("CardiologistConclusion not found this yNumber -> [%s]!!", yNumber))));
        LOGGER.info("GetCardioConclusionByApplicantByyNumber [{}] yNumber and by [{}] username !!",
                yNumber,
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return responseDto;

    }

    @Override
    public ConclusionResponseDto getCardioConclusionByApplicantId(Long applicantId) {
        ConclusionResponseDto responseDto = CardiologistUtils
                .convertToResponseDto(repository.findCardiologistConclusionByApplicantId(applicantId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("CardiologistConclusion not found this applicantId -> [%d]!!", applicantId))));
        LOGGER.info("GetCardioConclusionByApplicantId [{}] applicantId and by [{}] username !!",
                applicantId,
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return responseDto;
    }

    @Override
    public List<ConclusionResponseDto> getCardioConclusionsByApplicantJshshir(String jshshir, Pageable pageable) {
        List<ConclusionResponseDto> dtoList = repository
                .findCardiologistConclusionsByApplicantJShshir(jshshir, pageable)
                .stream()
                .map(CardiologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
        LOGGER.info("GetCardioConclusionsByApplicantJshshir [{}] jshshir and by [{}] username !!",
                jshshir,
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
        return dtoList;
    }

    @Override
    public List<ConclusionResponseDto> getCardioConclusionsByApplicantByYDate(LocalDate yDate, Pageable pageable) {
        List<ConclusionResponseDto> dtoList = repository
                .findCardiologistConclusionsByApplicantByYDate(yDate, pageable)
                .stream()
                .map(CardiologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
        LOGGER.info("GetCardioConclusionsByApplicantByYDate [{}] yDate and by [{}] username !!",
                yDate,
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        return dtoList;
    }

    @Override
    public List<ConclusionResponseDto> findCardiologistConclusionsByApplicantByYDate(LocalDate yDate1,
                                                                                     LocalDate yDate2,
                                                                                     Pageable pageable) {
        List<ConclusionResponseDto> dtoList = repository
                .findCardiologistConclusionsByApplicantByYDate(yDate1, yDate2, pageable)
                .stream()
                .map(CardiologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
        LOGGER.info("FindCardiologistConclusionsByApplicantByYDate [{}] yDate1, [{}] yDate2 " +
                        "and by [{}] username !!",
                yDate1,
                yDate2,
                ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

        return dtoList;
    }
}
