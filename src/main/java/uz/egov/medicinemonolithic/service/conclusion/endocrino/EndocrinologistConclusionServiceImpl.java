package uz.egov.medicinemonolithic.service.conclusion.endocrino;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.conclusion.request.EndocrinologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.entity.conclusion.EndocrinologistConclusion;
import uz.egov.medicinemonolithic.repository.conclusion.EndocrinologistConclusionRepository;
import uz.egov.medicinemonolithic.util.conclusion.EndocrinologistUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EndocrinologistConclusionServiceImpl implements EndocrinologistConclusionService {
    private final EndocrinologistConclusionRepository repository;
    private final ObjectMapper mapper;

    @Override
    public ConclusionResponseDto save(EndocrinologistConclusionRequestDto conclusionDto) {
        EndocrinologistConclusion conclusion = EndocrinologistUtils.convertToEntity(conclusionDto);
        return EndocrinologistUtils.convertToResponseDto(repository.save(conclusion));
    }

    @SneakyThrows
    @Override
    public ConclusionResponseDto update(Long id, EndocrinologistConclusionRequestDto conclusionDto) {
        Optional<EndocrinologistConclusion> conclusionOptional = repository.findById(id);
        if (conclusionOptional.isPresent()) {
            EndocrinologistConclusionRequestDto requestDto = EndocrinologistUtils.convertToRequestDto(conclusionOptional.get());
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            conclusionDto.setApplicationId(null);
            conclusionDto.setSpecialistId(null);
            EndocrinologistConclusionRequestDto updateValue = mapper.updateValue(requestDto, conclusionDto);
            EndocrinologistConclusion conclusion = EndocrinologistUtils.convertToEntity(updateValue);
            conclusion.setCreatedAt(conclusionOptional.get().getCreatedAt());
            conclusion.setId(id);
            return EndocrinologistUtils.convertToResponseDto(repository.save(conclusion));
        }
        throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, String.format("EndocrinologistConclusion not found this id -> [%d]!!", id));
    }

    @Override
    public ConclusionResponseDto getUrologistConclusionById(Long id) {
        return EndocrinologistUtils.convertToResponseDto(repository.findById(id).orElseThrow(()
                -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("EndocrinologistConclusion not found this id -> [%d]!!", id))));
    }

    @Override
    public List<ConclusionResponseDto> getAllUrologistConclusions(Pageable pageable) {
        return repository.findAll(pageable)
                .stream()
                .map(EndocrinologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getEndocrinoConclusionsBySpecialistId(Long specialistId, Pageable pageable) {
        return repository
                .findEndocrinologistConclusionsBySpecialistId(specialistId, pageable)
                .stream()
                .map(EndocrinologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getEndocrinoConclusionsByIdWorkPlace(Long idWorkPlace,
                                                                            Pageable pageable) {
        return repository
                .findEndocrinologistConclusionsByIdWorkPlace(idWorkPlace, pageable)
                .stream()
                .map(EndocrinologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public ConclusionResponseDto getEndocrinoConclusionByApplicationId(Long applicationId) {
        return EndocrinologistUtils.convertToResponseDto(repository.findEndocrinologistConclusionByApplicationId(applicationId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("EndocrinologistConclusion not found this applicationId -> [%d]!!", applicationId))));

    }

    @Override
    public ConclusionResponseDto getEndocrinoConclusionByApplicantNtky_id(String ntkyId) {
        return EndocrinologistUtils
                .convertToResponseDto(repository.findEndocrinologistConclusionByApplicantNtky_id(ntkyId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("EndocrinologistConclusion not found this ntky_id -> [%s]!!", ntkyId))));
    }

    @Override
    public ConclusionResponseDto getEndocrinoConclusionByApplicantByyNumber(String yNumber) {
        return EndocrinologistUtils
                .convertToResponseDto(repository.findEndocrinologistConclusionByApplicantByyNumber(yNumber).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("EndocrinologistConclusion not found this yNumber -> [%s]!!", yNumber))));
    }

    @Override
    public ConclusionResponseDto getEndocrinoConclusionByApplicantId(Long applicantId) {
        return EndocrinologistUtils
                .convertToResponseDto(repository.findEndocrinologistConclusionByApplicantId(applicantId).orElseThrow(
                        ()-> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("EndocrinologistConclusion not found this applicantId -> [%d]!!", applicantId))));
    }

    @Override
    public List<ConclusionResponseDto> getEndocrinoConclusionByApplicantJShshir(String jshshir,
                                                                                Pageable pageable) {
        return repository
                .findEndocrinologistConclusionByApplicantJShshir(jshshir, pageable)
                .stream()
                .map(EndocrinologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getEndocrinoConclusionByApplicantByYDate(LocalDate yDate,
                                                                                Pageable pageable) {
        return repository
                .findEndocrinologistConclusionByApplicantByYDate(yDate, pageable)
                .stream()
                .map(EndocrinologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getEndocrinoConclusionByApplicantByYDate(LocalDate yDate1, LocalDate yDate2,
                                                                                Pageable pageable) {
        return repository
                .findEndocrinologistConclusionByApplicantByYDate(yDate1, yDate2, pageable)
                .stream()
                .map(EndocrinologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }
}
