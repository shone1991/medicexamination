package uz.egov.medicinemonolithic.service.conclusion.phthis;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.conclusion.request.PhthisiatricianConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.entity.conclusion.PhthisiatricianConclusion;
import uz.egov.medicinemonolithic.repository.conclusion.PhthisiatricianConclusionRepository;
import uz.egov.medicinemonolithic.util.conclusion.PhthisiatricianUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PhthisiatricianConclusionServiceImpl implements PhthisiatricianConclusionService {
    private final PhthisiatricianConclusionRepository repository;
    private final ObjectMapper mapper;

    @Override
    public ConclusionResponseDto save(PhthisiatricianConclusionRequestDto conclusionDto) {
        PhthisiatricianConclusion conclusion = PhthisiatricianUtils.convertToEntity(conclusionDto);
        return PhthisiatricianUtils.convertToResponseDto(repository.save(conclusion));
    }

    @SneakyThrows
    @Override
    public ConclusionResponseDto update(Long id, PhthisiatricianConclusionRequestDto conclusionDto) {
        Optional<PhthisiatricianConclusion> conclusionOptional = repository.findById(id);
        if (conclusionOptional.isPresent()) {
            PhthisiatricianConclusionRequestDto requestDto = PhthisiatricianUtils
                    .convertToRequestDto(conclusionOptional.get());
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            conclusionDto.setApplicationId(null);
            conclusionDto.setSpecialistId(null);
            PhthisiatricianConclusionRequestDto updateValue = mapper.updateValue(requestDto, conclusionDto);
            PhthisiatricianConclusion conclusion = PhthisiatricianUtils.convertToEntity(updateValue);
            conclusion.setCreatedAt(conclusionOptional.get().getCreatedAt());
            conclusion.setId(id);
            return PhthisiatricianUtils.convertToResponseDto(repository.save(conclusion));
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("PhthisiatricianConclusion not found this id -> [%d]!!", id));
    }

    @Override
    public ConclusionResponseDto getPhthisiatricianConclusionById(Long id) {
        return PhthisiatricianUtils.convertToResponseDto(repository.findById(id).orElseThrow(()
                -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("PhthisiatricianConclusion not found this id -> [%d]!!", id))));
    }

    @Override
    public List<ConclusionResponseDto> getAllPhthisiatricianConclusions(Pageable pageable) {
        return repository.findAll(pageable)
                .stream()
                .map(PhthisiatricianUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getPhthisiatricianConclusionsBySpecialistId(Long specialistId, Pageable pageable) {
        return repository
                .findPhthisiatricianConclusionsBySpecialistId(specialistId, pageable)
                .stream()
                .map(PhthisiatricianUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getPhthisiatricianConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable) {
        return repository
                .findPhthisiatricianConclusionsByIdWorkPlace(idWorkPlace, pageable)
                .stream()
                .map(PhthisiatricianUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public ConclusionResponseDto getPhthisiatricianConclusionByApplicationId(Long applicationId) {
        return PhthisiatricianUtils
                .convertToResponseDto(repository.findPhthisiatricianConclusionByApplicationId(applicationId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("PhthisiatricianConclusion not found this applicationId -> [%d]!!", applicationId))));

    }

    @Override
    public ConclusionResponseDto getPhthisiatricianConclusionByApplicantNtky_id(String ntky_id) {
        return PhthisiatricianUtils
                .convertToResponseDto(repository.findPhthisiatricianConclusionByApplicantNtky_id(ntky_id).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("PhthisiatricianConclusion not found this ntky_id -> [%s]!!", ntky_id))));
    }

    @Override
    public ConclusionResponseDto getPhthisiatricianConclusionByApplicantByyNumber(String yNumber) {
        return PhthisiatricianUtils
                .convertToResponseDto(repository.findPhthisiatricianConclusionByApplicantByyNumber(yNumber).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("PhthisiatricianConclusion not found this yNumber -> [%s]!!", yNumber))));
    }

    @Override
    public ConclusionResponseDto getPhthisiatricianConclusionByApplicantId(Long applicantId) {
        return PhthisiatricianUtils
                .convertToResponseDto(repository.findPhthisiatricianConclusionByApplicantId(applicantId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("PhthisiatricianConclusion not found this applicantId -> [%d]!!", applicantId))));

    }

    @Override
    public List<ConclusionResponseDto> getPhthisiatricianConclusionByApplicantJShshir(String jshshir, Pageable pageable) {
        return repository
                .findPhthisiatricianConclusionByApplicantJShshir(jshshir, pageable)
                .stream()
                .map(PhthisiatricianUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getPhthisiatricianConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable) {
        return repository
                .findPhthisiatricianConclusionByApplicantByYDate(yDate, pageable)
                .stream()
                .map(PhthisiatricianUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getPhthisiatricianConclusionByApplicantByYDate(LocalDate yDate1,
                                                                                          LocalDate yDate2,
                                                                                          Pageable pageable) {
        return repository
                .findPhthisiatricianConclusionByApplicantByYDate(yDate1, yDate2, pageable)
                .stream()
                .map(PhthisiatricianUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }
}
