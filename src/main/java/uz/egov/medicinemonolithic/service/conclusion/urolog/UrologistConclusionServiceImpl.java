package uz.egov.medicinemonolithic.service.conclusion.urolog;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.conclusion.request.UrologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.entity.conclusion.UrologistConclusion;
import uz.egov.medicinemonolithic.repository.conclusion.UrologistConclusionRepository;
import uz.egov.medicinemonolithic.util.conclusion.UrologistUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UrologistConclusionServiceImpl implements UrologistConclusionService {
    private final UrologistConclusionRepository repository;
    private final ObjectMapper mapper;

    @Override
    public ConclusionResponseDto save(UrologistConclusionRequestDto conclusionDto) {
        UrologistConclusion urologistConclusion = UrologistUtils.convertToEntity(conclusionDto);
        return UrologistUtils.convertToResponseDto(repository.save(urologistConclusion));
    }

    @SneakyThrows
    @Override
    public ConclusionResponseDto update(Long id, UrologistConclusionRequestDto conclusionDto) {
        Optional<UrologistConclusion> conclusionOptional = repository.findById(id);
        if (conclusionOptional.isPresent()) {
            UrologistConclusionRequestDto requestDto = UrologistUtils.convertToRequestDto(conclusionOptional.get());
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            conclusionDto.setApplicationId(null);
            conclusionDto.setSpecialistId(null);
            UrologistConclusionRequestDto updateValue = mapper.updateValue(requestDto, conclusionDto);
            UrologistConclusion conclusion = UrologistUtils.convertToEntity(updateValue);
            conclusion.setCreatedAt(conclusionOptional.get().getCreatedAt());
            conclusion.setId(id);
            return UrologistUtils.convertToResponseDto(repository.save(conclusion));
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("UrologistConclusion not found this id -> [%d]!!", id));
    }

    @Override
    public ConclusionResponseDto getUrologistConclusionById(Long id) {
        return UrologistUtils.convertToResponseDto(repository.findById(id).orElseThrow(()
                -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("UrologistConclusion not found this id -> [%d]!!", id))));
    }

    @Override
    public List<ConclusionResponseDto> getAllUrologistConclusions(Pageable pageable) {
        return repository.findAll(pageable)
                .stream()
                .map(UrologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getUrologistConclusionsBySpecialistId(Long specialistId, Pageable pageable) {
        return repository
                .findUrologistConclusionsBySpecialistId(specialistId, pageable)
                .stream()
                .map(UrologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getUrologistConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable) {
        return repository
                .findUrologistConclusionsByIdWorkPlace(idWorkPlace, pageable)
                .stream()
                .map(UrologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public ConclusionResponseDto getUrologistConclusionByApplicationId(Long applicationId) {
        return UrologistUtils
                .convertToResponseDto(repository.findUrologistConclusionByApplicationId(applicationId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("UrologistConclusion not found this applicationId -> [%d]!!", applicationId))));
    }

    @Override
    public ConclusionResponseDto getUrologistConclusionByApplicantNtky_id(String ntky_id) {
        return UrologistUtils
                .convertToResponseDto(repository.findUrologistConclusionByApplicantNtky_id(ntky_id).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("UrologistConclusion not found this ntky_id -> [%s]!!", ntky_id))));
    }

    @Override
    public ConclusionResponseDto getUrologistConclusionByApplicantByyNumber(String yNumber) {
        return UrologistUtils
                .convertToResponseDto(repository.findUrologistConclusionByApplicantByyNumber(yNumber).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("UrologistConclusion not found this yNumber -> [%s]!!", yNumber))));

    }

    @Override
    public ConclusionResponseDto getUrologistConclusionByApplicantId(Long applicantId) {
        return UrologistUtils
                .convertToResponseDto(repository.findUrologistConclusionByApplicantId(applicantId).orElseThrow(
                        ()-> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("UrologistConclusion not found this applicantId -> [%d]!!", applicantId))));
    }

    @Override
    public List<ConclusionResponseDto> getUrologistConclusionByApplicantJShshir(String jshshir, Pageable pageable) {
        return repository
                .findUrologistConclusionByApplicantJShshir(jshshir, pageable)
                .stream()
                .map(UrologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getUrologistConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable) {
        return repository
                .findUrologistConclusionByApplicantByYDate(yDate, pageable)
                .stream()
                .map(UrologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getUrologistConclusionByApplicantByYDate(LocalDate yDate1,
                                                                              LocalDate yDate2,
                                                                              Pageable pageable) {
        return repository
                .findUrologistConclusionByApplicantByYDate(yDate1, yDate2, pageable)
                .stream()
                .map(UrologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }
}
