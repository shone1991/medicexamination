package uz.egov.medicinemonolithic.service.conclusion.endocrino;

import org.springframework.data.domain.Pageable;
import uz.egov.medicinemonolithic.dto.conclusion.request.EndocrinologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author: Saidov Og'abek
 **/
public interface EndocrinologistConclusionService {
    ConclusionResponseDto save(EndocrinologistConclusionRequestDto conclusionDto);

    ConclusionResponseDto update(Long id, EndocrinologistConclusionRequestDto endocrinologistConclusion);

    ConclusionResponseDto getUrologistConclusionById(Long id);

    List<ConclusionResponseDto> getAllUrologistConclusions(Pageable pageable);

    /*Below methods for retrieving existing records of medical checkups*/
    List<ConclusionResponseDto> getEndocrinoConclusionsBySpecialistId(Long specialistId,
                                                                      Pageable pageable);

    List<ConclusionResponseDto> getEndocrinoConclusionsByIdWorkPlace(Long idWorkPlace,
                                                                     Pageable pageable);

    ConclusionResponseDto getEndocrinoConclusionByApplicationId(Long applicationId);

    ConclusionResponseDto getEndocrinoConclusionByApplicantNtky_id(String ntkyId);

    ConclusionResponseDto getEndocrinoConclusionByApplicantByyNumber(String yNumber);

    ConclusionResponseDto getEndocrinoConclusionByApplicantId(Long applicantId);

    List<ConclusionResponseDto> getEndocrinoConclusionByApplicantJShshir(String jshshir,
                                                                         Pageable pageable);

    List<ConclusionResponseDto> getEndocrinoConclusionByApplicantByYDate(LocalDate yDate,
                                                                         Pageable pageable);

    List<ConclusionResponseDto> getEndocrinoConclusionByApplicantByYDate(LocalDate yDate1, LocalDate yDate2,
                                                                         Pageable pageable);
}
