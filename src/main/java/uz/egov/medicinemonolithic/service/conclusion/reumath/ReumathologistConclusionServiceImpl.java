package uz.egov.medicinemonolithic.service.conclusion.reumath;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.dto.conclusion.request.ReumathologistConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;
import uz.egov.medicinemonolithic.entity.conclusion.ReumathologistConclusion;
import uz.egov.medicinemonolithic.repository.conclusion.ReumathologistConclusionRepository;
import uz.egov.medicinemonolithic.util.conclusion.ReumathologistUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ReumathologistConclusionServiceImpl implements ReumathologistConclusionService {
    private final ReumathologistConclusionRepository repository;
    private final ObjectMapper mapper;

    @Override
    public ConclusionResponseDto save(ReumathologistConclusionRequestDto conclusionDto) {
        ReumathologistConclusion conclusion = ReumathologistUtils.convertToEntity(conclusionDto);
        return ReumathologistUtils.convertToResponseDto(repository.save(conclusion));
    }

    @SneakyThrows
    @Override
    public ConclusionResponseDto update(Long id, ReumathologistConclusionRequestDto conclusionDto) {
        Optional<ReumathologistConclusion> conclusionOptional = repository.findById(id);
        if (conclusionOptional.isPresent()) {
            ReumathologistConclusionRequestDto requestDto = ReumathologistUtils.convertToRequestDto(conclusionOptional.get());
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            conclusionDto.setApplicationId(null);
            conclusionDto.setSpecialistId(null);
            ReumathologistConclusionRequestDto updateValue = mapper.updateValue(requestDto, conclusionDto);
            ReumathologistConclusion conclusion = ReumathologistUtils.convertToEntity(updateValue);
            conclusion.setCreatedAt(conclusionOptional.get().getCreatedAt());
            conclusion.setId(id);
            return ReumathologistUtils.convertToResponseDto(repository.save(conclusion));
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("ReumathologistConclusion not found this id -> [%d]!!", id));
    }

    @Override
    public ConclusionResponseDto getReumathologistConclusionById(Long id) {
        return ReumathologistUtils.convertToResponseDto(repository.findById(id).orElseThrow(()
                -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                String.format("ReumathologistConclusion not found this id -> [%d]!!", id))));
    }

    @Override
    public List<ConclusionResponseDto> getAllReumathologistConclusions(Pageable pageable) {
        return repository.findAll(pageable)
                .stream()
                .map(ReumathologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getReumathologistConclusionsBySpecialistId(Long specialistId, Pageable pageable) {
        return repository
                .findReumathologistConclusionsBySpecialistId(specialistId, pageable)
                .stream()
                .map(ReumathologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getReumathologistConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable) {
        return repository
                .findReumathologistConclusionsByIdWorkPlace(idWorkPlace, pageable)
                .stream()
                .map(ReumathologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public ConclusionResponseDto getReumathologistConclusionByApplicationId(Long applicationId) {
        return ReumathologistUtils
                .convertToResponseDto(repository.findReumathologistConclusionByApplicationId(applicationId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("ReumathologistConclusion not found this applicationId -> [%d]!!", applicationId))));
    }

    @Override
    public ConclusionResponseDto getReumathologistConclusionByApplicantNtky_id(String ntky_id) {
        return ReumathologistUtils
                .convertToResponseDto(repository.findReumathologistConclusionByApplicantNtky_id(ntky_id).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("ReumathologistConclusion not found this ntky_id -> [%s]!!", ntky_id))));
    }

    @Override
    public ConclusionResponseDto getReumathologistConclusionByApplicantByyNumber(String yNumber) {
        return ReumathologistUtils
                .convertToResponseDto(repository.findReumathologistConclusionByApplicantByyNumber(yNumber).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("ReumathologistConclusion not found this yNumber -> [%s]!!", yNumber))));
    }

    @Override
    public ConclusionResponseDto getReumathologistConclusionByApplicantId(Long applicantId) {
        return ReumathologistUtils
                .convertToResponseDto(repository.findReumathologistConclusionByApplicantId(applicantId).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("ReumathologistConclusion not found this applicantId -> [%d]!!", applicantId))));
    }

    @Override
    public List<ConclusionResponseDto> getReumathologistConclusionByApplicantJShshir(String jshshir, Pageable pageable) {
        return repository
                .findReumathologistConclusionByApplicantJShshir(jshshir, pageable)
                .stream()
                .map(ReumathologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getReumathologistConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable) {
        return repository
                .findReumathologistConclusionByApplicantByYDate(yDate, pageable)
                .stream()
                .map(ReumathologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ConclusionResponseDto> getReumathologistConclusionByApplicantByYDate(LocalDate yDate1,
                                                                                     LocalDate yDate2,
                                                                                     Pageable pageable) {
        return repository
                .findReumathologistConclusionByApplicantByYDate(yDate1, yDate2, pageable)
                .stream()
                .map(ReumathologistUtils::convertToResponseDto)
                .collect(Collectors.toList());
    }
}
