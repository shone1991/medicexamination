package uz.egov.medicinemonolithic.service.conclusion.overall;


import org.springframework.data.domain.Pageable;
import uz.egov.medicinemonolithic.dto.conclusion.request.OverallConclusionRequestDto;
import uz.egov.medicinemonolithic.dto.conclusion.response.ConclusionResponseDto;

import java.time.LocalDate;
import java.util.List;

/**
 * @Author: Saidov Og'abek
 **/
public interface OverallConclusionService {
    ConclusionResponseDto save(OverallConclusionRequestDto conclusion);

    ConclusionResponseDto update(Long id, OverallConclusionRequestDto conclusion);

    List<ConclusionResponseDto> getAllOverallConclusions(Pageable pageable);

    ConclusionResponseDto getOverallConclusionById(Long id);

    List<ConclusionResponseDto> getOverallConclusionsBySpecialistId(Long specialistId, Pageable pageable);

    List<ConclusionResponseDto> getOverallConclusionsByIdWorkPlace(Long idWorkPlace, Pageable pageable);

    ConclusionResponseDto getOverallConclusionByApplicationId(Long applicationId);

    ConclusionResponseDto getOverallConclusionByApplicantNtky_id(String ntky_id);

    ConclusionResponseDto getOverallConclusionByApplicantByyNumber(String yNumber);

    ConclusionResponseDto getOverallConclusionByApplicantId(Long applicantId);

    List<ConclusionResponseDto> getOverallConclusionByApplicantJShshir(String jshshir, Pageable pageable);

    List<ConclusionResponseDto> getOverallConclusionByApplicantByYDate(LocalDate yDate, Pageable pageable);

    List<ConclusionResponseDto> getOverallConclusionByApplicantByYDate(LocalDate yDate1,
                                                                       LocalDate yDate2,
                                                                       Pageable pageable);
}
