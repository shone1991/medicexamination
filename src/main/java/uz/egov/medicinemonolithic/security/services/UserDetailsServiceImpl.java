package uz.egov.medicinemonolithic.security.services;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import uz.egov.medicinemonolithic.entity.Clinic;
import uz.egov.medicinemonolithic.entity.users.User;
import uz.egov.medicinemonolithic.repository.ClinicRepository;
import uz.egov.medicinemonolithic.repository.user_role.UserRepository;
import uz.egov.medicinemonolithic.service.specialist.SpecialistService;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;
    private final SpecialistService specialistService;
    private final ClinicRepository clinicRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

        if (!userRepository.existsByUsernameAndIsActiveTrue(username))
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "This user has been blocked !!");

        Clinic workPlace = clinicRepository.findById(
                        specialistService
                                .findSpecialistByUserId(user.getId())
                                .getClinicId())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Clinic Not Found with username: " + username));

//        return UserDetailsImpl.build(user);
        return UserDetailsImpl.build(user, workPlace);
    }

}
