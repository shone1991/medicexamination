package uz.egov.medicinemonolithic.dto.auth.response;

import uz.egov.medicinemonolithic.entity.Clinic;

import java.util.List;

public class JwtResponse {
  private String token;
  private String type = "Bearer";
  private Long id;
  private String name;
  private String surname;
  private Clinic clinic;
  private String username;
  private String phone;
  private List<String> roles;

  public JwtResponse(String accessToken, Long id, String username, String phone, List<String> roles) {
    this.token = accessToken;
    this.id = id;
    this.username = username;
    this.phone = phone;
    this.roles = roles;
  }

  public JwtResponse(String accessToken, Long id, String name, String surname,
                     Clinic clinic, String username, String phone, List<String> roles) {
    this.token = accessToken;
    this.id = id;
    this.name=name;
    this.surname=surname;
    this.clinic=clinic;
    this.username = username;
    this.phone = phone;
    this.roles = roles;
  }

  public String getAccessToken() {
    return token;
  }

  public void setAccessToken(String accessToken) {
    this.token = accessToken;
  }

  public String getTokenType() {
    return type;
  }

  public void setTokenType(String tokenType) {
    this.type = tokenType;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public Clinic getClinic() {
    return clinic;
  }

  public void setClinic(Clinic clinic) {
    this.clinic = clinic;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public List<String> getRoles() {
    return roles;
  }
}
