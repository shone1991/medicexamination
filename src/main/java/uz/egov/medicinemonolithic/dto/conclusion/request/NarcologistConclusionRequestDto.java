package uz.egov.medicinemonolithic.dto.conclusion.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 04-March-2022  Friday 4:35 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NarcologistConclusionRequestDto {
    private Long applicationId;
    private Boolean hasPsychoPathology;
    private Boolean isRegistered;
    private byte[] certificate;
    private String comment;
    private String diagnosisInfo;
    private Long specialistId;
    private LocalDate conclusionDate;
}
