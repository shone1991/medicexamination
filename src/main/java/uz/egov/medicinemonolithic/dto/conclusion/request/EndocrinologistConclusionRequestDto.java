package uz.egov.medicinemonolithic.dto.conclusion.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 04-March-2022  Friday 2:45 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EndocrinologistConclusionRequestDto {
    private Long applicationId;
    private String comment;
    private Boolean isHealthy;
    private String examinationDetails;
    private Long specialistId;
    private LocalDate conclusionDate;
}
