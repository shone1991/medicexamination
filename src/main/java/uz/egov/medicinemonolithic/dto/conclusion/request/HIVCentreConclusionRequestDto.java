package uz.egov.medicinemonolithic.dto.conclusion.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 04-March-2022  Friday 5:26 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HIVCentreConclusionRequestDto {
    private Long applicationId;
    private Boolean isInfected;
    private LocalDate examinationDate;
    private String numberExamination;
    private String comment;
    private Boolean result;
    private Long specialistId;
    private LocalDate conclusionDate;
}
