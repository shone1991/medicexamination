package uz.egov.medicinemonolithic.dto.conclusion.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 07-March-2022  Monday 3:13 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PsychiatristConclusionRequestDto {
    private Long applicationId;
    private Boolean isRegistered;
    private byte[] certificate;
    private Boolean hasPsychoDisorders;
    private String comment;
    private Long specialistId;
    private LocalDate conclusionDate;
}
