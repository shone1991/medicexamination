package uz.egov.medicinemonolithic.dto.conclusion.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 28-February-2022  Monday 5:26 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VenerologistConclusionRequestDto {
    private Long applicationId;
    private Boolean hasSiphilis;
    private LocalDate foundOutDate;
    private byte[] certificate;
    private String comment;
    private Long specialistId;
    private LocalDate conclusionDate;
}
