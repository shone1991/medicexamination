package uz.egov.medicinemonolithic.dto.conclusion.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 03-March-2022  Thursday 4:06 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PhthisiatricianConclusionRequestDto {
    private Long applicationId;
    private Boolean hasTuberculosis;
    private byte[] certificate;
    private String comment;
    private Long specialistId;
    private LocalDate conclusionDate;

}
