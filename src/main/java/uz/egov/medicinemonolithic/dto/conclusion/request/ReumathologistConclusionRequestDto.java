package uz.egov.medicinemonolithic.dto.conclusion.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 03-March-2022  Thursday 2:00 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReumathologistConclusionRequestDto {
    private Long applicationId;
    private String comment;
    private Boolean isHealthy;
    private String examinationDetails;
    private Long specialistId;
    private LocalDate conclusionDate;
}
