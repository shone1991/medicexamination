package uz.egov.medicinemonolithic.dto.conclusion.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 03-March-2022  Thursday 5:47 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CardiologistConclusionRequestDto {
    private Long applicationId;
    private String ekgResultComment;
    private Boolean isHealthy;
    private String examinationDetails;
    private Long specialistId;
    private LocalDate conclusionDate;
}
