package uz.egov.medicinemonolithic.dto.conclusion.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 07-March-2022  Monday 2:43 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OverallConclusionRequestDto {
    private Long applicationId;
    private Long endocrinologistConclusionId;
    private Long cardiologistConclusionId;
    private Long narcologistConclusionId;
    private Long phthisiatricianConclusionId;
    private Long psychiatristConclusionId;
    private Long reumathologistConclusionId;
    private Long urologistConclusionId;
    private Long venerologistConclusionId;
    private Long specialistId;
    private String comment;
    private String numberConclusion;
    private LocalDate conclusionDate;
    private LocalDate conclusionValidDate;
    private LocalDate explanationDate;
}
