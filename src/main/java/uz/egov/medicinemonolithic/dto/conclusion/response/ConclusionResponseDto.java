package uz.egov.medicinemonolithic.dto.conclusion.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.egov.medicinemonolithic.entity.ApplicationStatus;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 28-February-2022  Monday 5:15 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConclusionResponseDto {
    private Long id;
    private Long applicationId;
    private String nsh_FIO;
    private LocalDate nSh_BDate;
    private String branch;
    private LocalDateTime information_date;
    @Enumerated(EnumType.STRING)
    private ApplicationStatus applicationStatus;
    private String comment;
}
