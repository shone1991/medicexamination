package uz.egov.medicinemonolithic.dto.specialist.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 14-March-2022  Monday 4:11 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SpecialistRequestDto {
    private Long userId;
    private Long clinicId;
}
