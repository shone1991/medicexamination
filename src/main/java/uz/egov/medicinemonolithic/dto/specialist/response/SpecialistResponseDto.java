package uz.egov.medicinemonolithic.dto.specialist.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 14-March-2022  Monday 4:13 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SpecialistResponseDto {
    private Long id;
    private Long userId;
    private Long clinicId;
}
