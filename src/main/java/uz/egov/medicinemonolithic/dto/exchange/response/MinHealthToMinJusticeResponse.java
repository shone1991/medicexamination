package uz.egov.medicinemonolithic.dto.exchange.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * @author f.lapasov on 2/8/2022
 * @project medicine
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class MinHealthToMinJusticeResponse {
    @JsonProperty
    private String ntky_id;
    @JsonProperty
    private String id_org;
    @JsonProperty
    private String error;
    @JsonProperty
    private String receive_time_org;
}
