package uz.egov.medicinemonolithic.dto.exchange.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * @author f.lapasov on 2/8/2022
 * @project medicine
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class NTYKData {
    @JsonProperty
    @NotBlank(message = "ntky_id is required")
    @NotNull
    private String ntky_id;
    @JsonProperty
    @NotBlank(message = "jshshir is required")
    @Size(min = 14,max = 14,message = "jshshir size must be 14")
    private String jshshir;
    @JsonProperty
    @NotBlank(message = "inVoiceN is required")
    private String inVoiceN;
    @JsonProperty
    @JsonFormat(pattern="dd.MM.yyyy")
    @NotBlank(message = "inVoiceN is required")
    private LocalDate inVoiceD;
    @JsonProperty
    @NotBlank(message = "branch is required")
    private String branch;
    @JsonProperty
    @NotBlank(message = "yNumber is required")
    private String yNumber;
    @JsonFormat(pattern="dd.MM.yyyy")
    @NotNull(message = "yDate is required")
    @JsonProperty
    private LocalDate yDate;
    @JsonProperty
    @NotBlank(message = "nSh_FIO is required")
    private String nSh_FIO;
    @JsonProperty
    @NotBlank(message = "nSh_Address is required")
    private String nSh_Address;
    @JsonFormat(pattern="dd.MM.yyyy")
    @JsonProperty
    @NotNull(message = "nSh_BDate is required")
    private LocalDate nSh_BDate;
    @JsonProperty
    @NotBlank(message = "fHDY_Bosh is required")
    private String fHDY_Bosh;
}
