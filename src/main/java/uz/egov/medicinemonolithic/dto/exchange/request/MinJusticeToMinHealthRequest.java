package uz.egov.medicinemonolithic.dto.exchange.request;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * @author f.lapasov on 2/8/2022
 * @project medicine
 */

@Data
@Getter
@Setter
public class MinJusticeToMinHealthRequest {
    @Valid
    @NotNull
    private NTYKInformation ntykInformation;
}
