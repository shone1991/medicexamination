package uz.egov.medicinemonolithic.dto.application.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.egov.medicinemonolithic.entity.Applicant;

import java.time.LocalDateTime;

/**
 * @author f.lapasov on 2/14/2022
 * @project medicine-monolithic
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationExchangeDto {
    private Applicant applicant;
    private LocalDateTime information_date;
}
