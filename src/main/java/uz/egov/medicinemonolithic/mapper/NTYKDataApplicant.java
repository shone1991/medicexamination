package uz.egov.medicinemonolithic.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;
import uz.egov.medicinemonolithic.dto.exchange.request.NTYKData;
import uz.egov.medicinemonolithic.entity.Applicant;

/**
 * @author f.lapasov on 2/8/2022
 * @project medicine
 */

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface NTYKDataApplicant {

    @Mapping(source = "ntky_id",target = "ntky_id")
    @Mapping(source = "jshshir",target = "jshshir")
    @Mapping(source = "inVoiceN",target = "inVoiceN")
    @Mapping(source = "inVoiceD",target = "inVoiceD")
    @Mapping(source = "branch",target = "branch")
    @Mapping(source = "YNumber",target = "yNumber")
    @Mapping(source = "YDate",target = "yDate")
    @Mapping(source = "NSh_FIO",target = "nSh_FIO")
    @Mapping(source = "NSh_Address",target = "nSh_Address")
    @Mapping(source = "NSh_BDate",target = "nSh_BDate")
    @Mapping(source = "FHDY_Bosh",target = "fHDY_Bosh")
    Applicant ntykDataToApplicant(NTYKData ntykData);
}
