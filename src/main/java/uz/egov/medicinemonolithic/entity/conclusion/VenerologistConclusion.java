package uz.egov.medicinemonolithic.entity.conclusion;

import lombok.*;
import uz.egov.medicinemonolithic.entity.Application;
import uz.egov.medicinemonolithic.entity.Clinic;
import uz.egov.medicinemonolithic.entity.Specialist;
import uz.egov.medicinemonolithic.entity.base.BaseEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(
        uniqueConstraints = @UniqueConstraint(columnNames = {"application_id", "specialist_id"})
)
public class VenerologistConclusion extends BaseEntity {
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id")
    private Application application;
    private Boolean hasSiphilis;
    private LocalDate foundOutDate;
//    @Lob
//    private byte[] certificate;
    private String comment;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specialist_id")
    private Specialist specialist;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clinic_id")
    private Clinic workPlace;
    private LocalDate conclusionDate;
    @OneToMany
    private List<Certificate> certificate=new ArrayList<>();
}
