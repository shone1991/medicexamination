package uz.egov.medicinemonolithic.entity.conclusion;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import uz.egov.medicinemonolithic.entity.Application;
import uz.egov.medicinemonolithic.entity.Clinic;
import uz.egov.medicinemonolithic.entity.Specialist;
import uz.egov.medicinemonolithic.entity.base.BaseEntity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(
        uniqueConstraints = @UniqueConstraint(columnNames = {"application_id", "specialist_id"})
)
public class CardiologistConclusion extends BaseEntity {
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id")
    private Application application;
    private String ekgResultComment;
    private Boolean isHealthy;
    private String examinationDetails;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specialist_id")
    private Specialist specialist;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clinic_id")
    private Clinic workPlace;
    private LocalDate conclusionDate;
}
