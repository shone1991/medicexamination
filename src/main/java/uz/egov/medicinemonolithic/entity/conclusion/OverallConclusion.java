package uz.egov.medicinemonolithic.entity.conclusion;

import lombok.*;
import uz.egov.medicinemonolithic.entity.Application;
import uz.egov.medicinemonolithic.entity.Clinic;
import uz.egov.medicinemonolithic.entity.Specialist;
import uz.egov.medicinemonolithic.entity.base.BaseEntity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(
        uniqueConstraints = @UniqueConstraint(columnNames = {"application_id", "specialist_id"})
)
public class OverallConclusion extends BaseEntity {
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id")
    private Application application;
    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.MERGE)
    EndocrinologistConclusion endocrinologistConclusion;
    @OneToOne(fetch = FetchType.LAZY)
    CardiologistConclusion cardiologistConclusion;
    @OneToOne(fetch = FetchType.LAZY)
    NarcologistConclusion narcologistConclusion;
    @OneToOne(fetch = FetchType.LAZY)
    PhthisiatricianConclusion phthisiatricianConclusion;
    @OneToOne(fetch = FetchType.LAZY)
    PsychiatristConclusion psychiatristConclusion;
    @OneToOne(fetch = FetchType.LAZY)
    ReumathologistConclusion reumathologistConclusion;
    @OneToOne(fetch = FetchType.LAZY)
    UrologistConclusion urologistConclusion;
    @OneToOne(fetch = FetchType.LAZY)
    VenerologistConclusion venerologistConclusion;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specialist_id")
    private Specialist specialist;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clinic_id")
    private Clinic workPlace;
    private String comment;
    private String numberConclusion;
    private LocalDate conclusionDate;
    private LocalDate conclusionValidDate;
    private LocalDate explanationDate;
}
