package uz.egov.medicinemonolithic.entity.conclusion;

import lombok.*;
import uz.egov.medicinemonolithic.entity.Application;
import uz.egov.medicinemonolithic.entity.Clinic;
import uz.egov.medicinemonolithic.entity.Specialist;
import uz.egov.medicinemonolithic.entity.base.BaseEntity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(
        uniqueConstraints = @UniqueConstraint(columnNames = {"application_id", "specialist_id"})
)
public class PsychiatristConclusion extends BaseEntity {
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id")
    private Application application;
    private boolean isRegistered;
    @Lob
    private byte[] certificate;
    private boolean hasPsychoDisorders;
    private String comment;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "specialist_id")
    private Specialist specialist;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clinic_id")
    private Clinic workPlace;
    private LocalDate conclusionDate;
}
