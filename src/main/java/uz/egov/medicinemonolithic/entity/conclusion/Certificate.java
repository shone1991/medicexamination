package uz.egov.medicinemonolithic.entity.conclusion;

import lombok.*;
import uz.egov.medicinemonolithic.entity.base.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 * @author Saidov Og'abek
 * @project medicine-monolithic
 * @created at 02-March-2022  Wednesday 5:35 PM
 **/
@Entity(name = "certificate")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Certificate extends BaseEntity {
    private String file_url;
    @ManyToOne
    private VenerologistConclusion venerologistConclusion;

}
