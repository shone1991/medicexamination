package uz.egov.medicinemonolithic.entity.conclusion;

import lombok.Getter;
import lombok.Setter;
import uz.egov.medicinemonolithic.entity.base.BaseEntity;

import javax.persistence.Column;
import java.time.LocalDate;

@Getter
@Setter
public class AbstractConclusion extends BaseEntity {
    @Column(name = "specialistId")
    private Long specialistId;
    @Column(name = "idWorkPlace")
    private Long idWorkPlace;
    @Column(name = "conclusionDate")
    private LocalDate conclusionDate;
}
