package uz.egov.medicinemonolithic.entity;

import lombok.*;
import uz.egov.medicinemonolithic.entity.base.BaseEntity;
import uz.egov.medicinemonolithic.validator.Unique;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Applicant extends BaseEntity {

    @Column(unique=true)
    @Unique(message = "{message}")
    private String ntky_id;
    private String jshshir;
    private String inVoiceN;
    private LocalDate inVoiceD;
    private String branch;
    @Column(unique=true)
    private String yNumber;
    private LocalDate yDate;
    private String nSh_FIO;
    private String nSh_Address;
    private LocalDate nSh_BDate;
    private String fHDY_Bosh;
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Applicant applicant = (Applicant) o;
        return ntky_id.equals(applicant.ntky_id) && jshshir.equals(applicant.jshshir)
                && inVoiceN.equals(applicant.inVoiceN) && inVoiceD.equals(applicant.inVoiceD)
                && branch.equals(applicant.branch) && yNumber.equals(applicant.yNumber)
                && yDate.equals(applicant.yDate) && nSh_FIO.equals(applicant.nSh_FIO)
                && nSh_Address.equals(applicant.nSh_Address)
                && nSh_BDate.equals(applicant.nSh_BDate)
                && fHDY_Bosh.equals(applicant.fHDY_Bosh);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), ntky_id, jshshir, inVoiceN, inVoiceD, branch, yNumber, yDate, nSh_FIO, nSh_Address, nSh_BDate, fHDY_Bosh);
    }
}
