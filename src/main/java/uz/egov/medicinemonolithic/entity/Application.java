package uz.egov.medicinemonolithic.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.egov.medicinemonolithic.entity.base.BaseEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Application extends BaseEntity {
    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.REFRESH)
    private Applicant applicant;


    private LocalDateTime information_date;

    @Enumerated(EnumType.STRING)
    private ApplicationStatus applicationStatus;
}
