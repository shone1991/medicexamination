package uz.egov.medicinemonolithic.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.egov.medicinemonolithic.entity.base.BaseEntity;

import javax.persistence.Entity;

/**
 * @author f.lapasov on 2/14/2022
 * @project medicine-monolithic
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Clinic extends BaseEntity {
    private String name;
}
