package uz.egov.medicinemonolithic.entity.users;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.time.LocalDate;

/**
 * @author f.lapasov on 2/15/2022
 * @project medicine-monolithic
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Embeddable
public class UserDetail {
    private String name;
    private String surname;
    private LocalDate birthDate;
    private String pinfl;
    private String passportSerialNumber;
}
