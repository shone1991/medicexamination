package uz.egov.medicinemonolithic.entity.users;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public enum ERole {
    ROLE_USER("ROLE_USER"),
    ROLE_MODERATOR("ROLE_MODERATOR"),
    ROLE_ADMIN("ROLE_ADMIN"),

    ROLE_HEAD_PHYSICIAN("ROLE_HEAD_PHYSICIAN"),

    ROLE_NARCOLOGIST("ROLE_NARCOLOGIST"),
    ROLE_HIVCENTRE("ROLE_HIVCENTRE"),
    ROLE_PSYCHIATRIST("ROLE_PSYCHIATRIST"),
    ROLE_ENDOCRINOLOGIST("ROLE_ENDOCRINOLOGIST"),
    ROLE_REUMATHOLOGIST("ROLE_REUMATHOLOGIST"),
    ROLE_UROLOGIST("ROLE_UROLOGIST"),
    ROLE_CARDIOLOGIST("ROLE_CARDIOLOGIST"),
    ROLE_VENEROLOGIST("ROLE_VENEROLOGIST"),
    ROLE_PHTHISIATRICIAN("ROLE_PHTHISIATRICIAN");

    private String value;

    ERole(String value) {
        this.value = value;
    }

    public static ERole getEnum(String value) {
        for (ERole v : values())
            if (v.name().equalsIgnoreCase(value)) return v;
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "This name of role is not available !!");
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ERole{" +
                "value='" + value + '\'' +
                '}';
    }
}
