package uz.egov.medicinemonolithic.entity;

public enum ApplicationStatus {
    ACCEPTED,PROCESSING,APPROVED,REJECTED
}
