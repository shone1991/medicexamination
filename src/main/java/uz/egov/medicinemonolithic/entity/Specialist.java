package uz.egov.medicinemonolithic.entity;

import lombok.*;
import org.hibernate.Hibernate;
import uz.egov.medicinemonolithic.entity.base.BaseEntity;
import uz.egov.medicinemonolithic.entity.users.User;
import uz.egov.medicinemonolithic.validator.Unique;

import javax.persistence.*;
import java.util.Objects;

/**
 * @author f.lapasov on 2/14/2022
 * @project medicine-monolithic
 */
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = { "user_id"})})
public class Specialist extends BaseEntity {
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @ToString.Exclude
    private User userMedicine;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "clinic_id")
    @ToString.Exclude
    private Clinic workPlace;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Specialist that = (Specialist) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
